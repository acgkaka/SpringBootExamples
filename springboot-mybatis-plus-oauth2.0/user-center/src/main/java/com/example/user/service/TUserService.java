package com.example.user.service;

import com.example.user.entity.TUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ACGkaka
 * @since 2021-04-25
 */
public interface TUserService extends IService<TUser> {

    /**
     * 根据 clientId 和 用户名 查询
     */
    TUser loadUserByUsername(String clientId, String username);

    void updateUser(String username);
}
