package com.example.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.user.entity.TUser;
import com.example.user.mapper.TUserMapper;
import com.example.user.service.TUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ACGkaka
 * @since 2021-04-25
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements TUserService {

    @Autowired
    private TUserMapper userMapper;

    @Override
    public TUser loadUserByUsername(String clientId, String username) {
        // 根据 clientId 和 用户名 查询
        LambdaQueryWrapper<TUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TUser::getUsername, username);
        TUser user = this.getOne(lambdaQueryWrapper);

        // TODO:查询权限信息

        return user;
    }

    @Override
    public void updateUser(String username) {
        UpdateWrapper<TUser> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda().set(TUser::getUsername, username).eq(TUser::getId, 7);
        userMapper.update(null, updateWrapper);
    }
}
