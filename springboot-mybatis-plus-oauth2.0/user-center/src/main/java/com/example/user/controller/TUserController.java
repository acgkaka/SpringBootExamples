package com.example.user.controller;


import com.example.common.Result;
import com.example.user.entity.TUser;
import com.example.user.service.TUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author ACGkaka
 * @since 2021-04-25
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class TUserController {

    @Autowired
    private TUserService tUserService;

    @GetMapping("/list")
    public List<TUser> list() {
        return tUserService.list();
    }

    /**
     * 根据用户名加载用户信息
     */
    @GetMapping("/loadUserByUsername")
    public Result<TUser> loadUserByUsername(@RequestParam("clientId") String clientId, @RequestParam("username") String username) {
        try {
            Result<TUser> result= Result.succeed();
            TUser user = tUserService.loadUserByUsername(clientId, username);
            return result.setData(user);
        } catch (Exception e) {
            log.error(">>>>>>>>>>【ERROR】登录失败，请联系管理员");
            throw new RuntimeException("登录失败，请联系管理员");
        }
    }
}
