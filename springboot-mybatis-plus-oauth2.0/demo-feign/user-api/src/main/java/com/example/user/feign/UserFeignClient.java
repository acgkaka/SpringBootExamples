package com.example.user.feign;

import com.example.common.Result;
import com.example.user.entity.TUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Feign接口-用户中心
 */
@FeignClient(value = "user-center")
public interface UserFeignClient {

    /**
     * 根据用户名加载用户信息
     */
    @GetMapping("/user/loadUserByUsername")
    Result<TUser> loadUserByUsername(@RequestParam("clientId") String clientId, @RequestParam("username") String username);
}
