//package com.example.auth.config.token;
//
//import org.springframework.security.authentication.AbstractAuthenticationToken;
//import org.springframework.security.core.GrantedAuthority;
//
//import java.util.Collection;
//
///**
// * Security - 封装Token（密码登录方式）
// */
//public class SMSLoginAuthenticationToken extends AbstractAuthenticationToken {
//
//    // ~ Instance fields
//    // ================================================================================================
//
//    /** 存放用户名 ： credentials 字段去掉，因为短信认证在授权认证前已经过滤了 */
//    private final Object principal;
//
//    // ~ Constructors
//    // ===================================================================================================
//
//    /**
//     * This constructor can be safely used by any code that wishes to create a
//     * <code>UsernamePasswordAuthenticationToken</code>, as the {@link #isAuthenticated()}
//     * will return <code>false</code>.
//     */
//    public SMSLoginAuthenticationToken(String mobile) {
//        super(null);
//        this.principal = mobile;
//        setAuthenticated(false);
//    }
//
//    /**
//     * 该构造方法仅用于 AuthenticationManager 或 AuthenticationProvider 的实现。
//     * @param principal     用户信息
//     * @param authorities   权限信息
//     */
//    public SMSLoginAuthenticationToken(Object principal,
//                                       Collection<? extends GrantedAuthority> authorities) {
//        super(authorities);
//        this.principal = principal;
//        super.setAuthenticated(true); // must use super, as we override
//    }
//
//
//    // ~ Methods
//    // ========================================================================================================
//    @Override
//    public Object getPrincipal() {
//        return this.principal;
//    }
//
//    @Override
//    public Object getCredentials() {
//        return null;
//    }
//
//    @Override
//    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
//        if (isAuthenticated) {
//            throw new IllegalArgumentException(
//                    "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
//        }
//
//        super.setAuthenticated(false);
//    }
//
//    @Override
//    public void eraseCredentials() {
//        super.eraseCredentials();
//    }
//
//}
