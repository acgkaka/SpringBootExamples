//package com.example.auth.config.provider;
//
//import com.example.auth.config.token.SMSLoginAuthenticationToken;
//import com.example.user.entity.TUser;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.DisabledException;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//
///**
// * Security - 自定义身份验证逻辑
// */
//@Component
//public class SMSLoginAuthenticationProvider implements AuthenticationProvider {
//
//    @Lazy
//    @Resource
//    @Qualifier("smsLoginDetailServiceImpl")
//    private UserDetailsService smsLoginDetailServiceImpl;
//
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        SMSLoginAuthenticationToken token = (SMSLoginAuthenticationToken) authentication;
//
//        UserDetails userDetails = smsLoginDetailServiceImpl.loadUserByUsername((String) token.getPrincipal());
//        if (userDetails == null) {
//            throw new AuthenticationCredentialsNotFoundException("用户名或密码错误");
//        } else if (!userDetails.isEnabled()) {
//            throw new DisabledException("用户已作废");
//        }
//
//        // 判断用户状态是否正常
//        if (userDetails instanceof TUser) {
//            TUser loginUser = (TUser) userDetails;
//            if (loginUser.getIsLock() != null && loginUser.getIsLock() == 1) {
//                throw new IllegalArgumentException("账号存在异常，已被限制登录!");
//            }
//        }
//
//        SMSLoginAuthenticationToken authenticationResult = new SMSLoginAuthenticationToken(userDetails, userDetails.getAuthorities());
//        // 需要把未认证中的一些信息copy到已认证的token中
//        authenticationResult.setDetails(token);
//        return authenticationResult;
//    }
//
//    @Override
//    public boolean supports(Class<?> aClass) {
//        return SMSLoginAuthenticationToken.class.isAssignableFrom(aClass);
//    }
//
//}
