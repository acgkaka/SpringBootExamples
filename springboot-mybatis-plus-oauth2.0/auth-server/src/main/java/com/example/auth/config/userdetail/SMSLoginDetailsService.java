//package com.example.auth.config.userdetail;
//
//import com.example.user.entity.TUser;
//import com.example.auth.service.TUserService;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.oauth2.common.exceptions.BadClientCredentialsException;
//import org.springframework.security.oauth2.common.exceptions.UnauthorizedClientException;
//import org.springframework.security.oauth2.provider.ClientDetails;
//import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
//import org.springframework.security.web.authentication.www.BasicAuthenticationConverter;
//import org.springframework.stereotype.Service;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//
///**
// * Security - 封装登录用户信息
// */
//@Service
//public class SMSLoginDetailsService implements UserDetailsService {
//
//    private final BasicAuthenticationConverter authenticationConverter = new BasicAuthenticationConverter();
//    @Resource
//    private TUserService tUserService;
//    @Resource
//    private JdbcClientDetailsService jdbcClientDetailsService;
//    @Resource
//    private PasswordEncoder passwordEncoder;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) {
//        // 根据用户名加载用户信息
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        String clientId;
//        if (authentication != null) {
//            Object principal = authentication.getPrincipal();
//            if (principal instanceof User) {
//                User clientUser = (User) principal;
//                clientId = clientUser.getUsername();
//            } else if (principal instanceof TUser) {
//                getClientIdByRequest();
//                return (TUser) principal;
//            } else {
//                throw new UnsupportedOperationException();
//            }
//        } else {
//            clientId = getClientIdByRequest();
//        }
//
//        // 获取用户（包含权限信息）
//        TUser loginUser = tUserService.loadUserByUsername(clientId, username);
//        // 用户不存在
//        if (loginUser == null) {
//            throw new UsernameNotFoundException("user not found");
//        }
//        return loginUser;
//    }
//
//
//    /**
//     * 从httpRequest中获取clientId并验证客户端信息
//     */
//    public String getClientIdByRequest() {
//        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        if (attributes == null) throw new UnsupportedOperationException();
//        HttpServletRequest request = attributes.getRequest();
//        UsernamePasswordAuthenticationToken client = authenticationConverter.convert(request);
//        if (client == null) {
//            throw new UnauthorizedClientException("unauthorized client");
//        }
//        ClientDetails clientDetails = jdbcClientDetailsService.loadClientByClientId(client.getName());
//        if (!passwordEncoder.matches((String) client.getCredentials(), clientDetails.getClientSecret())) {
//            throw new BadClientCredentialsException();
//        }
//        return clientDetails.getClientId();
//    }
//
//}
