-- ------------------------------
-- 用户表
-- ------------------------------
CREATE TABLE `t_user` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(64) NOT NULL COMMENT '用户名',
  `password` varchar(64) NOT NULL COMMENT '密码',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';


-- ------------------------------
-- OAuth2.0-客户端信息表
-- ------------------------------
CREATE TABLE `oauth_client_details` (
	`client_id` VARCHAR(256) NOT NULL COMMENT '主键（客户端ID）' COLLATE 'utf8_general_ci',
	`resource_ids` VARCHAR(256) NULL DEFAULT NULL COMMENT '客户端所能访问的资源ID集合（多个资源用,分隔）' COLLATE 'utf8_general_ci',
	`client_secret` VARCHAR(256) NULL DEFAULT NULL COMMENT '客户端访问密钥' COLLATE 'utf8_general_ci',
	`scope` VARCHAR(256) NULL DEFAULT NULL COMMENT '客户端申请的权限范围（read/write/trust，多个权限用,分隔）' COLLATE 'utf8_general_ci',
	`authorized_grant_types` VARCHAR(256) NULL DEFAULT NULL COMMENT '5种oauth授权方式(authorization_code;password;refresh_token;client_credentials)' COLLATE 'utf8_general_ci',
	`web_server_redirect_uri` VARCHAR(256) NULL DEFAULT NULL COMMENT '回调地址，当grant_type为authorization_code或implicit时, 在Oauth的流程中会使用并检查与数据库内的redirect_uri是否一致。' COLLATE 'utf8_general_ci',
	`authorities` VARCHAR(256) NULL DEFAULT NULL COMMENT '客户端所拥有的Spring Security权限值（多个权限用,分隔）' COLLATE 'utf8_general_ci',
	`access_token_validity` INT(11) NULL DEFAULT NULL COMMENT '设定客户端的access_token的有效时间值(单位:秒)，若不设定值则使用默认的有效时间值(60 * 60 * 12, 12小时)',
	`refresh_token_validity` INT(11) NULL DEFAULT NULL COMMENT '设定客户端的refresh_token的有效时间值(单位:秒)，若不设定值则使用默认的有效时间值(60 * 60 * 24 * 30, 30天)',
	`additional_information` VARCHAR(4096) NULL DEFAULT NULL COMMENT '这是一个预留的字段,在Oauth的流程中没有实际的使用,可选,但若设置值,必须是JSON格式的数据' COLLATE 'utf8_general_ci',
	`autoapprove` VARCHAR(256) NULL DEFAULT NULL COMMENT '设置用户是否自动批准授予权限操作, 默认值为 ‘false’, 可选值包括 ‘true’,‘false’, ‘read’,‘write’.' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`client_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端信息表';


-- ------------------------------
-- 初始化数据
-- ------------------------------
-- 账号/密码：ACGkaka/123456
INSERT INTO `t_user` (`id`, `username`, `password`, `create_time`, `update_time`) VALUES
    (1, 'ACGkaka', '$2a$10$tTm9Lo2hMqjcKS0naRBahu8iZ6AiqHgDZ.JYBpDmM5q.pc6aFsxOa', '2025-02-11 11:01:21', '2025-02-11 11:04:20');

INSERT INTO oauth_client_details (client_id,resource_ids,client_secret,scope,authorized_grant_types,web_server_redirect_uri,authorities,access_token_validity,refresh_token_validity,additional_information,autoapprove) VALUES
    ('app',NULL,'$2a$10$p.7WaA3cHOW0e/S3HgXy0e4wrtJfTilw2mH.fiEQRbPhLvIVOYbXW','app','password,refresh_token',NULL,NULL,1800,36000,'{}','true');

