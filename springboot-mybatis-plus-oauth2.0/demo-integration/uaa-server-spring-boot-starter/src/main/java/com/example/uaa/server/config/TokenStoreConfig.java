package com.example.uaa.server.config;

import com.example.uaa.server.token.RedisTemplateTokenStore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.util.Assert;

import javax.sql.DataSource;

/**
 * redis存储token
 */
@Configuration
public class TokenStoreConfig {


    @Bean
    @ConditionalOnProperty(prefix = "security.oauth2.token.store", name = "type", havingValue = "jdbc", matchIfMissing = false)
    public JdbcTokenStore jdbcTokenStore(DataSource dataSource) {
        // oauth_access_token oauth_refresh_token 创建两张表
        return new JdbcTokenStore(dataSource);
    }

    @Bean
    @ConditionalOnProperty(prefix = "security.oauth2.token.store", name = "type", havingValue = "redis", matchIfMissing = true)
    public RedisTemplateTokenStore redisTokenStore(RedisTemplate<String, Object> redisTemplate) {
        Assert.state(redisTemplate != null, "RedisTemplate must be provided");

        RedisTemplateTokenStore redisTemplateStore = new RedisTemplateTokenStore();
        redisTemplateStore.setRedisTemplate(redisTemplate);
        return redisTemplateStore;
    }

}
