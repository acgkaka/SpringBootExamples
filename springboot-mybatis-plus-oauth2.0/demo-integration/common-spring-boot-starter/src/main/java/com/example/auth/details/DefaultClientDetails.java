package com.example.auth.details;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import java.io.Serializable;


/**
 * 客户端应用信息
 */
@AllArgsConstructor
@Data
public class DefaultClientDetails extends BaseClientDetails implements Serializable {

	/**
	 * 如果oauth_client_details表需要扩展字段，可以在这里补充
	 */
	private static final long serialVersionUID = -4996423520248249518L;

	
	public DefaultClientDetails(String clientId, String resourceIds,
                                String scopes, String grantTypes, String authorities,
                                String redirectUris) {
		super(  clientId,   resourceIds,
				  scopes,   grantTypes,   authorities,
				  redirectUris);
	}
	
}
