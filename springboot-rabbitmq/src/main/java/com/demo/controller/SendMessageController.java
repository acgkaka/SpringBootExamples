package com.demo.controller;

import com.alibaba.fastjson.JSON;
import com.demo.config.RabbitDirectConfig;
import com.demo.config.RabbitFanoutConfig;
import com.demo.config.RabbitTopicConfig;
import com.demo.config.message.MyMessageVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * <p> @Title SendMessageController
 * <p> @Description 推送消息接口
 *
 * @author ACGkaka
 * @date 2023/1/12 15:23
 */
@Slf4j
@RestController
public class SendMessageController {

    /**
     * 使用 RabbitTemplate，这提供了接收/发送等方法。
     */
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/sendDirectMessage")
    public String sendDirectMessage() {
        MyMessageVO message = new MyMessageVO();
        message.setMsgId(UUID.randomUUID().toString());
        message.setData("Hello world.");
        message.setCreateTime(LocalDateTime.now());

        log.info(">>>>>>>>>>【INFO】生产者推送消息：{}", message);

        // 将消息携带绑定键值：TEST_DIRECT_ROUTING，发送到交换机：TEST_DIRECT_EXCHANGE
        rabbitTemplate.convertAndSend(RabbitDirectConfig.DIRECT_EXCHANGE_NAME, RabbitDirectConfig.DIRECT_ROUTING_NAME, JSON.toJSONString(message));
        return "OK";
    }

    @GetMapping("/sendDirectMessage2")
    public String sendDirectMessage2() {
        MyMessageVO message = new MyMessageVO();
        message.setMsgId(UUID.randomUUID().toString());
        message.setData("Hello world.");
        message.setCreateTime(LocalDateTime.now());

        log.info(">>>>>>>>>>【INFO】生产者推送消息：{}", message);

        //生成唯一标识
        CorrelationData correlationData = new CorrelationData(message.getMsgId());

        //不管成功失败都会调用confirm或者throwable,这是异步调用
        correlationData.getFuture().addCallback(
                confirm -> {
                    // 设置消息发送到交换机(Exchange)回调
                    if (confirm != null && confirm.isAck()) {
                        log.info(">>>>>>>>>>【INFO】发送成功ACK，msgId: {}, message: {}", correlationData.getId(), message);
                    } else {
                        log.error(">>>>>>>>>>【ERROR】发送失败NACK，msgId: {}, message: {}", correlationData.getId(), message);
                    }
                },
                throwable -> {
                    //发生错误，链接mq异常，mq未打开等...报错回调
                    System.out.println("发送失败throwable = " + throwable + ",  id:" + correlationData.getId());
                }
        );

        // 将消息携带绑定键值：TEST_DIRECT_ROUTING，发送到交换机：TEST_DIRECT_EXCHANGE
        rabbitTemplate.convertAndSend(RabbitDirectConfig.DIRECT_EXCHANGE_NAME, RabbitDirectConfig.DIRECT_ROUTING_NAME, message, correlationData);
        return "OK";
    }

    @GetMapping("/sendFanoutMessage")
    public String sendFanoutMessage() {
        MyMessageVO message = new MyMessageVO();
        message.setMsgId(UUID.randomUUID().toString());
        message.setData("Hello world.");
        message.setCreateTime(LocalDateTime.now());

        log.info(">>>>>>>>>>【INFO】生产者推送消息：{}", message);

        // 将消息携带绑定键值：TEST_FANOUT_ROUTING，发送到交换机：TEST_FANOUT_EXCHANGE
        rabbitTemplate.convertAndSend(RabbitFanoutConfig.FANOUT_EXCHANGE_NAME, RabbitFanoutConfig.FANOUT_ROUTING_NAME, message);
        return "OK";
    }

    @GetMapping("/sendTopicMessage")
    public String sendTopicMessage() {
        MyMessageVO message = new MyMessageVO();
        message.setMsgId(UUID.randomUUID().toString());
        message.setData("Hello world.");
        message.setCreateTime(LocalDateTime.now());

        log.info(">>>>>>>>>>【INFO】生产者推送消息：{}", message);

        // 将消息携带绑定键值：test，发送到交换机：TEST_TOPIC_EXCHANGE
        rabbitTemplate.convertAndSend(RabbitTopicConfig.TOPIC_EXCHANGE_NAME, RabbitTopicConfig.TOPIC_ROUTING_NAME_1, message);
        return "OK";
    }

}
