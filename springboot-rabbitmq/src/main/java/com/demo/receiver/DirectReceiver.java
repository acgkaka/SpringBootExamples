package com.demo.receiver;

import com.alibaba.fastjson2.JSON;
import com.demo.config.RabbitDirectConfig;
import com.demo.config.message.MyMessageVO;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * <p> @Title DirectReceiver
 * <p> @Description 直连交换机监听类
 *
 * @author ACGkaka
 * @date 2023/1/12 15:59
 */
@Slf4j
@Component
public class DirectReceiver {

    @RabbitListener(queues = RabbitDirectConfig.DIRECT_QUEUE_NAME)
    public void process(String body, Message message, Channel channel) throws IOException {
        try {
            log.info("DirectReceiver消费者收到消息: {}", body);

            MyMessageVO myMessageVO = JSON.parseObject(body, MyMessageVO.class);
            log.info("反序列化后: {}", myMessageVO);

            // 手动答应消费完成，从队列中删除该消息
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);

        } catch (Exception e) {
            log.error("DirectReceiver消费者消费失败，原因: {}", e.getMessage(), e);

            // 手动答应消费完成，从队列中删除该消息（不重回队列）
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),false);
        }
    }

}
