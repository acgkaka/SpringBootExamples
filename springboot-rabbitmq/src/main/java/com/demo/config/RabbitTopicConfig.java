package com.demo.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p> @Title RabbitTopicConfig
 * <p> @Description 主题交换机配置类
 *
 * @author ACGkaka
 * @date 2023/1/16 6:02
 */
@Configuration
public class RabbitTopicConfig {

    public static final String TOPIC_EXCHANGE_NAME = "TEST_TOPIC_EXCHANGE";
    public static final String TOPIC_ROUTING_NAME_1 = "test";
    public static final String TOPIC_ROUTING_NAME_2 = "test.topic";
    public static final String TOPIC_ROUTING_NAME_3 = "test.topic.message";
    public static final String TOPIC_QUEUE_NAME_1 = "TEST_TOPIC_QUEUE_1";
    public static final String TOPIC_QUEUE_NAME_2 = "TEST_TOPIC_QUEUE_2";
    public static final String TOPIC_QUEUE_NAME_3 = "TEST_TOPIC_QUEUE_3";

    @Bean
    public Queue testTopicQueue1() {
        return QueueBuilder.durable(TOPIC_QUEUE_NAME_1).build();
    }
    @Bean
    public Queue testTopicQueue2() {
        return QueueBuilder.durable(TOPIC_QUEUE_NAME_2).build();
    }
    @Bean
    public Queue testTopicQueue3() {
        return QueueBuilder.durable(TOPIC_QUEUE_NAME_3).build();
    }
    /**
     * 交换机(Exchange) 描述：接收消息并且转发到绑定的队列，交换机不存储消息
     */
    @Bean
    TopicExchange testTopicExchange() {
        return new TopicExchange(TOPIC_EXCHANGE_NAME, true, false);
    }

    /**
     * 綁定队列 testTopicQueue1() 到 testTopicExchange 交换机,路由键只接受完全匹配 test.topic1 的队列接受者可以收到消息
     */
    @Bean
    Binding bindingTestTopic1(Queue testTopicQueue1, TopicExchange testTopicExchange) {
        return BindingBuilder.bind(testTopicQueue1).to(testTopicExchange).with(TOPIC_ROUTING_NAME_1);
    }
    @Bean
    Binding bindingTestTopic2(Queue testTopicQueue2, TopicExchange testTopicExchange) {
        return BindingBuilder.bind(testTopicQueue2).to(testTopicExchange).with(TOPIC_ROUTING_NAME_2);
    }
    @Bean
    Binding bindingTestTopic3(Queue testTopicQueue3, TopicExchange testTopicExchange) {
        return BindingBuilder.bind(testTopicQueue3).to(testTopicExchange).with(TOPIC_ROUTING_NAME_3);
    }
}
