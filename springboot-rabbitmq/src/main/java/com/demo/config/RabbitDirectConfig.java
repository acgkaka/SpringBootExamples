package com.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p> @Title RabbitDirectConfig
 * <p> @Description 直连交换机配置
 * Direct Exchange是RabbitMQ默认的交换机模式，也是最简单的模式，根据key全文匹配去寻找队列。
 *
 * @author ACGkaka
 * @date 2023/1/12 15:09
 */
@Slf4j
@Configuration
public class RabbitDirectConfig {

    public static final String DIRECT_EXCHANGE_NAME = "TEST_DIRECT_EXCHANGE";
    public static final String DIRECT_ROUTING_NAME = "TEST_DIRECT_ROUTING";
    public static final String DIRECT_QUEUE_NAME = "TEST_DIRECT_QUEUE";

    @Autowired
    private SimpleRabbitListenerContainerFactoryConfigurer configurer;

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate();
        rabbitTemplate.setConnectionFactory(connectionFactory);
        // 设置开启Mandatory,才能触发回调函数,无论消息推送结果怎么样都强制调用回调函数
        rabbitTemplate.setMandatory(true);
        //设置message序列化方法（不建议指定序列化方法，JDK8的LocalDateTime会反序列化异常，FastJSON会存在反序列化漏洞）
//        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());

        // 设置消息发送到交换机回调
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            if (ack) {
                log.info(">>>>>>>>>>【INFO】消息发送到交换机成功, 相关数据: {}", correlationData);
            } else {
                log.error(">>>>>>>>>>【ERROR】消息发送到交换机失败, 错误原因: {}, 相关数据: {}", cause, correlationData);
            }
        });

        // 设置消息发送到队列回调（经测试，只有失败才会调用）
        rabbitTemplate.setReturnsCallback((returnedMessage) -> {
            log.error(">>>>>>>>>>【ERROR】消息发送到队列失败：响应码: {}, 响应信息: {}, 交换机: {}, 路由键: {}, 消息内容: {}",
                    returnedMessage.getReplyCode(), returnedMessage.getReplyText(), returnedMessage.getExchange(), returnedMessage.getRoutingKey(), returnedMessage.getMessage());
        });
        return rabbitTemplate;
    }

    /**
     * 消息监听配置
     */
    @Bean
    public RabbitListenerContainerFactory<?> rabbitListenerContainerFactory(ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        // 设置连接工厂
        factory.setConnectionFactory(connectionFactory);
        // 采用yaml中的配置
        configurer.configure(factory, connectionFactory);
        // 设置消息确认模式
//        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        // 设置反序列化（不建议指定序列化方法，JDK8的LocalDateTime会反序列化异常，FastJSON会存在反序列化漏洞）
//        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        return factory;
    }

    /**
     * 队列，命名：testDirectQueue
     *
     * @return 队列
     */
    @Bean
    public Queue testDirectQueue() {
        // durable:是否持久化，默认是false，持久化队列：会被存储在磁盘上，当消息代理重启时仍然存在，暂存队列：当前连接有效
        // exclusive:默认false，只能被当前创建的连接使用，而且当连接关闭后队列即被删除。此参考优先级高于durable。
        // autoDelete:是否自动删除，当没有生产者或消费者使用此队列，该队列会自动删除。

        // 一般设置一下队列的持久化就好，其余两个默认false
//        return new Queue(DIRECT_QUEUE_NAME, true);

        return QueueBuilder.durable(DIRECT_QUEUE_NAME).build();
    }

    /**
     * Direct交换机，命名：testDirectExchange
     * @return Direct交换机
     */
    @Bean
    DirectExchange testDirectExchange() {
        return new DirectExchange(DIRECT_EXCHANGE_NAME, true, false);
    }

    /**
     * 绑定 将队列和交换机绑定，并设置用于匹配键：testDirectRouting
     * @return 绑定
     */
    @Bean
    Binding bindingDirect() {
        return BindingBuilder.bind(testDirectQueue()).to(testDirectExchange()).with(DIRECT_ROUTING_NAME);
    }
}
