package com.demo.controller;

import com.demo.common.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> @Title DemoController
 * <p> @Description 测试Controller
 *
 * @author ACGkaka
 * @date 2025/2/7 11:46
 */
@RestController
public class DemoController {

    @RequestMapping("test")
    @ResponseBody
    public Result<Object> test() {
        return Result.succeed();
    }
}
