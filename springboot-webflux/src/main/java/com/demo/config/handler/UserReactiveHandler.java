package com.demo.config.handler;

import com.demo.entity.User;
import com.demo.service.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

/**
 * <p> @Title UserReactiveHandler
 * <p> @Description 配置 WebFlux 接口
 *
 * @author ACGkaka
 * @date 2024/3/20 11:40
 */
@Slf4j
@Component
public class UserReactiveHandler {

    @Resource
    private DemoService demoService;

    /**
     * 添加用户
     */
    public Mono<ServerResponse> addUser(ServerRequest request) {
        log.info(">>>>>>>>>> 方法 addUser() 被调用了");
        // 获取入参
        Mono<User> userParam = request.bodyToMono(User.class);
        return userParam.flatMap(dto -> {
            // 校验代码需要放在这里
            if (StringUtils.isEmpty(dto.getUsername())) {
                throw new IllegalArgumentException("用户名不能为空");
            }

            return ok().contentType(MediaType.APPLICATION_JSON)
                    .body(Mono.create(cityMonoSink -> cityMonoSink.success(demoService.addUser(dto))), User.class);
        });
    }

    /**
     * 获取用户列表
     */
    public Mono<ServerResponse> userList(ServerRequest request) {
        log.info(">>>>>>>>>> 方法 userList() 被调用了");
        return ok().contentType(MediaType.APPLICATION_JSON)
                .body(Flux.fromIterable(demoService.findAllUser()), User.class);
    }

    /**
     * 根据 id 查询用户
     */
    public Mono<ServerResponse> findUserById(ServerRequest request) {
        log.info(">>>>>>>>>> findUserById() 被调用了");
        // 获取入参方式一：路径传参
        String idStr = request.pathVariable("id");
        // 获取入参方式二：query传参
//        String idStr = request.queryParam("id").get();
        Long id = Long.parseLong(idStr);
        // 校验代码需要放在这里
        if (StringUtils.isEmpty(id)) {
            throw new IllegalArgumentException("id不能为空");
        }

        return ok().contentType(MediaType.APPLICATION_JSON)
                .body(Mono.create(cityMonoSink -> cityMonoSink.success(demoService.findUserById(id))), User.class);
    }


}
