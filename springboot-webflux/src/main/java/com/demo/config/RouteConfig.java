package com.demo.config;

import com.demo.config.handler.UserReactiveHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import javax.annotation.Resource;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;

/**
 * <p> @Title RouteConfig
 * <p> @Description 路由配置
 *
 * @author ACGkaka
 * @date 2024/3/21 13:39
 */
@Configuration
public class RouteConfig {
    @Resource
    private UserReactiveHandler handler;

    @Bean
    public RouterFunction<ServerResponse> routes() {
        // 下面的操作相当于 @RequestMapping
        return RouterFunctions.route(POST("/addUser"), handler::addUser)
                .andRoute(GET("/userList"), handler::userList)
                .andRoute(GET("/findUserById/{id}"), handler::findUserById);
    }
}
