package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> @Title User
 * <p> @Description 用户信息
 *
 * @author ACGkaka
 * @date 2024/3/21 11:12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    /**
     * 主键
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 年龄
     */
    private Integer age;
}
