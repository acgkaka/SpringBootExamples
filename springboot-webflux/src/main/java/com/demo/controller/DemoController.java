package com.demo.controller;

import com.demo.service.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

/**
 * <p> @Title DemoController
 * <p> @Description 测试Controller
 *
 * @author ACGkaka
 * @date 2023/4/24 18:02
 */
@Slf4j
@RestController
@RequestMapping("/demo")
public class DemoController {

    @Resource
    private DemoService demoService;

    /**
     * webflux接口测试（返回 0个 或 1个结果）
     */
    @GetMapping("/monoTest")
    public Mono<Object> monoTest() {
        /// 写法一：命令式写法
//        String data = getOneResult("monoTest()");
//        return Mono.just(data);

        // 写法二：响应式写法（语句需要在流中执行）
        return Mono.create(cityMonoSink -> {
            String data = demoService.getOneResult("monoTest()");
            cityMonoSink.success(data);
        });
    }

    /**
     * webflux接口测试（返回 0个 或 多个结果）
     */
    @GetMapping("/fluxTest")
    public Flux<Object> fluxTest() {
        // 写法一：命令式写法
//        List<String> list = getMultiResult("fluxTest()");
//        return Flux.fromIterable(list);

        // 写法二：响应式写法（语句需要在流中执行）
        return Flux.fromIterable(demoService.getMultiResult("fluxTest()"));
    }

}
