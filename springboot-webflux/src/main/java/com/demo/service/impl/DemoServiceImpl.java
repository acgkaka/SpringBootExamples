package com.demo.service.impl;

import com.demo.entity.User;
import com.demo.service.DemoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> @Title DemoServiceImpl
 * <p> @Description 测试ServiceImpl
 *
 * @author ACGkaka
 * @date 2024/3/20 11:46
 */
@Service
public class DemoServiceImpl implements DemoService {

    @Override
    public String getOneResult(String methodName) {
        // 模拟业务处理，返回单个结果
        return String.format("%s方法调用成功", methodName);
    }

    @Override
    public List<String> getMultiResult(String methodName) {
        // 模拟业务处理，返回多个结果
        List<String> list = new ArrayList<>(3);
        for (int i = 0; i < 3; i++) {
            list.add(String.format("%s方法调用成功，第 %d 条", methodName, i + 1));
        }
        return list;
    }

    @Override
    public User addUser(User user) {
        // 添加用户
        user.setId(1L);
        return user;
    }

    @Override
    public List<User> findAllUser() {
        // 查询所有用户
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            int no = i + 1;
            list.add(new User((long) no, "USER_" + no, "PWD_" + no, 18 + no));
        }
        return list;
    }

    @Override
    public User findUserById(Long id) {
        // 根据 id 查询用户
        return new User(id, "USER_" + id, "PWD_" + id, 18);
    }
}
