package com.demo.service;

import com.demo.entity.User;

import java.util.List;

/**
 * <p> @Title DemoService
 * <p> @Description 测试Service
 *
 * @author ACGkaka
 * @date 2024/3/20 11:46
 */
public interface DemoService {

    /**
     * 模拟业务处理，返回单个结果
     */
    String getOneResult(String methodName);

    /**
     * 模拟业务处理，返回多个结果
     */
    List<String> getMultiResult(String methodName);

    /**
     * 添加用户
     */
    User addUser(User user);

    /**
     * 查询所有用户
     */
    List<User> findAllUser();

    /**
     * 根据 id 查询用户
     */
    User findUserById(Long id);

}
