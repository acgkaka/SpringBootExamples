package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SpringbootDemoApplication {

    /**
     * 可以使用以下两种方式创建 ApplicationContext
     */
    public static void main(String[] args) {
        // 方式一
//        SpringApplication.run(SpringbootDemoApplication.class, args);

        // 方式二：使用 SpringApplicationBuilder 来创建 SpringApplication。
        // builder 提供了链式调用 API，更加方便，可读性更强。
        SpringApplicationBuilder builder = new SpringApplicationBuilder()
                .web(WebApplicationType.REACTIVE).sources(SpringbootDemoApplication.class);
        builder.run(args);
    }

}
