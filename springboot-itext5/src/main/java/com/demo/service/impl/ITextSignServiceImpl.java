package com.demo.service.impl;

import com.demo.constant.Constant;
import com.demo.dto.SignInfo;
import com.demo.dto.SignDetail;
import com.demo.service.ITextSignService;
import com.demo.util.ImageUtil;
import com.demo.util.KeywordPDFUtils;
import com.demo.util.PdfSignUtil;
import com.demo.util.model.PDFKeywordDTO;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfReader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.*;

/**
 * <p> @Title ITextSignServiceImpl
 * <p> @Description iText 签署测试
 *
 * @author ACG kaka
 * @date 2024/10/8 17:20
 */
@Slf4j
@Service
public class ITextSignServiceImpl implements ITextSignService {
    @Override
    public void signByXy() throws IOException {
        // 坐标签署
        Map<String, Object> signCert = getSignCert();
        PrivateKey pk0 = (PrivateKey) signCert.get("pk");
        Certificate[] chain0 = (Certificate[]) signCert.get("chain");

        // 签署信息
        SignInfo signInfo = new SignInfo();
        signInfo.setPage(1);
        signInfo.setX(30.0F);
        signInfo.setY(30.0F);
        signInfo.setSignW(200.0F);
        signInfo.setSignH(100.0F);
        signInfo.setRotate(0f);
        signInfo.setType("sign");

        List<Map<String, Object>> itemList = new ArrayList<>();
        itemList.add(getItem(signInfo));

        byte[] oldPdfData = FileUtils.readFileToByteArray(new File("D:\\test(1).pdf"));
        byte[] newPdfData = PdfSignUtil.signPDFBySeal(oldPdfData, chain0, pk0, itemList);
        FileUtils.writeByteArrayToFile(new File("D:\\test(1)-sign.pdf"), newPdfData);
        log.info(">>>>>>>>>>【INFO】坐标签署成功");
    }

    @Override
    public void signByKey() throws Exception {
        // 关键字签署
        // 识别关键字坐标
        byte[] bytes = FileUtils.readFileToByteArray(new File("D:\\test(1).pdf"));
        String base64 = Base64.encodeBase64String(bytes);
        List<PDFKeywordDTO> keyWords = KeywordPDFUtils.getKeywords(base64, "This", 1, 1, 1);
        System.out.println("keyWords: " + keyWords);
        PDFKeywordDTO keyword;

        // 关键字坐标转换
        if (keyWords != null && keyWords.size() > 0) {
            keyword = keyWords.get(0);
            float x;
            float y;
            // 偏移量
            float keyWordTop = 0.0f;
            float keyWordBottom = 0.0f;
            if (keyWordTop != 0.0f && !Float.isNaN(keyWordTop)) {
                y = 812 - keyword.getY() - keyWordTop;
            } else {
                y = 812 - keyword.getY() + keyWordBottom;
            }
            float keyWordLeft = 0.0f;
            float keyWordRight = 0.0f;
            if (keyWordLeft != 0.0f && !Float.isNaN(keyWordLeft)) {
                x = keyword.getX() - keyWordLeft;
            } else {
                x = keyword.getX() + keyWordRight;
            }
            keyword.setX(x);
            keyword.setY(y);
        } else {
            throw new RuntimeException("未找到对应的关键字。");
        }

        // 加载证书
        Map<String, Object> signCert = getSignCert();
        PrivateKey pk0 = (PrivateKey) signCert.get("pk");
        Certificate[] chain0 = (Certificate[]) signCert.get("chain");

        // 签署信息
        SignInfo signInfo = new SignInfo();
        signInfo.setPage(keyword.getPage());
        signInfo.setX(keyword.getX());
        signInfo.setY(keyword.getY());
        signInfo.setSignW(200.0F);
        signInfo.setSignH(100.0F);
        signInfo.setRotate(0f);
        signInfo.setType("sign");

        List<Map<String, Object>> itemList = new ArrayList<>();
        itemList.add(getItem(signInfo));

        byte[] oldPdfData = FileUtils.readFileToByteArray(new File("D:\\test(1).pdf"));
        byte[] newPdfData = PdfSignUtil.signPDFBySeal(oldPdfData, chain0, pk0, itemList);
        FileUtils.writeByteArrayToFile(new File("D:\\test(1)-sign.pdf"), newPdfData);
        log.info(">>>>>>>>>>【INFO】关键字签署成功");
    }

    @Override
    public void dateSign() throws Exception {
        // 日期签署
        Map<String, Object> signCert = getSignCert();
        PrivateKey pk0 = (PrivateKey) signCert.get("pk");
        Certificate[] chain0 = (Certificate[]) signCert.get("chain");

        // 签署信息
        SignInfo signInfo = new SignInfo();
        signInfo.setPage(1);
        signInfo.setX(231.66F);
        signInfo.setY(287.0F);
        signInfo.setSignW(130.0F);
        signInfo.setSignH(20.0F);
        signInfo.setRotate(0f);
        signInfo.setType("sign");

        // 日期信息
        SignDetail valSetting = new SignDetail();
        valSetting.setContent("2024-10-30");
        valSetting.setFontSize(14F);
        valSetting.setFontColor("#000000");
        valSetting.setTextAlign("center");
        valSetting.setTextVlign("middle");
        signInfo.setSignDetail(valSetting);

        // 签署PDF
        List<Map<String, Object>> itemList = new ArrayList<>();
        itemList.add(getItem(signInfo));

        byte[] oldPdfData = FileUtils.readFileToByteArray(new File("D:\\test(1).pdf"));
        byte[] newPdfData = PdfSignUtil.signPDFBySeal(oldPdfData, chain0, pk0, itemList);
        FileUtils.writeByteArrayToFile(new File("D:\\test(1)-sign.pdf"), newPdfData);

        log.info(">>>>>>>>>>【INFO】日期签署成功");
    }


    @Override
    public void crossSignBySeal() throws Exception {
        // 骑缝章签署
        Map<String, Object> signCert = getSignCert();
        PrivateKey pk0 = (PrivateKey) signCert.get("pk");
        Certificate[] chain0 = (Certificate[]) signCert.get("chain");

        // 签署信息
        SignInfo signInfo = new SignInfo();
        signInfo.setPage(1);
        signInfo.setX(90.0F);
        signInfo.setY(90.0F);
        signInfo.setSignW(100.0F);
        signInfo.setSignH(100.0F);
        signInfo.setRotate(0f);
        signInfo.setType("seal");

        List<Map<String, Object>> itemList = new ArrayList<>();
        itemList.add(getItem(signInfo));
        byte[] oldPdfData = FileUtils.readFileToByteArray(new File("D:\\test_2页.pdf"));

        // 获取PDF页数
        PdfReader reader;
        try {
            reader = new PdfReader(oldPdfData);
        } catch (Exception e) {
            log.error("读取pdf发生异常，{}", e.getMessage());
            throw new IllegalArgumentException("读取pdf失败");
        }
        int pdfPages = reader.getNumberOfPages();

        // 根据PDF页数切割印章
        Image[] images;
        String straddleMode = "R";
        try {
            byte[] sealBytes = (byte[]) itemList.get(0).get("image");
            images = ImageUtil.subImages(sealBytes, pdfPages, (float) 50, straddleMode);
        } catch (Exception e) {
            log.error("骑缝章图片处理发生异常，{}", e.getMessage(), e);
            throw new IllegalArgumentException("骑缝章图片处理发生异常");
        }

        // 签署PDF
        String rules = (String) itemList.get(0).get("rules");
        byte[] newPdfData = PdfSignUtil.crossSignBySeal(oldPdfData, rules, chain0, pk0, images, straddleMode);
        FileUtils.writeByteArrayToFile(new File("D:\\test(1)-sign.pdf"), newPdfData);
        log.info(">>>>>>>>>>【INFO】骑缝章签署成功");
    }


    @Override
    public void textareaSign() throws Exception {
        // 文本域签署
        Map<String, Object> signCert = getSignCert();
        PrivateKey pk0 = (PrivateKey) signCert.get("pk");
        Certificate[] chain0 = (Certificate[]) signCert.get("chain");

        // 签署信息
        SignInfo signInfo = new SignInfo();
        signInfo.setPage(1);
        signInfo.setX(150.0F);
        signInfo.setY(300.0F);
        signInfo.setSignW(250.0F);
        signInfo.setSignH(100.0F);
        signInfo.setRotate(0f);
        signInfo.setType("sign");

        // 日期信息
        SignDetail valSetting = new SignDetail();
        valSetting.setContent("这是一段文本测试。这是一段文本测试。这是一段文本测试。这是一段文本测试。这是一段文本测试。这是一段文本测试。这是一段文本测试。");
        valSetting.setFontSize(13F);
        valSetting.setFontColor("#000000");
        valSetting.setTextAlign("left");
        valSetting.setTextVlign("top");
        valSetting.setFontFamily("宋体");
        signInfo.setSignDetail(valSetting);

        // 签署PDF
        List<Map<String, Object>> itemList = new ArrayList<>();
        itemList.add(getItem(signInfo));

        byte[] oldPdfData = FileUtils.readFileToByteArray(new File("D:\\test(1).pdf"));
        byte[] newPdfData = PdfSignUtil.signPDFBySeal(oldPdfData, chain0, pk0, itemList);
        FileUtils.writeByteArrayToFile(new File("D:\\test(1)-sign.pdf"), newPdfData);

        log.info(">>>>>>>>>>【INFO】文本域签署成功");
    }

    public static void main(String[] args) throws Exception {
        // 文本域签署
        Map<String, Object> signCert = getSignCert();
        PrivateKey pk0 = (PrivateKey) signCert.get("pk");
        Certificate[] chain0 = (Certificate[]) signCert.get("chain");

        // 签署信息
        SignInfo signInfo = new SignInfo();
        signInfo.setPage(1);
        signInfo.setX(150.0F);
        signInfo.setY(300.0F);
        signInfo.setSignW(250.0F);
        signInfo.setSignH(100.0F);
        signInfo.setRotate(0f);
        signInfo.setType("sign");

        // 日期信息
        SignDetail valSetting = new SignDetail();
        valSetting.setContent("这是一段文本测试。这是一段文本测试。这是一段文本测试。这是一段文本测试。这是一段文本测试。这是一段文本测试。这是一段文本测试。");
        valSetting.setFontSize(13F);
        valSetting.setFontColor("#000000");
        valSetting.setTextAlign("left");
        valSetting.setTextVlign("top");
        valSetting.setFontFamily("宋体");
        signInfo.setSignDetail(valSetting);

        // 签署PDF
        List<Map<String, Object>> itemList = new ArrayList<>();
        itemList.add(getItem(signInfo));

        byte[] oldPdfData = FileUtils.readFileToByteArray(new File("D:\\test(1).pdf"));
        byte[] newPdfData = PdfSignUtil.signPDFBySeal(oldPdfData, chain0, pk0, itemList);
        FileUtils.writeByteArrayToFile(new File("D:\\test(1)-sign.pdf"), newPdfData);

        log.info(">>>>>>>>>>【INFO】文本域签署成功");
    }

    /**
     * 获取签署信息
     */
    private static Map<String, Object> getItem(SignInfo signInfo) {
        Map<String, Object> itemMap = new HashMap<>();

        itemMap.put("rules", signInfo.getPage() + "," + signInfo.getX() + "," + signInfo.getY() + "," +
                signInfo.getSignW() + "," + signInfo.getSignH() + "," +
                signInfo.getRotate());
        itemMap.put("valSetting", signInfo.getSignDetail());

        try {
            if ("sign".equalsIgnoreCase(signInfo.getType())) {
                Image image = Image.getInstance(FileUtils.readFileToByteArray(new File("D:\\signature.png")));
                itemMap.put("image", image);
            } else if ("seal".equalsIgnoreCase(signInfo.getType())) {
                byte[] image = FileUtils.readFileToByteArray(new File("D:\\seal.png"));
                itemMap.put("image", image);
            }
        } catch (BadElementException | IOException e) {
            log.error("读取签名图片发生异常，signInfo.getSealId()：{}", signInfo.getSealId(), e);
            throw new IllegalArgumentException("读取签名图片失败");
        } catch (IllegalArgumentException e){
            throw e;
        }
        return itemMap;
    }

    /**
     * 获取证书
     */
    private static Map<String, Object> getSignCert() throws IOException {
        // 获取证书
        byte[] certBytes = FileUtils.readFileToByteArray(new File("D:\\certificate (2).p12"));

        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(certBytes);) {

            KeyStore ks = KeyStore.getInstance("PKCS12");
            char[] PASSWORD = Constant.CA_ENCRY_KEY.toCharArray();
            ks.load(inputStream, PASSWORD);
            String alias = ks.aliases().nextElement();
            PrivateKey pk = (PrivateKey) ks.getKey(alias, PASSWORD);
            Certificate[] chain = ks.getCertificateChain(alias);

            Map<String, Object> signCert = new HashMap<String, Object>();
            signCert.put("chain", chain);
            signCert.put("pk", pk);

            return signCert;
        } catch (UnrecoverableKeyException | CertificateException | KeyStoreException | IOException |
                 NoSuchAlgorithmException e) {
            log.error("读取证书异常，{}", e.getMessage(), e);
            throw new IllegalArgumentException("读取证书失败");
        }
    }
}
