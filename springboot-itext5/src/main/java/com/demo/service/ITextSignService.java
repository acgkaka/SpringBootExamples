package com.demo.service;

import java.io.IOException;

/**
 * <p> @Title ITextSignService
 * <p> @Description iText 签署测试
 *
 * @author ACG kaka
 * @date 2024/10/8 17:20
 */
public interface ITextSignService {

    /**
     * 坐标签署
     */
    void signByXy() throws IOException;

    /**
     * 关键字签署
     */
    void signByKey() throws Exception;

    /**
     * 日期签署
     */
    void dateSign() throws Exception;

    /**
     * 骑缝章签署
     */
    void crossSignBySeal() throws Exception;

    /**
     * 文本域签署
     */
    void textareaSign() throws Exception;
}
