package com.demo.constant;


/**
 * <p> @Title Constant
 * <p> @Description 常量类
 *
 * @author ACGkaka
 * @date 2024/8/15 11:36
 */
public class Constant {

    /**
     * 时间戳配置
     */
    public static final Integer TSA_RFC3161_ENABLE = 1; // 是否启用时间戳
    public static final Integer TSA_RFC3161_AUTH_ENABLE = 0; // 时间戳接口是否需要验证用户名、密码
    public static final String TSA_RFC3161_USERNAME = null; // 时间戳接口用户名
    public static final String TSA_RFC3161_PASSWORD = null; // 时间戳接口密码
    /**
     * 时间戳接口地址（亲测7个时间戳地址均可用）
     */
    public static final String TSA_RFC3161_URL = "http://timestamp.digicert.com";
//    public static final String TSA_RFC3161_URL = "http://aatl-timestamp.globalsign.com/tsa/aohfewat2389535fnasgnlg5m23";
//    public static final String TSA_RFC3161_URL = "https://timestamp.sectigo.com";
//    public static final String TSA_RFC3161_URL = "http://timestamp.entrust.net/TSS/RFC3161sha2TS";
//    public static final String TSA_RFC3161_URL = "http://tsa.swisssign.net";
//    public static final String TSA_RFC3161_URL = "http://tsa.quovadisglobal.com/TSS/HttpTspServer";
//    public static final String TSA_RFC3161_URL = "http://timestamp.apple.com/ts01";

    /**
     * 证书密码
     */
    public static final String CA_ENCRY_KEY = "123456";

}
