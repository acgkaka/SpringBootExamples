package com.demo.util;

import com.demo.util.model.PDFKeywordDTO;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;
import com.itextpdf.text.pdf.parser.Vector;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * PDF关键字工具类
 */
@Slf4j
public class KeywordPDFUtils implements Serializable {

    /**
     * 识别关键字（根据文件路径）
     * @param filePath  文件路径
     * @param keyword   关键字
     * @param findType  印章类型（1个人 2企业）
     * @return
     * @throws Exception
     */
    public static synchronized List<PDFKeywordDTO> getKeywords(String filePath, final String keyword, int findType) throws Exception {
        byte[] inBytes = Base64.decodeBase64(filePath);
        Map<Integer, List<PDFKeywordDTO>> map = getPDFText(inBytes, keyword, findType);

        List<PDFKeywordDTO> list = new ArrayList<>();
        for (Integer pageNum : map.keySet()) {
            list.addAll(map.get(pageNum));
        }
//        Collections.sort(list); // 关键字位置排序（从上到下，从左到右）
        return list;
    }

    /**
     * 识别关键字（根据文件路径、页码）
     * @param filePath  文件路径
     * @param keyword   关键字
     * @param findType  印章类型（1个人 2企业）
     * @param beginPage 查找开始页（大于等于1，如果页数大于pdf页数，则beginPage=最后一页）
     * @param endPage   查找结束页（负数表示倒数第几页，0表示最后一页，如果负数超过pdf页数或超过beginPage，则endPage = beginPage）
     * @return
     * @throws Exception
     */
    public static synchronized List<PDFKeywordDTO> getKeywords(String filePath, final String keyword, int findType, int beginPage, int endPage) throws Exception {
        byte[] pdfArray = Base64.decodeBase64(filePath);
        Map<Integer, List<PDFKeywordDTO>> map = getPDFText(pdfArray, keyword, findType, beginPage, endPage);
        List<PDFKeywordDTO> list = new ArrayList<>();
        for (Integer pageNum : map.keySet()) {
            list.addAll(map.get(pageNum));
        }

        return list;
    }

    /**
     * 识别关键字（根据文件路径、辅助关键字）
     * @param filePath  文件路径
     * @param keyword   关键字1
     * @param keyWord2  关键字2
     * @param findType  印章类型（1个人 2企业）
     * @return
     * @throws Exception
     */
    public static synchronized List<PDFKeywordDTO> getKeywords(String filePath, final String keyword, String keyWord2, int findType) throws Exception {
        Map<Integer, List<PDFKeywordDTO>> map;
        if (StringUtils.isBlank(keyWord2)) {
            byte[] inBytes = Base64.decodeBase64(filePath);
            map = getPDFText(inBytes, keyword, findType);
        } else {
            byte[] inBytes = Base64.decodeBase64(filePath);
            map = getPDFText(inBytes, keyword, keyWord2, findType);
        }
        List<PDFKeywordDTO> list = new ArrayList<PDFKeywordDTO>();
        for (Integer pageNum : map.keySet()) {
            list.addAll(map.get(pageNum));
        }
        return list;
    }

    /**
     * 识别关键字（根据文件字节流、辅助关键字）
     * @param pdfArray  文件字节流
     * @param keyWord1  关键字1
     * @param keyWord2  关键字2
     * @param findType  印章类型（1个人 2企业）
     * @return
     * @throws Exception
     */
    public static synchronized List<PDFKeywordDTO> getKeywords(byte[] pdfArray, final String keyWord1, String keyWord2, int findType) throws Exception {
        Map<Integer, List<PDFKeywordDTO>> map;
        if (StringUtils.isBlank(keyWord2)) {
            map = getPDFText(pdfArray, keyWord1, findType);
        } else {
            map = getPDFText(pdfArray, keyWord1, keyWord2, findType);
        }
        List<PDFKeywordDTO> list = new ArrayList<>();
        for (Integer pageNum : map.keySet()) {
            list.addAll(map.get(pageNum));
        }
        return list;
    }

    /**
     * 识别关键字（根据文件字节流）
     * @param pdfArray  文件字节流
     * @param keyword   关键字
     * @param findType  印章类型（1个人 2企业）
     * @return  存储每页的关键字信息（key-页码，value-页码对应页上的关键字信息）
     * @throws IOException IO异常
     */
    public static synchronized Map<Integer, List<PDFKeywordDTO>> getPDFText(byte[] pdfArray, String keyword, int findType) throws IOException {
        // 存储每页的关键字信息（key-页码，value-页码对应页上的关键字信息）
        try {
            // 加载PDF信息
            PdfReader pdfReader = new PdfReader(pdfArray);
            int pageNum = pdfReader.getNumberOfPages();
            return getPDFTextCommon(pdfReader, keyword, findType, 0, pageNum);
        } catch (IOException e) {
            log.error("PDF关键字识别错误，原因：{}", e.getMessage(), e);
            throw e;
        }
    }

    /**
     * 识别关键字（根据文件字节流，自定义起始页）
     *
     * @param pdfArray  文件字节流
     * @param keyword   关键字
     * @param findType  印章类型（1个人 2企业）
     * @param beginPage 查找开始页（大于等于1）
     * @param endPage   查找结束页（大于等于开始页）
     * @return  存储每页的关键字信息（key-页码，value-页码对应页上的关键字信息）
     * @throws IOException IO异常
     */
    public static Map<Integer, List<PDFKeywordDTO>> getPDFText(byte[] pdfArray, String keyword, int findType, int beginPage, int endPage) throws IOException {
        try {
            // 加载PDF信息
            PdfReader pdfReader = new PdfReader(pdfArray);
            return getPDFTextCommon(pdfReader, keyword, findType, beginPage, endPage);
        } catch (IOException e) {
            log.error("PDF关键字识别错误，原因：{}", e.getMessage(), e);
            throw e;
        }
    }

    /**
     * 识别关键字（根据文件字节流、辅助关键字）
     *
     * @param pdfArray  文件字节流
     * @param keyWord1  关键字1（辅助，例如：甲方）
     * @param keyWord2  关键字2（根据关键字1，筛选举例最近的关键字2，例如：签字处）
     * @param findType  印章类型（1个人 2企业）
     * @return
     * @throws Exception
     */
    public static Map<Integer, List<PDFKeywordDTO>> getPDFText(byte[] pdfArray, String keyWord1, String keyWord2, int findType) throws Exception {
        try {
            // 查找第一个关键字
            Map<Integer, List<PDFKeywordDTO>> firstKeyWrodMap = getPDFText(pdfArray, keyWord1, findType);
            Map<Integer, List<PDFKeywordDTO>> secondKeyWrodMap = new HashMap<>();
            if (firstKeyWrodMap.isEmpty()) {
                return firstKeyWrodMap;
            }

            // 根据第1个关键字，识别距离每个关键字1最近的关键字2
            PdfReader pdfReader = new PdfReader(pdfArray);
//        PdfReader pdfReader = new PdfReader(filePath);
            PdfReaderContentParser pdfReaderContentParser = new PdfReaderContentParser(pdfReader);
            for (Integer pageNum : firstKeyWrodMap.keySet()) {
                List<PDFKeywordDTO> pagePDFKeywordDTO = firstKeyWrodMap.get(pageNum);

                // 查找第二个关键字
                List<PDFKeywordDTO> secondPDFKeywordDTO = getPDFTextCommonByPage(pdfReaderContentParser, keyWord2, findType, pageNum);

                // 遍历第一个关键字
                for (PDFKeywordDTO firstKeywordDTO : pagePDFKeywordDTO) {
                    List<PDFKeywordDTO> resultPDFKeywordDTOList = new ArrayList<>();
                    PDFKeywordDTO nearKeyBean = null;
                    double distance = 99999d;

                    // 遍历第二个关键字
                    for (PDFKeywordDTO secondKeywordDTO : secondPDFKeywordDTO) {
                        // 计算距离,先判断Y，是否在一条水平线上，再判断x 是否在同一条垂直线上
                        if (secondKeywordDTO.getY() > firstKeywordDTO.getY()) {
                            continue;
                        }
                        // 计算两点距离，筛选出距离第一个关键字最近的一个
                        double currentPointDistance = Math.abs(Math.sqrt(Math.pow(firstKeywordDTO.getX() - secondKeywordDTO.getX(), 2) + Math.pow(firstKeywordDTO.getY() - secondKeywordDTO.getY(), 2)));
                        if (distance - currentPointDistance > 0.00000001d) {
                            distance = currentPointDistance;
                            nearKeyBean = secondKeywordDTO;
                        }
                    }

                    // 如果距离关键字1最近的关键字2存在，放入集合。
                    if (nearKeyBean != null) {
                        if (secondKeyWrodMap.isEmpty()) {
                            resultPDFKeywordDTOList.add(nearKeyBean);
                            secondKeyWrodMap.put(pageNum, resultPDFKeywordDTOList);
                        } else {
                            List<PDFKeywordDTO> otherPDFKeywordDTO = secondKeyWrodMap.get(pageNum);
                            if (otherPDFKeywordDTO != null && !otherPDFKeywordDTO.isEmpty()) {
                                otherPDFKeywordDTO.add(nearKeyBean);
                                secondKeyWrodMap.put(pageNum, otherPDFKeywordDTO);
                            } else {
                                resultPDFKeywordDTOList.add(nearKeyBean);
                                secondKeyWrodMap.put(pageNum, resultPDFKeywordDTOList);
                            }
                        }
                    }

                }

            }

            return secondKeyWrodMap;
        } catch (Exception e) {
            throw e;
        }
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1");
        Map<Integer, List<String>> map = new HashMap<>();
        map.put(1, list);
        list.add("2");
        map.put(2, list);
        System.out.println(map);
    }

    /**
     * 识别关键字（根据文件字节流）
     * @param pdfReader PDF读取器
     * @param keyword   关键字
     * @param findType  印章类型（1个人 2企业）
     * @param beginPage 查找开始页（大于等于1）
     * @param endPage   查找结束页（大于等于开始页）
     * @return  存储每页的关键字信息（key-页码，value-页码对应页上的关键字信息）
     * @throws IOException IO异常
     */
    private static Map<Integer, List<PDFKeywordDTO>> getPDFTextCommon(PdfReader pdfReader, String keyword, int findType, int beginPage, int endPage) throws IOException {
        Map<Integer, List<PDFKeywordDTO>> map = new HashMap<>();
        PdfReaderContentParser pdfReaderContentParser = new PdfReaderContentParser(pdfReader);
        // 遍历每页PDF
        for (int i = beginPage; i <= endPage; i++) {
            List<PDFKeywordDTO> list = getPDFTextCommonByPage(pdfReaderContentParser, keyword, findType, i);
            if (list.size() > 0) {
                map.put(i, list);
            }
        }
        return map;
    }

    /**
     * 识别每页PDF的关键字
     *
     * @param contentParser PDF读取器
     * @param keyword       关键字
     * @param findType      印章类型（1个人 2企业）
     * @param page          当前页（大于等于1）
     * @return  存储每页的关键字信息（key-页码，value-页码对应页上的关键字信息）
     * @throws IOException IO异常
     */
    private static List<PDFKeywordDTO> getPDFTextCommonByPage(PdfReaderContentParser contentParser, String keyword, int findType, int page) throws IOException {
        List<PDFKeywordDTO> list = new ArrayList<>();
        StringBuffer pageContext = new StringBuffer();
        contentParser.processContent(page, new RenderListener() {
            /**
             * 逐字读取当页文本内容
             */
            @Override
            public void renderText(TextRenderInfo textRenderInfo) {
                String text = textRenderInfo.getText(); // 整页内容
                pageContext.append(text);
                // 匹配关键字
                if (StringUtils.isNotBlank(text) && pageContext.toString().contains(keyword)) {
                    // 左下点：下降线的开始点。
                    Vector bottomLeftPoint = textRenderInfo.getDescentLine().getStartPoint();
                    // 右上点：上升线的结束点。
                    Vector topRightPoint = textRenderInfo.getAscentLine().getEndPoint();

                    String newText = text.replace("  ", "　");

                    Rectangle rectangle;
                    // 处理同一行关键字不在块尾的情况
                    if ((text.indexOf(keyword) + keyword.length()) < text.length()) {
                        float rx = textRenderInfo.getBaseline().getLength() / newText.length() * (newText.indexOf(keyword) + keyword.length()) + bottomLeftPoint.get(Vector.I1);
                        rectangle = new Rectangle(bottomLeftPoint.get(Vector.I1), bottomLeftPoint.get(Vector.I2), rx, topRightPoint.get(Vector.I2));
                    } else {
                        rectangle = new Rectangle(bottomLeftPoint.get(Vector.I1), bottomLeftPoint.get(Vector.I2), topRightPoint.get(Vector.I1), topRightPoint.get(Vector.I2));
                    }

                    // 封装PDF中关键字识别结果
                    PDFKeywordDTO bean = new PDFKeywordDTO();
                    if (findType == 1) {
                        // 签名（矩形）
                        bean.setX(rectangle.getLeft() + rectangle.getWidth() + rectangle.getWidth() / newText.length() * 0.5f);
                        float y = rectangle.getBottom() - rectangle.getHeight() / 2;
                        bean.setY(y < 1 ? 1 : y);
                        bean.setFontHeight(rectangle.getHeight());
                    } else {
                        // 印章（圆形）
                        float x = rectangle.getLeft() + rectangle.getWidth() - rectangle.getWidth() / newText.length() * keyword.length() / 2 - 80;
                        bean.setX(x < 10 ? 10 : x);
                        bean.setY(rectangle.getBottom() - 25);
                        bean.setFontHeight(1);
                    }
                    bean.setPage(page);
                    bean.setText(text);
                    list.add(bean);
                    pageContext.setLength(0);
                }
            }

            @Override
            public void renderImage(ImageRenderInfo arg0) {}
            @Override
            public void endTextBlock() {}
            @Override
            public void beginTextBlock() {}
        });
        return list;
    }
}
