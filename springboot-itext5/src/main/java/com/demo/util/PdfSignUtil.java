package com.demo.util;

import com.demo.constant.Constant;
import com.demo.dto.SignDetail;
import com.itextpdf.text.*;
import com.itextpdf.text.exceptions.BadPasswordException;
import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.security.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.boot.system.ApplicationHome;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.*;

@Slf4j
public class PdfSignUtil {

    private static final BouncyCastleProvider provider = new BouncyCastleProvider();

    private static String fontRootPath = null;

    static {
        Security.addProvider(provider);

        ApplicationHome applicationHome = new ApplicationHome(PdfSignUtil.class);
        File jarF = applicationHome.getSource();
        fontRootPath = jarF.getParent();
    }

    public static byte[] signPDFBySeal(byte[] pdfArray, Certificate[] chain, PrivateKey pk, List<Map<String, Object>> signItemList) {
        try {
            // pdf签章开始
            PdfReader reader = new PdfReader(pdfArray);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            // 创建签章工具PdfStamper ，最后一个boolean参数：false的话，pdf文件只允许被签名一次，多次签名，最后一次有效；true的话，pdf可以被追加签名，验签工具可以识别出每次签名之后文档是否被修改
            // 参数1：PDF读取器
            // 参数2：签完章的PDF写出流。
            PdfStamper stamper = PdfStamper.createSignature(reader, os, '\u0000', null, true);
            // 获取数字签章属性对象，设定数字签章的属性
            PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
            appearance.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
            appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
            for (Map<String, Object> itemMap : signItemList) {
                String rules = (String) itemMap.get("rules");
                SignDetail signDetail = (SignDetail) itemMap.get("valSetting");
                Image image = null;

                String ruleItem[] = rules.split(",");
                int page = Integer.parseInt(ruleItem[0]);
                float pageWidth = reader.getPageSizeWithRotation(page).getWidth();
                float pageHeight = reader.getPageSizeWithRotation(page).getHeight();
                float fLeft = Float.parseFloat(ruleItem[1]); // x
                float y = Float.parseFloat(ruleItem[2]);
                float signW = Float.parseFloat(ruleItem[3]);
                float signH = Float.parseFloat(ruleItem[4]);
                float rotate = Float.parseFloat(ruleItem[5]);
                float fUp = pageHeight - y;
                float fBottom = fUp - signH;

                // 计算签名位置
                Rectangle rect = null;
                if (signDetail == null) {
                    Object cacheImage = itemMap.get("image");
                    if (cacheImage instanceof Image) {
                        image = (Image) cacheImage;
                    } else if (cacheImage instanceof byte[]) {
                        image = Image.getInstance((byte[]) cacheImage);
                    } else {
                        throw new IllegalArgumentException("不支持的印章类型");
                    }
                    rect = new Rectangle(fLeft, fBottom, fLeft + signW, fUp);
                    image.scaleToFit(rect.getWidth(), rect.getHeight());
                } else {
                    float fRight = fLeft + signW;
                    rect = new Rectangle(fLeft, fBottom, fRight, fUp);
                    image = getWatermarkedImage(stamper, signDetail.getFontSize(), signDetail.getFontFamily()
                            , signDetail.getFontColor(), signDetail.getTextAlign(), signDetail.getTextVlign(), signDetail.getContent(), rect.getWidth(), rect.getHeight());
                }
                float oldH = image.getScaledHeight();
                float oldW = image.getScaledWidth();
                if (Float.compare(0, rotate) != 0) {
                    image.setRotationDegrees(-rotate);
                    float newH = image.getScaledHeight();
                    float newW = image.getScaledWidth();
                    // 签名位置中的四个参数的分别是，图章左下角x，图章左下角y，图章右上角x，图章右上角y
                    rect.setLeft(rect.getLeft() - (newW - oldW) / 2);
                    rect.setBottom(rect.getBottom() - (newH - oldH) / 2);
                    rect.setRight(rect.getRight() + (newW - oldW) / 2);
                    rect.setTop(rect.getTop() + (newH - oldH) / 2);
                }

                // 设置签名的位置，页码，签名域名称。（注意：多次追加签名的时候，签名预名称不能一样）
                // 签名的位置，是图章相对于pdf页面的位置坐标，原点为pdf页面左下角。
                appearance.addVisibleSignature(rect, page, UUID.randomUUID().toString(), image);

            }

            appearance.setDifferentImage(true);

            // 时间戳
            TSAClient tsaClient = null;
            if (1 == Constant.TSA_RFC3161_ENABLE) {
                if (1 == Constant.TSA_RFC3161_AUTH_ENABLE) {
                    tsaClient = new TSAClientBouncyCastle(Constant.TSA_RFC3161_URL, Constant.TSA_RFC3161_USERNAME, Constant.TSA_RFC3161_PASSWORD);
                } else {
                    tsaClient = new TSAClientBouncyCastle(Constant.TSA_RFC3161_URL);
                }
            }
            ExternalDigest digest = new BouncyCastleDigest();
            ExternalSignature signature = new PrivateKeySignature(pk, DigestAlgorithms.SHA256, provider.getName());
            MakeSignature.signDetached(appearance, digest, signature, chain, null, null, tsaClient, 0, MakeSignature.CryptoStandard.CMS);

            try {
                stamper.close();
                reader.close();
            } catch (Exception e) {
                log.error("关闭pdf资源发生异常，{}", e.getMessage());
            }
            pdfArray = os.toByteArray();

            return pdfArray;

        } catch (Exception e) {
            log.error("文件签署发生异常，{}", e.getMessage(), e);
            throw new RuntimeException("文件签署发生异常");
        }
    }

    /**
     * 无证书签署
     */
    public static byte[] signPDFBySealNoCert(byte[] pdfArray, List<Map<String, Object>> signItemList) {
        try {
            // pdf签章开始
            PdfReader reader = null;
            PdfStamper stamper = null;
            PdfSignatureAppearance appearance = null;
            ByteArrayOutputStream os = null;
            reader = new PdfReader(pdfArray);
            os = new ByteArrayOutputStream();
            stamper = PdfStamper.createSignature(reader, os, '\u0000', null, true);
            appearance = stamper.getSignatureAppearance();
            appearance.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
            appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
            for (Map<String, Object> itemMap : signItemList) {
                String rules = (String) itemMap.get("rules");
                SignDetail signDetail = (SignDetail) itemMap.get("valSetting");
                Image image = null;

                String ruleItem[] = rules.split(",");
                int page = Integer.parseInt(ruleItem[0]);
                float pageWidth = reader.getPageSizeWithRotation(page).getWidth();
                float pageHeight = reader.getPageSizeWithRotation(page).getHeight();
                float fLeft = Float.parseFloat(ruleItem[1]); // x
                float y = Float.parseFloat(ruleItem[2]);
                float signW = Float.parseFloat(ruleItem[3]);
                float signH = Float.parseFloat(ruleItem[4]);
                float rotate = Float.parseFloat(ruleItem[5]);
                float fUp = pageHeight - y;
                float fBottom = fUp - signH;

                Rectangle rect = null;
                if (signDetail == null) {
                    Object cacheImage = itemMap.get("image");
                    if (cacheImage instanceof Image) {
                        image = (Image) cacheImage;
                    } else if (cacheImage instanceof byte[]) {
                        image = Image.getInstance((byte[]) cacheImage);
                    } else {
                        throw new IllegalArgumentException("不支持的印章类型");
                    }
                    rect = new Rectangle(fLeft, fBottom, fLeft + signW, fUp);
                    image.scaleToFit(rect.getWidth(), rect.getHeight());
                } else {
                    float fRight = fLeft + signW;
                    rect = new Rectangle(fLeft, fBottom, fRight, fUp);
                    image = getWatermarkedImage(stamper, signDetail.getFontSize(), signDetail.getFontFamily()
                            , signDetail.getFontColor(), signDetail.getTextAlign(), signDetail.getTextVlign(), signDetail.getContent(), rect.getWidth(), rect.getHeight());
                }
                float oldH = image.getScaledHeight();
                float oldW = image.getScaledWidth();
                if (Float.compare(0, rotate) != 0) {
                    image.setRotationDegrees(-rotate);
                    float newH = image.getScaledHeight();
                    float newW = image.getScaledWidth();
                    rect.setLeft(rect.getLeft() - (newW - oldW) / 2);
                    rect.setBottom(rect.getBottom() - (newH - oldH) / 2);
                    rect.setRight(rect.getRight() + (newW - oldW) / 2);
                    rect.setTop(rect.getTop() + (newH - oldH) / 2);
                }
                appearance.addVisibleSignature(rect, page, UUID.randomUUID().toString(), image);

            }
            appearance.setDifferentImage(true);

            try {
                stamper.close();
                reader.close();
            } catch (Exception e) {
                log.error("关闭pdf资源发生异常，{}", e.getMessage());
            }
            pdfArray = os.toByteArray();

            return pdfArray;

        } catch (Exception e) {
            log.error("文件签署发生异常，{}", e.getMessage(), e);
            throw new RuntimeException("文件签署发生异常");
        }
    }

    /**
     * 根据 证书字节数组 进行 骑缝章签署
     */
    public static Map<String, Object> crossSignBySeal(byte[] pdfArray, String rules, byte[] sealBytes, byte[] certBytes, String straddleMode) {
        PdfReader reader = null;
        try {
            reader = new PdfReader(pdfArray);
        } catch (Exception e) {
            log.error("读取pdf发生异常，{}", e.getMessage());
            throw new IllegalArgumentException("读取pdf失败");
        }
        Image[] images = null;
        try {
            images = ImageUtil.subImages(sealBytes, reader.getNumberOfPages(), (float) 50, straddleMode);
        } catch (Exception e) {
            log.error("骑缝章图片处理发生异常，{}", e.getMessage());
            throw new IllegalArgumentException("骑缝章图片处理发生异常");
        }
        Map<String, Object> crossSignMap = new HashMap<>();
        crossSignMap.put("signNumber", images == null ? 0 : images.length);
        crossSignMap.put("pdfArray", PdfSignUtil.crossSignBySeal(pdfArray, rules, certBytes, Constant.CA_ENCRY_KEY, images, straddleMode));
        return crossSignMap;
    }

    public static byte[] crossSignBySeal(byte[] pdfArray, String rules, byte[] certBytes, String certPwd, Image[] images, String straddleMode) {
        PrivateKey pk = null;
        Certificate[] chain = null;
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(certBytes);
            KeyStore ks = KeyStore.getInstance("PKCS12");
            char[] PASSWORD = certPwd.toCharArray();
            ks.load(inputStream, PASSWORD);
            String alias = (String) ks.aliases().nextElement();
            pk = (PrivateKey) ks.getKey(alias, PASSWORD);
            chain = ks.getCertificateChain(alias);
        } catch (Exception e) {
            log.error("读取证书发生异常，{}", e.getMessage());
            throw new RuntimeException("证书信息读取失败");
        }
        return crossSignBySeal(pdfArray, rules, chain, pk, images, straddleMode);
    }

    public static byte[] crossSignBySeal(byte[] pdfArray, String rules, Certificate[] chain, PrivateKey pk, Image[] images, String straddleMode) {
        try {
            // pdf签章开始
            PdfReader reader = null;
            PdfStamper stamper = null;
            PdfSignatureAppearance appearance = null;
            ByteArrayOutputStream os = null;
            reader = new PdfReader(pdfArray);
            os = new ByteArrayOutputStream();
            stamper = PdfStamper.createSignature(reader, os, '\u0000', null, true);
            appearance = stamper.getSignatureAppearance();
            appearance.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
            appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
            int page = 1;
            for (Image image : images) {
                String ruleItem[] = rules.split(",");
                float pageWidth = reader.getPageSizeWithRotation(page).getWidth();
                float pageHeight = reader.getPageSizeWithRotation(page).getHeight();
                float x = Float.parseFloat(ruleItem[1]);
                float y = Float.parseFloat(ruleItem[2]);
                float signH = Float.parseFloat(ruleItem[4]);
                image.scaleToFit(pageWidth, signH);
                float fLeft = pageWidth - image.getScaledWidth() - 4;
                float fRight = fLeft + image.getScaledWidth();
                float fUp = pageHeight - y;
                float fBottom = fUp - signH;

                Rectangle rect = new Rectangle(fLeft, fBottom, fRight, fUp);

                appearance.addVisibleSignature(rect, page, UUID.randomUUID().toString(), image);

                page++;
            }

            appearance.setDifferentImage(true);

            TSAClient tsaClient = null;
            if (1 == Constant.TSA_RFC3161_ENABLE) {
                if (1 == Constant.TSA_RFC3161_AUTH_ENABLE) {
                    tsaClient = new TSAClientBouncyCastle(Constant.TSA_RFC3161_URL, Constant.TSA_RFC3161_USERNAME, Constant.TSA_RFC3161_PASSWORD);
                } else {
                    tsaClient = new TSAClientBouncyCastle(Constant.TSA_RFC3161_URL);
                }
            }
            ExternalDigest digest = new BouncyCastleDigest();
            ExternalSignature signature = new PrivateKeySignature(pk, DigestAlgorithms.SHA256, provider.getName());
            MakeSignature.signDetached(appearance, digest, signature, chain, null, null, tsaClient, 0, MakeSignature.CryptoStandard.CMS);

            try {
                stamper.close();
                reader.close();
            } catch (Exception e) {
                log.error("关闭pdf资源发生异常，{}", e.getMessage());
            }
            pdfArray = os.toByteArray();

            return pdfArray;

        } catch (Exception e) {
            log.error("骑缝签署文件发生异常，{}", e.getMessage(), e);
            throw new RuntimeException("骑缝签署文件发生异常");
        }
    }

    public static byte[] signPDFBySeal2(byte[] pdfArray, byte[] sealImg, String ruleInfo, Certificate[] chain, PrivateKey pk) {
        try {
            ArrayList<AcroFields.FieldPosition> signPosDefList = new ArrayList();
            if (ruleInfo.isEmpty()) {
                throw new RuntimeException("x-盖章规则未定义");
            }

            char firstChar = ruleInfo.charAt(0);
            String[] ruleItem;
            Float frotate = 0.0f;
            if (firstChar >= '0' && firstChar <= '9') {
                ruleItem = ruleInfo.split(",");
                String pagestr = ruleItem[0].trim();
                String[] pageIndex = pagestr.split("\\|");
                int pageSize = pageIndex.length;
                PdfReader reader = new PdfReader(pdfArray);

                for (int i = 0; i < pageSize; ++i) {
                    String tmp = pageIndex[i];
                    if (!tmp.isEmpty()) {
                        int nPage = Integer.parseInt(tmp);
                        float pageHeight = reader.getPageSizeWithRotation(nPage).getHeight();
                        float fLeft = Float.parseFloat(ruleItem[1]);
                        float fBottom = Float.parseFloat(ruleItem[2]);
                        float fRight = Float.parseFloat(ruleItem[3]);
                        float fUp = Float.parseFloat(ruleItem[4]);
                        frotate = Float.parseFloat(ruleItem[5]);
                        AcroFields.FieldPosition signPosDef = new AcroFields.FieldPosition();
                        signPosDef.page = nPage;
                        Rectangle rectangle = new Rectangle(fLeft, pageHeight - fUp - fBottom, fLeft + fRight, pageHeight - fBottom);

                        signPosDef.position = rectangle;
                        signPosDefList.add(signPosDef);
                    }
                }
            } else if (firstChar >= '-') {
                ruleItem = ruleInfo.split(",");
                AcroFields.FieldPosition signPosDef = new AcroFields.FieldPosition();
                float fLeft = Float.parseFloat(ruleItem[1]);
                float fbottom = Float.parseFloat(ruleItem[2]);
                frotate = Float.parseFloat(ruleItem[5]);
                signPosDef.page = -1;
                signPosDef.position = new Rectangle(fLeft, fbottom, fLeft, fbottom);
                signPosDefList.add(signPosDef);
            }

            pdfArray = sign2(pdfArray, sealImg, chain, pk, "SHA-256", provider.getName(), MakeSignature.CryptoStandard.CMS, "", "", signPosDefList, frotate);
        } catch (NoSuchAlgorithmException var29) {
            log.error(var29.getMessage(), var29);
            throw new RuntimeException(var29.getMessage(), var29.getCause());
        } catch (CertificateException var30) {
            log.error(var30.getMessage(), var30);
            throw new RuntimeException(var30.getMessage(), var30.getCause());
        } catch (KeyStoreException var31) {
            log.error(var31.getMessage(), var31);
            throw new RuntimeException(var31.getMessage(), var31.getCause());
        } catch (UnrecoverableKeyException var32) {
            log.error(var32.getMessage(), var32);
            throw new RuntimeException(var32.getMessage(), var32.getCause());
        } catch (FileNotFoundException var33) {
            log.error(var33.getMessage(), var33);
            throw new RuntimeException(var33.getMessage(), var33.getCause());
        } catch (IOException var34) {
            log.error(var34.getMessage(), var34);
            throw new RuntimeException(var34.getMessage(), var34.getCause());
        } catch (Exception var35) {
            log.error(var35.getMessage(), var35);
            throw new RuntimeException(var35.getMessage(), var35.getCause());
        }

        if (pdfArray == null) {
            throw new RuntimeException("签章失败，请重试");
        }
        return pdfArray;
    }

    public byte[] signPDFBySeal2(byte[] pdfArray, byte[] sealImg, String ruleInfo, byte[] certInfo, String certPwd) {
        PrivateKey pk = null;
        Certificate[] chain = null;
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(certInfo);
            KeyStore ks = KeyStore.getInstance("PKCS12");
            char[] PASSWORD = certPwd.toCharArray();
            ks.load(inputStream, PASSWORD);
            String alias = (String) ks.aliases().nextElement();
            pk = (PrivateKey) ks.getKey(alias, PASSWORD);
            chain = ks.getCertificateChain(alias);
        } catch (Exception e) {
            log.error("读取证书发生异常，{}", e.getMessage());
            throw new RuntimeException("证书信息读取失败");
        }

        return signPDFBySeal2(pdfArray, sealImg, ruleInfo, chain, pk);
    }

    private static byte[] sign2(byte[] pdfData, byte[] sealImage, Certificate[] chain, PrivateKey pk, String digestAlgorithm,
                                String provider, MakeSignature.CryptoStandard subfilter, String reason, String location,
                                ArrayList<AcroFields.FieldPosition> signPosList, Float frotate) throws Exception {
        ByteArrayOutputStream outputStream = null;
        PdfReader readerTmp = new PdfReader(pdfData);
        int pageCount = readerTmp.getNumberOfPages();
        Iterator var16 = signPosList.iterator();

        AcroFields.FieldPosition tmpPos;
        label46:
        while (var16.hasNext()) {
            tmpPos = (AcroFields.FieldPosition) var16.next();
            if (tmpPos.page == -1) {
                signPosList.remove(0);
                int i = 1;

                while (true) {
                    if (i > pageCount) {
                        break label46;
                    }

                    AcroFields.FieldPosition pos = new AcroFields.FieldPosition();
                    pos.page = i;
                    pos.position = tmpPos.position;
                    signPosList.add(pos);
                    ++i;
                }
            }
        }

        var16 = signPosList.iterator();

        while (var16.hasNext()) {
            tmpPos = (AcroFields.FieldPosition) var16.next();
            PdfReader reader = new PdfReader(pdfData);
            if (tmpPos.page <= pageCount) {
                outputStream = new ByteArrayOutputStream();
                PdfStamper stamper = PdfStamper.createSignature(reader, outputStream, '\u0000', (File) null, true);
                PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
                appearance.setReason("数字签名，不可否认");
                appearance.setLocation("ACGkaka");
                Rectangle rect = tmpPos.position;

                if (sealImage != null) {
                    Image image = Image.getInstance(sealImage);
                    image.scaleToFit(rect.getWidth(), rect.getHeight());
                    image.setAbsolutePosition(rect.getLeft(), rect.getBottom());
                    image.setRotationDegrees(-frotate);//旋转 角度
                    appearance.setSignatureGraphic(image);
                    appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
                    float srcWidth = image.getWidth();
                    float newWidth = rect.getWidth();
                    image.scalePercent(newWidth / srcWidth * 100);
                    rect.setRight(rect.getLeft() + image.getScaledWidth());
                    rect.setTop(rect.getBottom() + image.getScaledHeight());
                }

                appearance.setVisibleSignature(rect, tmpPos.page, UUID.randomUUID().toString());

                TSAClient tsaClient = null;
                if (1 == Constant.TSA_RFC3161_ENABLE) {
                    if (1 == Constant.TSA_RFC3161_AUTH_ENABLE) {
                        tsaClient = new TSAClientBouncyCastle(Constant.TSA_RFC3161_URL, Constant.TSA_RFC3161_USERNAME, Constant.TSA_RFC3161_PASSWORD);
                    } else {
                        tsaClient = new TSAClientBouncyCastle(Constant.TSA_RFC3161_URL);
                    }
                }
                ExternalDigest digest = new BouncyCastleDigest();
                ExternalSignature signature = new PrivateKeySignature(pk, digestAlgorithm, provider);
                MakeSignature.signDetached(appearance, digest, signature, chain, null, null, tsaClient, 0, subfilter);
                pdfData = outputStream.toByteArray();
            }
        }
        return pdfData;
    }

    public static Image getWatermarkedImage(PdfStamper stamper, Float fontSize, String fontFamily,
                                            String fontColor, String textAlign, String textVlign, String content, float width, float height)
            throws Exception {
//        String fontPath = PdfSignUtil.class.getClassLoader().getResource("/font/Songti.ttf").getPath();
//        BaseFont baseFont = BaseFont.createFont(fontRootPath + "/font/Songti.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//        BaseFont baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        BaseFont baseFont;
        if (Objects.equals("宋体", fontFamily)) {
            baseFont = BaseFont.createFont("C:\\Windows\\Fonts\\simsun.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        } else {
            // 默认字体：微软雅黑
            baseFont = BaseFont.createFont("C:\\Windows\\Fonts\\msyh.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        }
        int colorValue = Integer.valueOf(fontColor.trim().replace("#", ""), 16).intValue();
        int r = colorValue >> 16 & 255;
        int g = colorValue >> 8 & 255;
        int b = colorValue >> 0 & 255;
        Font font = new Font(baseFont, fontSize, Font.NORMAL, new BaseColor(r, g, b));
        PdfTemplate template = PdfTemplate.createTemplate(stamper.getWriter(), width, height);
//        ColumnText.showTextAligned(template, Element.ALIGN_BOTTOM,
//                new Phrase(content, font), 1, 1, 0);

        Rectangle rect = new Rectangle(1, 1, width + 1, height + 1);
        Chunk chunk = new Chunk(content, font);
//        chunk.setBackground(BaseColor.YELLOW);
        chunk.setLineHeight(fontSize);
        Paragraph paragraph = new Paragraph(chunk);
        ColumnText ct = new ColumnText(template);
        if (StringUtils.equals("right", textAlign)) {
            paragraph.setAlignment(Element.ALIGN_RIGHT); // 水平靠右
        } else if (StringUtils.equals("center", textAlign)) {
            paragraph.setAlignment(Element.ALIGN_CENTER); // 水平居中
        } else {
            paragraph.setAlignment(Element.ALIGN_LEFT); // 水平靠左
        }

        PdfPTable table = new PdfPTable(1);
        table.setPaddingTop(0);
        table.setTotalWidth(width);
        table.setLockedWidth(true);
        table.getDefaultCell().setMinimumHeight(height);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.getDefaultCell().setBorderWidth(0);
        table.getDefaultCell().setPadding(0);
//        table.getDefaultCell().setBackgroundColor(BaseColor.PINK);
        if (StringUtils.equals("middle", textVlign)) {
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE); // 垂直居中
        } else if (StringUtils.equals("buttom", textVlign)) {
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM); // 垂直靠下
        } else {
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP); // 垂直靠上
        }
        PdfPCell cell = table.getDefaultCell();
        cell.addElement(paragraph);
        table.addCell(cell);
        ct.addElement(table);
        ct.setSimpleColumn(rect);
        ct.go();

        return Image.getInstance(template);
    }

    public static int checkPdf(byte[] pdfArray) {
        PdfReader pdfReader = null;
        try {
            pdfReader = new PdfReader(pdfArray);
            int pages = pdfReader.getNumberOfPages();

            // 获取字段集合
            AcroFields fields = pdfReader.getAcroFields();
            // 获取签名信息集合
            ArrayList<String> names = fields.getSignatureNames();

            if (names != null && !names.isEmpty()) {
                for (String name : names) {
                    PdfPKCS7 pkcs7 = fields.verifySignature(name);
                    if (!pkcs7.verify()) {
                        throw new IllegalArgumentException("该文件已存在数字签名，且至少一个签名已破坏");
                    }
                }
            }
            return pages;
        } catch (BadPasswordException badPasswordException) {
            throw new IllegalArgumentException("暂不支持带有密码保护的pdf");
        } catch (InvalidPdfException e) {
            log.error("校验pdf文件损坏，{}", e.getMessage(), e);
            throw new IllegalArgumentException("pdf已损坏");
        } catch (GeneralSecurityException e) {
            log.error("校验pdf签名异常，{}", e.getMessage(), e);
            throw new IllegalArgumentException("验证签名发生异常");
        } catch (IOException e) {
            log.error("校验pdf发生IO异常，{}", e.getMessage(), e);
            throw new IllegalArgumentException("pdf读取异常，请确保pdf完整并重新上传");
        } finally {
            try {
                if (pdfReader != null) {
                    pdfReader.close();
                }
            } catch (Exception e) {
                log.error("关闭pdfreader异常，{}", e.getMessage(), e);
            }
        }
    }


}
