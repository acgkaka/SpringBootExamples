package com.demo.util.model;

import lombok.Data;

import java.io.Serializable;

/**
 * PDF中关键字识别结果。
 */
@Data
public class PDFKeywordDTO implements Comparable<PDFKeywordDTO>, Serializable {
    /**
     * X坐标，单位毫米，从左往右增加。
     */
    private float x;
    /**
     * Y坐标，单位毫米，从下往上增加。
     */
    private float y;
    /**
     * 关键字所在页码，从1开始
     */
    private int page;
    /**
     * 整页内容
     */
    private String text;
    /**
     * 字体高度
     */
    private float fontHeight;

    /**
     * 用于对关键字位置排序（从上到下，从左到右）
     */
    @Override
    public int compareTo(PDFKeywordDTO o) {
        // 判断Y轴是否相同
        int i = (int) (o.getY() - this.y);
        if(i == 0){
            // 判断X轴是否相同
            return (int) (this.x - o.getX());
        }
        return i;
    }

}
