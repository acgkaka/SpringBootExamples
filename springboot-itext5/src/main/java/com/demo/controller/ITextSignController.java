package com.demo.controller;

import com.demo.common.Result;
import com.demo.service.ITextSignService;
import com.demo.util.KeywordPDFUtils;
import com.demo.util.model.PDFKeywordDTO;
import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p> @Title ITextSignController
 * <p> @Description iText 签署测试
 *
 * @author ACGkaka
 * @date 2024/8/15 11:36
 */
@RestController
@RequestMapping("/sign")
public class ITextSignController {

    @Autowired
    private ITextSignService iTextSignService;

    /**
     * 获取坐标
     */
    @GetMapping("/getXyByKey")
    public Result<Object> getXyByKey() throws Exception {
        Result<Object> result = Result.succeed();
        byte[] bytes = FileUtils.readFileToByteArray(new File("D:\\test(1).pdf"));
        String base64 = Base64.encodeBase64String(bytes);
        List<PDFKeywordDTO> keyWords = KeywordPDFUtils.getKeywords(base64, "Test", 1, 1, 1);
        System.out.println(keyWords);
        return result;
    }

    /**
     * 坐标签署
     */
    @GetMapping("/signByXy")
    public Result<Object> signByXy() throws IOException {
        Result<Object> result = Result.succeed();
        iTextSignService.signByXy();
        return result;
    }

    /**
     * 关键字签署
     */
    @GetMapping("/signByKey")
    public Result<Object> signByKey() throws Exception {
        Result<Object> result = Result.succeed();
        iTextSignService.signByKey();
        return result;
    }

    /**
     * 日期签署
     */
    @GetMapping("/dateSign")
    public Result<Object> timeSign() throws Exception {
        Result<Object> result = Result.succeed();
        iTextSignService.dateSign();
        return result;
    }

    /**
     * 骑缝章签署
     */
    @GetMapping("/crossSignBySeal")
    public Result<Object> crossSignBySeal() throws Exception {
        Result<Object> result = Result.succeed();
        iTextSignService.crossSignBySeal();
        return result;
    }

    /**
     * 文本域签署
     */
    @GetMapping("/textareaSign")
    public Result<Object> textareaSign() throws Exception {
        Result<Object> result = Result.succeed();
        iTextSignService.textareaSign();
        return result;
    }


    /** 测试获取坐标 */
    public static void main(String[] args) throws Exception {
        byte[] bytes = FileUtils.readFileToByteArray(new File("D:\\test(1).pdf"));
        String base64 = Base64.encodeBase64String(bytes);
        List<PDFKeywordDTO> keyWords = KeywordPDFUtils.getKeywords(base64, "Test", 1, 1, 1);
        System.out.println(keyWords);
    }

}
