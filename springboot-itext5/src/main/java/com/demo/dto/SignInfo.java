package com.demo.dto;


import lombok.Data;

import java.io.Serializable;

/**
 *
 * 签名信息
 * </p>
 *
 * @author zhj
 * @since 2021-08-20
 */
@Data
public class SignInfo implements Serializable {

    /**
     * 签章页码
     */
    private Integer page;

    /**
     * x坐标
     */
    private Float x;

    /**
     * y坐标
     */
    private Float y;

    /**
     * 签名ID
     */
    private Long sealId;

    /**
     * 宽
     */
    private Float signW;

    /**
     * 高
     */
    private Float signH;

    /**
     *  pdf文件页高
     */
    private Float pageHeight = 842f;

    /**
     * 角度旋转
     */
    private Float rotate;

    /**
     * 类型 （textarea文本 sign个人签字 seal机构签章 time时间 crossPage骑缝章 checkbox多选 radio单选）
     */
    private String type;

    /**
     * 用于textarea time类型的子属性
     */
    private SignDetail signDetail;

}
