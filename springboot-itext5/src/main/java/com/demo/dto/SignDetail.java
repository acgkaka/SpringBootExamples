package com.demo.dto;


import lombok.Data;

@Data
public class SignDetail {

    /**
     * 字体大小、控件大小
     */
    private Float fontSize;

    /**
     * 字体颜色，默认黑色
     */
    private String fontColor = "#000000";

    /**
     * 字体名称
     */
    private String fontFamily;

    /**
     * 水平对齐方式（left/right/center）
     */
    private String textAlign;

    /**
     * 垂直对齐方式（top/middle/buttom）
     */
    private String textVlign;

    /**
     * 内容
     */
    private String content;

}
