package com.demo.decoder.method1;

import com.demo.decoder.method1.config.ServerInitializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;

/**
 * <p> @Title AdhesivePackageServer
 * <p> @Description 通过短连接解决粘包问题的服务端
 *
 * @author ACGkaka
 * @date 2024/3/27 16:51
 */
@Slf4j
public class AdhesivePackageServer {
    public static void main(String[] args) {
        log.info("Start...");
        NioEventLoopGroup group = new NioEventLoopGroup();
        ServerBootstrap server = new ServerBootstrap();

        server.group(group);
        server.channel(NioServerSocketChannel.class);
        server.childHandler(new ServerInitializer());

        server.bind("127.0.0.1", 8888);
        System.out.println("服务端启动成功....");
    }
}
