package com.demo.decoder.method2;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * <p> @Title FixedLengthFrameDecoderDemo
 * <p> @Description 通过 Netty 官方提供的定长帧解码器解决粘包拆包问题。
 *
 * @author ACGkaka
 * @date 2024/3/27 20:11
 */
public class FixedLengthFrameDecoderDemo {

    /**
     * 缓冲区大小
     */
    private static int BYTE_LENGTH = 8;

    public static void main(String[] args) {
        EmbeddedChannel channel = new EmbeddedChannel(
                // 定长帧解码器
                new FixedLengthFrameDecoder(BYTE_LENGTH),
                new LoggingHandler(LogLevel.DEBUG)
        );

        // 调用三次发送到数据的方法
        sendData(channel, "ABC");
        sendData(channel, "123456");
        sendData(channel, "ABC123");
    }

    /**
     * 发送数据
     * @param channel 数据通道
     * @param data 要发送的数据
     */
    private static void sendData(EmbeddedChannel channel, String data) {
        // 获取要发送数据的字节长度
        byte[] bytes = data.getBytes();
        int dataLength = bytes.length;

        // 根据固定长度补齐要发送的数据
        StringBuilder alignString = new StringBuilder();
        if (dataLength < BYTE_LENGTH) {
            int alignLength = BYTE_LENGTH - bytes.length;
            for (int i = 1; i <= alignLength; i++) {
                alignString.append("*");
            }
        }

        // 拼接上对齐数据
        String message = data + alignString;
        byte[] msgBytes = message.getBytes();

        // 构建缓冲区，通过 channel 发送数据
        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer(8);
        buffer.writeBytes(msgBytes);
        channel.writeInbound(buffer);
    }
}
