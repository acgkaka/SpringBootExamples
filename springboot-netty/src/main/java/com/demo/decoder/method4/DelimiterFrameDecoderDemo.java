package com.demo.decoder.method4;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * <p> @Title DelimiterFrameDecoderDemo
 * <p> @Description 通过 Netty 官方提供的分隔符帧解码器解决粘包拆包问题
 *
 * @author ACGkaka
 * @date 2024/3/27 20:32
 */
public class DelimiterFrameDecoderDemo {

    public static void main(String[] args) {
        // 自定义一个分隔符（记得要用 ByteBuf 对象来包装）
        ByteBuf delimiter = ByteBufAllocator.DEFAULT.buffer(1);
        delimiter.writeByte('|');

        // 通过 Netty 提供的测试通道来代替服务端、客户端
        EmbeddedChannel channel = new EmbeddedChannel(
                // 添加一个分隔符帧解码器（传入自定义的分隔符）
                new DelimiterBasedFrameDecoder(1024, delimiter),
                new LoggingHandler(LogLevel.DEBUG)
        );

        sendData(channel, "123");
        sendData(channel, "979799");
        sendData(channel, "123o12p3i12po3iop21i3");
    }

    /**
     * 发送数据
     */
    private static void sendData(EmbeddedChannel channel, String data) {
        // 在要发送的数据结尾，拼接上一个*号（因为前面定义的分隔符为*号）
        String message = data + "|";
        // 获取发送数据的字节长度
        byte[] msgBytes = message.getBytes();

        // 构建缓冲区，通过channel发送数据
        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer(8);
        buffer.writeBytes(msgBytes);
        channel.writeInbound(buffer);
    }
}
