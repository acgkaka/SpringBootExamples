package com.demo.decoder.method3;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * <p> @Title LineFrameDecoderDemo
 * <p> @Description 通过 Netty 官方提供的行帧解码器解决粘包拆包问题。
 *
 * @author ACGkaka
 * @date 2024/3/27 19:54
 */
public class LineFrameDecoderDemo {

    public static void main(String[] args) {
        // 通过 Netty 提供的测试通道来代替服务端、客户端。
        EmbeddedChannel channel = new EmbeddedChannel(
                // 添加一个行帧解码器（在超出1024后还未检测到换行符，就会停止读取）
                new LineBasedFrameDecoder(1024),
                // 加一个字符串处理器，可以处理中文，要不然中文数据显示不出来
                new StringDecoder(),
                new LoggingHandler(LogLevel.DEBUG)
        );

        // 调用三次发送数据的方法
        sendData(channel, "我叫王大锤");
        sendData(channel, "我在总结梳理粘包、拆包问题");
        sendData(channel, "知道的越多，不知道的越多");
    }

    /**
     * 发送数据
     */
    private static void sendData(EmbeddedChannel channel, String data) {
        // 在要发送的数据结尾，拼接上一个\n换行符（\r\n也可以）
        String message = data + "\n";
        // 获取发送数据的字节长度
        byte[] msgBytes = message.getBytes();

        // 构建缓冲区，通过 channel 发送数据
        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer(8);
        buffer.writeBytes(msgBytes);
        channel.writeInbound(buffer);
    }
}
