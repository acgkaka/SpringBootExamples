package com.demo.decoder.method5;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * <p> @Title LTCDecoderDemo
 * <p> @Description LTC解码器使用示例
 *
 * @author ACGkaka
 * @date 2024/3/28 8:43
 */
public class LTCDecoderDemo {

    public static void main(String[] args) {
        // 通过Netty提供的测试通道来代替客户端和服务端。
        EmbeddedChannel channel = new EmbeddedChannel(
                // 添加一个LTC解码器（超出1024后还是会进行分包操作）
                new LengthFieldBasedFrameDecoder(
                        1024, // 单个包最大传1024个字节，超出了分包
                        0, // 长度字段偏移量，表示描述数据长度的信息从第几个字节开始
                        4, // 长度字段占用字节数
                        0, // 长度调整数，表示在长度字段的第N个字节之后才是正文数据的开始
                        4 // 跳过几个字节开始读取数据，可以用来跳过一些头部信息，直接读取正文数据
                ),
                new StringDecoder(),
                new LoggingHandler(LogLevel.DEBUG)
        );

        // 调用发送数据的方法
        sendData(channel, "长度字段偏移量，表示描述数据长度信息从第几个字节开始");
        sendData(channel, "长度字段占用字节数");
        sendData(channel, "长度调整数，表示在长度字段的第N个字节之后才是正文数据的开始");
        sendData(channel, "跳过几个字节开始读取数据，可以用来跳过一些头部信息，直接读取正文数据");
    }

    /**
     * 发送数据
     */
    private static void sendData(EmbeddedChannel channel, String data) {
        // 获取要发送数据的字节以及长度
        byte[] dataBytes = data.getBytes();
        int dataLength = dataBytes.length;

        // 将数据长度写入到缓冲区，再将正文数据写入到缓冲区
        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer();
        // 写入数据正文的长度（int占用4个字节）
        buffer.writeInt(dataLength);
        // 写入数据
        buffer.writeBytes(dataBytes);

        // 发送最终组装好的数据
        channel.writeInbound(buffer);
    }
}
