package com.demo.sourceStudy;

import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> @Title Test
 * <p> @Description TODO
 *
 * @author ACGkaka
 * @date 2024/3/28 9:12
 */
public class FluxDemo {

    public static void main(String[] args) throws InterruptedException {
        List<String> list = new ArrayList<>();
        Flux.just("Tom", "Bob", "zhangsan", "lisi")
                .map(s -> s.concat("@qq.com"))
                .filter(s -> s.length() > 10)
                .subscribe(s -> {
                    System.out.println("result: " + s);
                    try {
                        Thread.sleep(1000L);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    list.add(s);
                });
        System.out.println(">>>>>>>>>> result1: " + list);
        Thread.sleep(10000L);
        System.out.println(">>>>>>>>>> result2: " + list);
    }
}
