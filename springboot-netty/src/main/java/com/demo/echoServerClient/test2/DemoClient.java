package com.demo.echoServerClient.test2;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class DemoClient {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("192.168.2.126", 6667);

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))) {
            String message1 = "Message 1";
            String message2 = "Message 2";
            // 不做特殊处理，直接写出两个消息，没有明显的消息边界
            writer.write(message1);
            writer.write(message2);
            writer.flush();

            socket.close();
            System.out.println("Message sent.");
        }
    }
}
