package com.demo.echoServerClient.test3;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

/**
 * <p> @Title NIOClient
 * <p> @Description TODO
 *
 * @author ACGkaka
 * @date 2024/3/27 15:13
 */
public class NIOClient {

    @SneakyThrows
    public static void main(String[] args) {
        SocketChannel socketChannel = SocketChannel.open();
        try {
            socketChannel.connect(new InetSocketAddress("127.0.0.1", 8080));
            ByteBuffer byteBuffer1, byteBuffer2;
            socketChannel.write(ByteBuffer.wrap("你".getBytes(StandardCharsets.UTF_8)));
            socketChannel.write(ByteBuffer.wrap("好".getBytes(StandardCharsets.UTF_8)));
            System.out.println("client send finished");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            socketChannel.close();
        }
    }

}

