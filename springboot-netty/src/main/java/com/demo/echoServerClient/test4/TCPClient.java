package com.demo.echoServerClient.test4;

import java.io.*;
import java.net.Socket;

public class TCPClient {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 8080);
        System.out.println("Connected to server.");

        String largeMessage = "This is a very long message that will be split into multiple packets by TCP..."; // 大于服务端缓冲区大小的消息
        byte[] messageBytes = largeMessage.getBytes();

        OutputStream output = socket.getOutputStream();
        output.write(messageBytes);
        output.flush();

        socket.close();
        System.out.println("Sent the large message.");
    }
}
