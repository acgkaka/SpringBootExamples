package com.demo.echoServerClient.test4;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPFragmentationServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Server is listening on port 8080...");

        Socket socket = serverSocket.accept();
        System.out.println("Accepted a client connection.");

        InputStream input = socket.getInputStream();
        byte[] buffer = new byte[10]; // 创建一个小于预期消息大小的缓冲区

        // 模拟每次只读取少量数据，导致拆包
        while (true) {
            int bytesRead = input.read(buffer);
            if (bytesRead == -1) break; // 没有更多数据可读
            System.out.println("Received bytes: " + new String(buffer, 0, bytesRead));
        }

        socket.close();
        serverSocket.close();
    }
}
