package com.demo.echoServerClient.test;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * <p> @Title TestByteBuffer
 * <p> @Description 测试ByteBuffer
 *
 * @author ACGkaka
 * @date 2024/3/24 20:52
 */
@Slf4j
public class TestByteBuffer {

    public static void main(String[] args) {
        // 获取 FileChannel
        // 方式1：输入流/输出流；方式2：RandomAccessFile
        try (FileChannel channel = new FileInputStream("data.txt").getChannel()) {
            // 准备缓冲区（划分一块内存方式获得，单位：字节）
            ByteBuffer buffer = ByteBuffer.allocate(10);
            while (true) {
                // 从 Channel 读取数据，向 buffer 写入
                int len = channel.read(buffer);
                log.debug("读取到的字节数 {}", len);
                if (len == -1) {
                    break;
                }
                // 打印 buffer 的内容
                buffer.flip(); // 切换置读模式
                while (buffer.hasRemaining()) {
                    byte b = buffer.get();// 不指定的情况下，默认一次读取一个字节
                    log.debug("实际字节 {}", (char) b);
                }
                buffer.clear(); // 切换为写模式
            }
        } catch (IOException e) {
        }
    }
}
