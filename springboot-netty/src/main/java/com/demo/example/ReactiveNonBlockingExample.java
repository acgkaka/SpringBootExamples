package com.demo.example;

import reactor.netty.http.client.HttpClient;

/**
 * <p> @Title ReactiveNonBlockingExample
 * <p> @Description Reactive非阻塞示例
 *
 * @author ACGkaka
 * @date 2024/3/30 11:13
 */
public class ReactiveNonBlockingExample {


    public static void main(String[] args) throws InterruptedException {
        // 创建一个非阻塞的HTTP客户端
        HttpClient httpClient = HttpClient.create();

        // 创建一个异步请求流
        StringBuilder builder = new StringBuilder();
        httpClient.get()
                .uri("http://localhost:8080/demo/test1")
                .responseContent()
                .aggregate()
                .asString()
                .subscribe(s -> {
                    builder.append(s);
                    System.out.println("response: " + s);
                });

        // 由于 Reactor 是异步非阻塞地，主线程在此不会被阻塞
        System.out.println("Main thread continues executing while requests are being processed in the background.");
        System.out.println(">>>>>>>>>> result1: " + builder);

        // 假设我们希望程序运行一段时间后退出
        Thread.sleep(5000L); // 模拟等待5秒后退出程序
        System.out.println(">>>>>>>>>> result2: " + builder);
    }
}
