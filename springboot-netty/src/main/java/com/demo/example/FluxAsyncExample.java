package com.demo.example;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

/**
 * <p> @Title FluxAsyncExample
 * <p> @Description Flux异步示例
 *
 * @author zhj
 * @date 2024/3/30 14:29
 */
@Slf4j
public class FluxAsyncExample {
    public static void main(String[] args) throws InterruptedException {
        log.info("Starting main method at " + System.currentTimeMillis());

        // 创建一个模拟异步操作的 Flux
        Flux<String> asyncFlux = Flux.defer(() -> {
            log.info("Async operation started at " + System.currentTimeMillis());
            return Flux.just("Result 1", "Result 2", "Result 3")
                    .delayElements(Duration.ofSeconds(1)); // 模拟每个结果之间有1秒延迟
        });

        // 订阅 Flux，并指定在另一个 Scheduler 上执行
        asyncFlux.subscribeOn(Schedulers.elastic()) // 使用弹性调度器模拟异步环境
                .subscribe(
                        result -> log.info("Received result: " + result + " at " + System.currentTimeMillis()),
                        error -> log.error("Error occurred: " + error),
                        () -> log.info("Async operation completed at " + System.currentTimeMillis())
                );

        log.info("Main method continues immediately at " + System.currentTimeMillis());

        Thread.sleep(5000L);
    }
}
