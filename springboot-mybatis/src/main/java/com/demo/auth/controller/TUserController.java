package com.demo.auth.controller;

import com.demo.auth.entity.TUser;
import com.demo.auth.service.TUserService;
import com.demo.common.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户表(TUser)表控制层
 *
 * @author ACGkaka
 * @since 2021-06-18 16:49:55
 */
@RestController
@RequestMapping("/user")
public class TUserController {
    /**
     * 服务对象
     */
    @Resource
    private TUserService tUserService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne")
    public TUser selectOne(Long id) {
        return this.tUserService.queryById(id);
    }

    /**
     * 查询全部数据
     *
     * @return 全部数据
     */
    @GetMapping("/findAll")
    public List<TUser> findAll() {
        return this.tUserService.queryAllByLimit(1, -1);
    }

    /**
     * 查询全部数据
     *
     * @return 全部数据
     */
    @GetMapping("/update")
    public Result<Object> update() {
        List<TUser> list = tUserService.queryAllByLimit(1, -1);
        if (list.size() > 0) {
            TUser user = list.get(0);
            user.setUsername("ACGkaka");
            this.tUserService.update(user);
            return Result.succeed();
        }
        return Result.failed("没有数据");
    }

}