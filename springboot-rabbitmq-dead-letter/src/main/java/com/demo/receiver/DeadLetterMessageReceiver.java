package com.demo.receiver;

import com.demo.config.RabbitMQConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * <p> @Title DeadLetterMessageReceiver
 * <p> @Description RabbitMQ死信队列消费端
 *
 * @author ACGkaka
 * @date 2024/1/7 18:14
 */
@Slf4j
@Component
public class DeadLetterMessageReceiver {

    @RabbitListener(queues = RabbitMQConfig.DEAD_LETTER_QUEUEA_NAME)
    public void receiveA(String body, Message message, Channel channel) throws IOException {
        log.info("死信队列A收到消息: {}，消息properties: {}", body, message.getMessageProperties());
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }

    @RabbitListener(queues = RabbitMQConfig.DEAD_LETTER_QUEUEB_NAME)
    public void receiveB(String body, Message message, Channel channel) throws IOException {
        log.info("死信队列B收到消息: {}", body);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
