package com.demo.receiver;

import com.demo.config.RabbitMQConfig;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * <p> @Title BusinessMessageReceiver
 * <p> @Description RabbitMQ业务队列消费端
 *
 * @author ACGkaka
 * @date 2024/1/7 17:43
 */
@Slf4j
@Component
public class BusinessMessageReceiver {

    @RabbitListener(queues = RabbitMQConfig.BUSINESS_QUEUEA_NAME)
    public void receiveA(String body, Message message, Channel channel) throws IOException {
        log.info("业务队列A收到消息: {}", body);
        try {
            if (body.contains("deadletter")) {
                throw new RuntimeException("dead letter exception");
            }
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            log.error("业务队列A消息消费发生异常，error msg: {}", e.getMessage(), e);
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), false);
        }
    }

    @RabbitListener(queues = RabbitMQConfig.BUSINESS_QUEUEB_NAME)
    public void receiveB(String body, Message message, Channel channel) throws IOException {
        log.info("业务队列B收到消息: {}", body);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
