package com.demo.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * <p> @Title RabbitMQOrderConfig
 * <p> @Description RabbitMQ配置
 *
 * @author ACGkaka
 * @date 2023/12/22 14:05
 */
@Configuration
public class RabbitMQConfig {

    /** 业务队列 */
    public static final String BUSINESS_EXCHANGE_NAME = "demo.simple.business.exchange";
    public static final String BUSINESS_QUEUEA_NAME = "demo.simple.business.queuea";
    public static final String BUSINESS_QUEUEB_NAME = "demo.simple.business.queueb";
    /** 死信队列 */
    public static final String DEAD_LETTER_EXCHANGE = "demo.simple.deadletter.exchange";
    public static final String DEAD_LETTER_QUEUEA_ROUTING_KEY = "demo.simple.deadletter.queuea.routingkey";
    public static final String DEAD_LETTER_QUEUEB_ROUTING_KEY = "demo.simple.deadletter.queueb.routingkey";
    public static final String DEAD_LETTER_QUEUEA_NAME = "demo.simple.deadletter.queuea";
    public static final String DEAD_LETTER_QUEUEB_NAME = "demo.simple.deadletter.queueb";

    // 声明业务交换机（广播）
    @Bean
    public FanoutExchange businessExchange() {
        return new FanoutExchange(BUSINESS_EXCHANGE_NAME);
    }

    // 声明死信交换机（直连）
    @Bean
    public DirectExchange deadLetterExchange() {
        return new DirectExchange(DEAD_LETTER_EXCHANGE);
    }

    // 声明业务队列A
    @Bean
    public Queue businessQueueA() {
        Map<String, Object> args = new HashMap<>(2);
        // 声明当前队列绑定的死信交换机
        args.put("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE);
        // 声明当前队列绑定的死信路由键
        args.put("x-dead-letter-routing-key", DEAD_LETTER_QUEUEA_ROUTING_KEY);
        return QueueBuilder.durable(BUSINESS_QUEUEA_NAME).withArguments(args).build();
    }

    // 声明业务队列B
    @Bean
    public Queue businessQueueB() {
        Map<String, Object> args = new HashMap<>(2);
        // 声明当前队列绑定的死信交换机
        args.put("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE);
        // 声明当前队列绑定的死信路由键
        args.put("x-dead-letter-routing-key", DEAD_LETTER_QUEUEB_ROUTING_KEY);
        return QueueBuilder.durable(BUSINESS_QUEUEB_NAME).withArguments(args).build();
    }

    // 声明死信队列A
    @Bean
    public Queue deadLetterQueueA() {
        return QueueBuilder.durable(DEAD_LETTER_QUEUEA_NAME).build();
    }

    // 声明死信队列B
    @Bean
    public Queue deadLetterQueueB() {
        return QueueBuilder.durable(DEAD_LETTER_QUEUEA_NAME).build();
    }

    // 声明业务队列A绑定关系
    @Bean
    public Binding businessBindingA(Queue businessQueueA, FanoutExchange businessExchange) {
        return BindingBuilder.bind(businessQueueA).to(businessExchange);
    }

    // 声明业务队列B绑定关系
    @Bean
    public Binding businessBindingB(Queue businessQueueB, FanoutExchange businessExchange) {
        return BindingBuilder.bind(businessQueueB).to(businessExchange);
    }

    // 声明死信队列A绑定关系
    @Bean
    public Binding deadLetterBindingA(Queue deadLetterQueueA, DirectExchange deadLetterExchange) {
        return BindingBuilder.bind(deadLetterQueueA).to(deadLetterExchange).with(DEAD_LETTER_QUEUEA_ROUTING_KEY);
    }

    // 声明死信队列B绑定关系
    @Bean
    public Binding deadLetterBindingB(Queue deadLetterQueueB, DirectExchange deadLetterExchange) {
        return BindingBuilder.bind(deadLetterQueueB).to(deadLetterExchange).with(DEAD_LETTER_QUEUEB_ROUTING_KEY);
    }
}
