package com.demo.controller;

import cn.hutool.core.util.IdUtil;
import com.demo.Demo;
import com.demo.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p> @Title DemoController
 * <p> @Description 测试Controller
 *
 * @author ACGkaka
 * @date 2023/4/24 18:02
 */
@Slf4j
@Controller
@RequestMapping("/demo")
public class DemoController {

    @GetMapping("/snowflakePage")
    public String snowflakePage() {
        return "snowflakePage";
    }

    @GetMapping("/snowflakeId")
    @ResponseBody
    public Result<Object> snowflakeId() {
        Demo demo = new Demo(IdUtil.getSnowflakeNextId());
        return Result.succeed().setData(demo);
    }
}
