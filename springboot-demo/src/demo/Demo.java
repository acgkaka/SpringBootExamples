package com.demo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * <p> @Title Demo
 * <p> @Description 测试雪花算法ID
 *
 * @author zhj
 * @date 2024/4/12 15:07
 */
@Data
@AllArgsConstructor
public class Demo {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
}
