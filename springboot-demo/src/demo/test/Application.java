package com.demo.test;

/**
 * <p> @Title Application
 * <p> @Description TODO
 *
 * @author zhj
 * @date 2024/4/10 10:47
 */
public class Application {

    public static void main(String[] args) {
        // 1.首次访问这个类的静态变量或静态方法时
//        System.out.println(Animal.num);
        // 2.子类初始化，如果父类还没初始化，会引发父类先初始化
//        System.out.println(Cat.sex);
        // 3.子类访问父类静态变量，只触发父类初始化
        System.out.println(Cat.num);
    }
}

class Animal {
    static int num = 55;
    static {
        System.out.println("Animal 静态代码块...");
    }
}

class Cat extends Animal {
    static boolean sex = false;
    static {
        System.out.printf("Cat 静态代码块...1");
    }

    static {
        System.out.printf("Cat 静态代码块...2");
    }
}
