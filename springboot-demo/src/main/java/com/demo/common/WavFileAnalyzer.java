package com.demo.common;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ws.schild.jave.MultimediaObject;
import ws.schild.jave.encode.AudioAttributes;

import javax.sound.sampled.*;
import java.beans.Encoder;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * <p> @Title WavFileAnalyzer
 * <p> @Description TODO
 *
 * @author zhj
 * @date 2024/5/20 19:47
 */
@RestController
@RequestMapping("/test")
public class WavFileAnalyzer {
    public static void main(String[] args) {
        String path = "D:\\java\\ffmpeg-7.0-full_build(1)\\h5.wav";
        File wavFile = new File(path); // 替换为你的WAV文件路径

        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(wavFile);
            AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(wavFile);
            AudioFormat format = audioInputStream.getFormat();

            boolean is16kHz = format.getSampleRate() == 16000;
            boolean is16Bit = format.getSampleSizeInBits() == 16;
            boolean isMono = format.getChannels() == 1;

            System.out.println("文件地址：" + path);
            System.out.println("采样率：" + format.getSampleRate() / 1000 + "。");
            System.out.println("位深度：" + format.getSampleSizeInBits() + "。");
            System.out.println("通道：" + format.getChannels() + "。");

            System.out.println("==========>");
            if (is16kHz && is16Bit && isMono) {
                System.out.println("WAV文件满足16kHz、16位、单通道的要求。");
            } else {
                System.out.println("WAV文件不完全满足16kHz、16位、单通道的要求。");
                if (!is16kHz) System.out.println("采样率不是16kHz，采样率：" + format.getSampleRate() / 1000 + "。");
                if (!is16Bit) System.out.println("位深度不是16位，位深度：" + format.getSampleSizeInBits() + "。");
                if (!isMono) System.out.println("不是单通道，通道：" + format.getChannels() + "。");
            }

            audioInputStream.close();
        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/transferWAV")
    public Result<Object> transferWAV(String aacFilePath, String wavFilePath) {
        boolean result = FfmpegUtil.formatAudio(aacFilePath, wavFilePath);
        if (result) {
            System.out.println("转换成功");
            return Result.succeed();
        } else {
            System.err.println("转换失败");
            return Result.failed();
        }
    }

    public static void main2(String[] args) {
        String aacFilePath = "D:\\java\\ffmpeg-7.0-full_build(1)\\xcx(1).wav";
        String wavFilePath = "D:\\java\\ffmpeg-7.0-full_build(1)\\xcx(2).wav";
        boolean result = FfmpegUtil.formatAudio(aacFilePath, wavFilePath);
        System.out.println(">>>>>>>>>>【INFO】转换结果：" + result);
    }

    private static final int SAMPLING_RATE = 16000;
    private static final int SINGLE_CHANNEL = 1;

}
