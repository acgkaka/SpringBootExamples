PS D:\IdeaProjects\SpringBootExamples\springboot-demo> jmap -heap 25848
Attaching to process ID 25848, please wait...
Debugger attached successfully.
Server compiler detected.
Heap Configuration:
   MinHeapFreeRatio         = 0
   MaxHeapFreeRatio         = 100
   MaxHeapSize              = 4125097984 (3934.0MB)
   NewSize                  = 85983232 (82.0MB)
   MaxNewSize               = 1374683136 (1311.0MB)
   OldSize                  = 171966464 (164.0MB)
   NewRatio                 = 2
   SurvivorRatio            = 8
   MetaspaceSize            = 21807104 (20.796875MB)
   CompressedClassSpaceSize = 1073741824 (1024.0MB)
   MaxMetaspaceSize         = 17592186044415 MB
   G1HeapRegionSize         = 0 (0.0MB)

Heap Usage:
PS Young Generation
Eden Space:
   capacity = 65011712 (62.0MB)
   used     = 15607624 (14.884590148925781MB)
   free     = 49404088 (47.11540985107422MB)
   24.007403466009325% used
From Space:
   capacity = 10485760 (10.0MB)
   used     = 0 (0.0MB)
   free     = 10485760 (10.0MB)
   0.0% used
To Space:
   capacity = 10485760 (10.0MB)
   used     = 0 (0.0MB)
   free     = 10485760 (10.0MB)
   0.0% used
PS Old Generation
   capacity = 171966464 (164.0MB)
   used     = 0 (0.0MB)
   free     = 171966464 (164.0MB)
   0.0% used

3512 interned Strings occupying 286240 bytes.