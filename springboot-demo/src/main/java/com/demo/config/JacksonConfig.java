//package com.demo.config;
//
//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.module.SimpleModule;
//import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
//import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
//import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
//
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
//@Configuration
//public class JacksonConfig {
//    @Bean
//    @Primary
//    @ConditionalOnMissingBean(ObjectMapper.class)
//    public ObjectMapper jacksonObjectMapper(Jackson2ObjectMapperBuilder builder)
//    {
//        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
//        // 全局配置序修改列化返回 Json 处理方案
//        objectMapper.registerModule(new SimpleModule()
//                // Json Long --> String，防止精度丢失
//                .addSerializer(Long.class, ToStringSerializer.instance)
//                // 自定义序列化时 LocalDateTime 时间日期格式
//                .addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
//                // 自定义反序列化时 LocalDateTime 时间日期格式
//                .addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
//
//        // 在序列化时，忽略值为 null 的属性
//        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
//        // 在序列化是，忽略值为默认值的属性
//        objectMapper.setDefaultPropertyInclusion(JsonInclude.Include.NON_DEFAULT);
//        // 在反序列化时，忽略在 json 中存在但 Java 对象不存在的属性。
//        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        return objectMapper;
//    }
//}
