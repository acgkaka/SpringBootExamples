import pandas as pd

# 读取两个Excel文件中的相关Sheet
cosmic_data = pd.read_excel('最新cosmic模版v1.6(2).xlsx', sheet_name='Sheet3')
e_sign_data = pd.read_excel('随e签功能清单_zhj.xlsx', sheet_name='Sheet2')

# 假设两份数据中分别有名为'功能过程'的列，用于匹配
cosmic_process_col = '功能过程'
e_sign_process_col = '功能过程'

# 对于《最新cosmic模板v1.6》中的每一行（即每一个功能过程）
for cosmic_row in cosmic_data.itertuples(index=False):
    cosmic_process = getattr(cosmic_row, cosmic_process_col)

    # 在《随e签功能清单_zhj》中找到匹配的功能过程
    matching_rows = e_sign_data[e_sign_data[e_sign_process_col] == cosmic_process]

    # 如果找到了匹配项，则将《最新cosmic模板v1.6》中该功能过程对应的FGHI列数据插入到《随e签功能清单_zhj》相应位置
    if not matching_rows.empty:
        for cosmic_fghi_col in ['F', 'G', 'H', 'I']:
            e_sign_data.insert(len(e_sign_data.columns), cosmic_fghi_col, None)  # 在末尾添加新列
            e_sign_data.loc[matching_rows.index, cosmic_fghi_col] = getattr(cosmic_row, cosmic_fghi_col)  # 插入对应数据

        # 若功能过程存在一对多关系，需根据实际情况在《随e签功能清单_zhj》中插入多行
        # 根据需求分析，此处假设仅需复制原有行即可（如果有更复杂的插入逻辑，请在此处实现）
        for _ in range(matching_rows.shape[0] - 1):
            e_sign_data = e_sign_data.append(matching_rows.iloc[0], ignore_index=True)

# 保存更新后的《随e签功能清单_zhj》至新的Excel文件或覆盖原文件
e_sign_data.to_excel('随e签功能清单_zhj_updated.xlsx', index=False)