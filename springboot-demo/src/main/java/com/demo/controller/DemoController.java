package com.demo.controller;

import com.demo.common.Result;
import com.demo.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * <p> @Title DemoController
 * <p> @Description 测试Controller
 *
 * @author ACGkaka
 * @date 2023/4/24 18:02
 */
@Slf4j
@RestController
@RequestMapping("/demo")
public class DemoController {

    @GetMapping("/test1")
    public Result<Object> test1() {
        testHeap();
        return Result.succeed();
    }

    @GetMapping("/test2")
    public Result<Object> test2() {
        testStack();
        return Result.succeed();
    }

    @GetMapping("/test3")
    public Result<Object> test3() {
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            byte[] buffer = new byte[1024 * 1024]; // 每次循环都创建一个新数组
        }
        return Result.succeed();
    }

    public void testHeap(){
        ArrayList<Object> list = new ArrayList<>(2000);
        for(;;){  //死循环一直创建对象，堆溢出
            list.add(new DemoController());
        }
    }
    int num=1;
    public void testStack(){  //无出口的递归调用，栈溢出
        num++;
        this.testStack();
    }

    public static void main1(String[] args){
        DemoController t = new DemoController();
        t.testHeap();
//        t.testStack();
    }


}
