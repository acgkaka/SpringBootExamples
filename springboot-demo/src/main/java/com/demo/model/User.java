package com.demo.model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 一个简单的 Java Bean 类
 */
@Data
@AllArgsConstructor
public class User {
    private int id;
    @SerializedName("name")
    private String name;
    private LocalDateTime birthday;
    private Address address;

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Data
    @AllArgsConstructor
    public static class Address {
        private String street;
        private String city;
    }
}
