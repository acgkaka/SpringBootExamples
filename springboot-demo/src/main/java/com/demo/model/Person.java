package com.demo.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Person {
    // 字符串测试
    private String name;
    // 空对象测试
    @SerializedName("age1")
    private Integer age;
    // 默认值测试
    private int height;
    // 日期测试
    private LocalDateTime createTime;
}
