package com.demo.test;

import com.demo.model.User;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

/**
 * <p> @Title JacksonTest
 * <p> @Description Jackson库测试
 *
 * @author ACGkaka
 * @date 2024/4/12 19:04
 */
public class JacksonTest {

    public static void main(String[] args) {
        // 序列化单个对象
        User user = new User(1, "路人甲", LocalDateTime.now(), new User.Address("Street1", "City1"));
        JsonSerializer<LocalDateTime> localDateTimeSerializer = (localDateTime, type, jsonSerializationContext) -> new JsonPrimitive(localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        JsonDeserializer<LocalDateTime> localDateTimeDeserializer = (json, typeOfT, context) -> LocalDateTime.parse(json.getAsJsonPrimitive().getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Gson gson = new GsonBuilder()
                // 设置 LocalDateTime 序列化格式
                .registerTypeAdapter(LocalDateTime.class, localDateTimeSerializer)
                // 设置 LocalDateTime 反序列化格式
                .registerTypeAdapter(LocalDateTime.class, localDateTimeDeserializer)
                .serializeNulls()
                .create();
        String jsonString = gson.toJson(user);
        System.out.println("Serialized JSON: " + jsonString);

        // 反序列化单个对象
        User deserializedUser = gson.fromJson(jsonString, User.class);
        System.out.println("Deserialized User: " + deserializedUser);

        // 序列化单个对象，保留null值和去掉空格（GSON默认保留null值，格式化可使用GsonBuilder）
        Gson prettyGson = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, localDateTimeSerializer)
                .registerTypeAdapter(LocalDateTime.class, localDateTimeDeserializer)
                .setPrettyPrinting().create();
        String formattedJson = prettyGson.toJson(user);
        System.out.println("Formatted JSON with null values preserved:\n" + formattedJson);

        // 序列化多个对象
        List<User> userList = Arrays.asList(new User(2, "路人乙"), new User(3, "路人丙"));
        String usersJson = gson.toJson(userList);
        System.out.println("Users as JSON: " + usersJson);

        // 反序列化多个对象
        Type userListType = new TypeToken<List<User>>() {}.getType();
        List<User> users = gson.fromJson(usersJson, userListType);
        System.out.println("Parsed Users from JSON: " + users);
    }

}
