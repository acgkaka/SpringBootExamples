package com.demo.websocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketMessage;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Describe 开启uk websocket
 * @Author lym
 * @Date 2024/08/15
 **/
@ServerEndpoint("/uk/websocket/wss/{code}/{timeStamp}")
@Component
@Slf4j
//public class UkWebSocket extends AbstractWebSocketHandler {
public class UkWebSocket {
    /**
     * 静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
     */
    private static AtomicInteger onlineCount = new AtomicInteger(0);
    private final int maxQueSize = 10000;
    /**
     * concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
     */
    private static ConcurrentHashMap<String, String> onlineMap = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, String> webSocketMap = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Session> sessionMap = new ConcurrentHashMap<>();

    /**
     * 建立链接
     *
     * @param session
     * @param
     * @param timeStamp
     */
    @OnOpen
//    public void onOpen(Session session, @PathParam("system") String system, @PathParam("code") String code, @PathParam("timeStamp") String timeStamp) {
    public void onOpen(Session session,@PathParam("code") String code,@PathParam("timeStamp") String timeStamp) {
        String key = code;
        log.info("===============key={}",key);
        if (onlineMap.containsKey(key)) {
            String sessionId = onlineMap.get(key);
            Session nowSession = sessionMap.get(sessionId);
            if (nowSession != null && nowSession.isOpen()) {
                try {
                    session.close();
                    log.warn("连接冲突:" + key);
                    return;
                } catch (Exception e) {
                    log.error("连接冲突关闭异常:" + key, e);
                }
            }
        }
        //加入set中
        String sessionId = session.getId();
        log.info("正在连接...sessionId={}", sessionId);
        onlineMap.put(key, session.getId());//code -> session-id
        sessionMap.put(session.getId(), session);//session-id ,wsSession
        webSocketMap.put(session.getId(), key);//session-id ,code
        //在线数加1
        addOnlineCount();
        log.info("连接成功:key=" + key + ",当前在线为:" + getOnlineCount());
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        if (sessionMap.containsKey(session.getId())) {
        } else {
            log.info("连接直接退出:" + session.getId() + ",当前在线人数为:" + getOnlineCount());
        }
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        log.info("start do on Message...");
    }

    /**
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        if (sessionMap.containsKey(session.getId())) {
            String key = webSocketMap.get(session.getId());
        } else {
            log.info("连接异常:" + session.getId() + ",当前在线人数为:" + getOnlineCount());
        }
    }

    /**
     * 实现服务器主动推送
     */
    public void sendMessage(Session session, String message) throws IOException {
        session.getBasicRemote().sendText(message);
    }

    public static int getOnlineCount() {
        return onlineCount.intValue();
    }

    public static void addOnlineCount() {
        onlineCount.incrementAndGet();
    }

    public static void subOnlineCount() {
        onlineCount.decrementAndGet();
    }


    public static Session getSessionMap(String key) {
        return sessionMap.get(key);
    }

    public static void sendMessage(String key, String message) throws IOException {
        if (onlineMap.containsKey(key)) {
            String sessionId = onlineMap.get(key);////code -> session-id
            Session nowSession = sessionMap.get(sessionId);//session-id ,wsSession
            boolean open = nowSession.isOpen();
            log.info("客户端key={},状态={}", key, open);
            if (open) {//如果session是open状态，则给它发送消息
                nowSession.getBasicRemote().sendText(message);
                log.info("发送完成，发送内容={}", message);
            } else {
                log.info("连接关闭，不发送");
            }
        } else {
            log.info("连接为空，不发送");
        }
    }
}
