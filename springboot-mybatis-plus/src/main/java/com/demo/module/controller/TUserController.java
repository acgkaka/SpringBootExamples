package com.demo.module.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.demo.module.common.Result;
import com.demo.module.entity.TUser;
import com.demo.module.service.TUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author ACGkaka
 * @since 2021-04-25
 */
@RestController
@RequestMapping("/user")
public class TUserController {


    @Resource
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/test")
    public Result<Object> test() {
        jdbcTemplate.execute("update t_user set id = 1;");

        String sql = "SELECT COUNT(*) FROM t_user";
        jdbcTemplate.execute(new ConnectionCallback<Integer>() {
            @Override
            public Integer doInConnection(Connection con) throws SQLException {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                if (rs.next()) {
                    return rs.getInt(1); // 返回查询结果的第一列值
                }
                return 0; // 如果没有结果，返回0
            }
        });
        return Result.succeed();
    }

    @Autowired
    private TUserService tUserService;

    @GetMapping("/list")
    public List<TUser> list() {
        return tUserService.list();
    }

    @GetMapping("/update")
    public Result update(String username) {
        tUserService.updateUser(username);
        return Result.succeed();
    }

    @GetMapping("/update1")
    public Result update1() {
        return Result.succeed();
    }

    @GetMapping("/create")
    public Result create() {
        List<TUser> list = new ArrayList<>(1000);
        LocalDateTime now = LocalDateTime.now();
        byte[] bytes = new byte[1024 * 1024];
        for (int i = 1; i <= 10000; i++) {
            list.add(new TUser(UUID.randomUUID().toString().replaceAll("-", ""), "用户_" + i, bytes, now, now));
            if (i % 1000 == 0) {
                long start = System.currentTimeMillis();
                tUserService.saveBatch(list);
                long end = System.currentTimeMillis();
                System.out.println("耗时：" + (end - start) / 1000 + " s");
                list.clear();
            }
        }
        return Result.succeed();
    }

    @GetMapping("/findAll")
    public Result findAll(String size) {
        Result<Object> result = Result.succeed();
        LambdaQueryWrapper<TUser> idQueryWrapper = new LambdaQueryWrapper<>();
        idQueryWrapper.select(TUser::getId);
        idQueryWrapper.last("limit " + size);
        Set<String> ids = tUserService.list(idQueryWrapper).stream().map(TUser::getId).collect(Collectors.toSet());
        LambdaQueryWrapper<TUser> userQueryWrapper = new LambdaQueryWrapper<>();
        userQueryWrapper.in(TUser::getId, ids);
        List<TUser> users = tUserService.list(userQueryWrapper);
        return result.setData(users);
    }
}
