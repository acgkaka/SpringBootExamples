package com.demo.service.impl;

import com.demo.service.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p> @Title DemoServiceImpl
 * <p> @Description 测试ServiceImpl
 *
 * @author ACGkaka
 * @date 2023/4/24 18:14
 */
@Slf4j
@Service
public class DemoServiceImpl implements DemoService {

    @Resource
    private ThreadPoolTaskExecutor sendSMSThreadPool;

    @Override
    public void sendSMS() {
        // 线程池-发送短信
        sendSMSThreadPool.execute(() -> {
            log.info("Thread: {}, SMS sending...", Thread.currentThread().getName());
        });
    }
}
