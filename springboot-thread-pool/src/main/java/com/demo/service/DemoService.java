package com.demo.service;

/**
 * <p> @Title DemoService
 * <p> @Description 测试Service
 *
 * @author ACGkaka
 * @date 2023/4/24 18:13
 */
public interface DemoService {

    /**
     * 线程池-发送短信
     */
    void sendSMS();
}
