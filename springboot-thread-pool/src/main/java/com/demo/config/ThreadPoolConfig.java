package com.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * <p> @Title ThreadPoolConfig
 * <p> @Description 线程池配置
 *
 * @author ACGkaka
 * @date 2024/01/18 21:08
 */
@Configuration
public class ThreadPoolConfig {

    /** 最佳线程数：操作系统线程数+2 */
    private static final int CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors() + 2;
    /** 最大线程数 */
    private static final int MAX_POOL_SIZE = CORE_POOL_SIZE * 2;
    /** 队列长度 */
    private static final int QUEUE_CAPACITY = 10000;

    /**
     * 发送短信线程池
     */
    @Bean
    public ThreadPoolTaskExecutor sendSMSThreadPool() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(CORE_POOL_SIZE);
        executor.setMaxPoolSize(MAX_POOL_SIZE);
        executor.setQueueCapacity(QUEUE_CAPACITY);
        executor.setThreadNamePrefix("SendSMS-");
        // 等待任务执行完毕后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.initialize();
        return executor;
    }
}
