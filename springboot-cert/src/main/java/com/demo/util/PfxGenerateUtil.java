package com.demo.util;


import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Random;

/**
 * .pfx/.p12 证书文件生成工具类
 */
@Component
public class PfxGenerateUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(PfxGenerateUtil.class);

    private static final String KEY_STORE_TYPE = "PKCS12";
    private static final String ALGORITHM_TYPE = "RSA";
    private static final int KEY_SIZE = 2048;
    private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";

    static {
        // 系统添加BC加密算法
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * 创建个人证书
     */
    public static X509Certificate createPersonPfx(String certPath, String password, String account) throws Exception {
        Date notBefore = new Date();
        Date notAfter = DateUtils.addYears(notBefore, 1);
        return createPfx(certPath, password, notBefore, notAfter, account, null);
    }

    /**
     * 创建企业证书
     */
    public static X509Certificate createOrgPfx(String certPath, String password, String account, String orgCode) throws Exception {
        Date notBefore = new Date();
        Date notAfter = DateUtils.addYears(notBefore, 1);
        return createPfx(certPath, password, notBefore, notAfter, account, orgCode);
    }

    /**
     * 创建证书
     */
    private static X509Certificate createPfx(String certPath, String password, Date notBefore, Date notAfter, String account, String orgCode) throws Exception {
        LOGGER.debug("The cert path is: " + certPath);
        //如果目录不存在创建目录
        File certFilePath = FileUtils.getFile(certPath);
        FileUtils.forceMkdir(certFilePath.getParentFile());
        // 设置颁发者和主题
        // CN (Common Name名字与姓氏)
        // OU (Organization Unit组织单位名称)
        // O(Organization组织名称)
        // L(Locality城市或区域名称)
        // ST(State州或省份名称)
        // C(Country国家名称）
        String issuerString;
        String subjectString;
        if(orgCode == null || "".equals(orgCode.trim())) {
            subjectString = "C=CN,CN=" + account;
            issuerString = "C=CN,CN=www.bo.cn CA Individual";
        } else {
            subjectString = "C=C,CN=" + orgCode;
            issuerString = "C=C,CN=www.bo.cn CA Enterprise";
        }

        // 证书序列号
        BigInteger serail = BigInteger.probablePrime(32, new Random());

        /* RSA算法产生公钥和私钥 */
        KeyPair keyPair = null;
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(ALGORITHM_TYPE);
        kpg.initialize(KEY_SIZE);
        keyPair = kpg.generateKeyPair();

        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();

        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
        // 设置序列号
        certGen.setSerialNumber(serail);
        // 设置颁发者
        certGen.setIssuerDN(new X509Principal(issuerString));
        // 设置有效期
        certGen.setNotBefore(notBefore);
        certGen.setNotAfter(notAfter);
        // 设置使用者
        certGen.setSubjectDN(new X500Principal(subjectString));
        // 设置公钥
        certGen.setPublicKey(publicKey);
        // 签名算法
        certGen.setSignatureAlgorithm(SIGNATURE_ALGORITHM);
        X509Certificate certificate = certGen.generateX509Certificate(privateKey, "BC");

        // 创建KeyStore
        KeyStore store = KeyStore.getInstance(KEY_STORE_TYPE);
        store.load(null);
        store.setKeyEntry(account, keyPair.getPrivate(), password.toCharArray(), new Certificate[]{certificate});

        try (FileOutputStream fout = new FileOutputStream(certPath);) {
            store.store(fout, password.toCharArray());
        }

        return certificate;
    }
}