package com.demo.util;

import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.signers.SM2Signer;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.*;

/**
 * 国密SM2算法工具类
 **/
@Slf4j
public class SM2Utils {
    private static final Charset CHARSETS = StandardCharsets.UTF_8;

    private static final SM2Engine.Mode DIGEST = SM2Engine.Mode.C1C3C2;

    /**
     * 生成公钥和私钥
     */
    public static KeyPair generateSm2KeyPair() {
        //使用标准名称创建EC参数生成的参数规范
        final ECGenParameterSpec sm2Spec = new ECGenParameterSpec("sm2p256v1");

        // 获取一个椭圆曲线类型的密钥对生成器
        final KeyPairGenerator kpg;
        try {
            kpg = KeyPairGenerator.getInstance("EC", new BouncyCastleProvider());
            // 使用SM2算法域参数集初始化密钥生成器（默认使用以最高优先级安装的提供者的 SecureRandom 的实现作为随机源）
            // kpg.initialize(sm2Spec);

            // 使用SM2的算法域参数集和指定的随机源初始化密钥生成器
            kpg.initialize(sm2Spec, new SecureRandom());

            // 通过密钥生成器生成密钥对
            return kpg.generateKeyPair();
        } catch (Exception e) {
            log.error(">>>>>>>>>>【ERROR】SM2密钥对生成失败，原因：{}", e.getMessage(), e);
            throw new RuntimeException("SM2密钥对生成失败");
        }
    }

    /**
     * 私钥转换为 {@link ECPrivateKeyParameters}
     *
     * @param key key
     * @return
     * @throws InvalidKeyException
     */
    public static ECPrivateKeyParameters privateKeyToParams(byte[] key) throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException {
        if (key == null) {
            throw new RuntimeException("key must be not null !");
        }
        PrivateKey privateKey = generatePrivateKey(key);
        return (ECPrivateKeyParameters) ECUtil.generatePrivateKeyParameter(privateKey);
    }

    /**
     * 生成私钥
     *
     * @param key key
     * @return
     */
    public static PrivateKey generatePrivateKey(byte[] key) throws NoSuchAlgorithmException, InvalidKeySpecException {
        if (key == null) {
            throw new RuntimeException("key must be not null !");
        }
        KeySpec keySpec = new PKCS8EncodedKeySpec(key);
        return getKeyFactory().generatePrivate(keySpec);
    }

    /**
     * 公钥转换为 {@link ECPublicKeyParameters}
     *
     * @param key key
     * @return
     * @throws InvalidKeyException
     */
    public static ECPublicKeyParameters publicKeyToParams(byte[] key) throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException {
        if (key == null) {
            throw new RuntimeException("key must be not null !");
        }

        // 生成公钥
        KeySpec keySpec = new X509EncodedKeySpec(key);
        PublicKey publicKey = getKeyFactory().generatePublic(keySpec);
        return (ECPublicKeyParameters) ECUtil.generatePublicKeyParameter(publicKey);
    }

    /**
     * 获取{@link KeyFactory}
     *
     * @return {@link KeyFactory}
     */
    private static KeyFactory getKeyFactory() throws NoSuchAlgorithmException {
        final Provider provider = new BouncyCastleProvider();
        return KeyFactory.getInstance("EC", provider);
    }

    /**
     * 加密
     *
     * @param dataStr   数据
     * @param publicKey 公钥
     * @return 加密之后的数据
     */
    public static String encrypt(String dataStr, String publicKey) {
        try {
            return Base64.toBase64String(encrypt(dataStr.getBytes(StandardCharsets.UTF_8), Base64.decode(publicKey)));
        } catch (Exception e) {
            throw new RuntimeException("参数加密异常");
        }
    }

    public static byte[] encrypt(byte[] data, byte[] publicKey) throws Exception {
        CipherParameters pubKeyParameters = new ParametersWithRandom(publicKeyToParams(publicKey));
        SM2Engine engine = new SM2Engine(DIGEST);
        engine.init(true, pubKeyParameters);
        return engine.processBlock(data, 0, data.length);
    }


    /**
     * 解密
     *
     * @param base64Data       数据
     * @param base64PrivateKey 私钥
     * @return 解密之后的数据
     */
    public static String decrypt(String base64Data, String base64PrivateKey) throws Exception {
        byte[] data = Base64.decode(base64Data);
        byte[] privateKey = Base64.decode(base64PrivateKey);
        return new String(decrypt(data, privateKey), CHARSETS);
    }

    public static byte[] decrypt(byte[] data, byte[] privateKey) throws Exception {
        CipherParameters privateKeyParameters = privateKeyToParams(privateKey);
        SM2Engine engine = new SM2Engine(DIGEST);
        engine.init(false, privateKeyParameters);
        return engine.processBlock(data, 0, data.length);
    }

    /**
     * 签名
     *
     * @param data 数据
     * @return 签名
     */
    public static byte[] sign(byte[] data, byte[] privateKey) throws Exception {
        SM2Signer signer = new SM2Signer();
        CipherParameters param = new ParametersWithRandom(privateKeyToParams(privateKey));
        signer.init(true, param);
        signer.update(data, 0, data.length);
        return signer.generateSignature();
    }

    /**
     * 用公钥检验数字签名的合法性
     *
     * @param data      数据
     * @param sign      签名
     * @param publicKey 公钥
     * @return 是否验证通过
     */
    public static boolean verify(byte[] data, byte[] sign, byte[] publicKey) throws Exception {
        SM2Signer signer = new SM2Signer();
        CipherParameters param = publicKeyToParams(publicKey);
        signer.init(false, param);
        signer.update(data, 0, data.length);
        return signer.verifySignature(sign);
    }
}
