package com.demo.cert;

import com.demo.util.SM2Utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.X509KeyUsage;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.encoders.Base64;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

/**
 * <p> @Title SM2CertGenerateTest
 * <p> @Description SM2证书生成测试类
 *
 * @author ACGkaka
 * @date 2024/8/23 15:41
 */
@Slf4j
public class SM2CertGenerateTest {

    /**
     * BouncyCastle算法提供者
     */
    private static final Provider BC = new BouncyCastleProvider();
    private static final String KEY_STORE_TYPE = "PKCS12";

    public static void main(String[] args) throws NoSuchAlgorithmException, OperatorCreationException, IOException, CertificateException, KeyStoreException {
        // 生成密钥对
        KeyPair keyPair = SM2Utils.generateSm2KeyPair();

        // 使用私钥创建算法提供容器
        ContentSigner sigGen = new JcaContentSignerBuilder("SM3withSM2")
                .setProvider(BC)
                .build(keyPair.getPrivate());

        // 构造X.509 第3版的证书构建者
        X509v3CertificateBuilder certGen = new JcaX509v3CertificateBuilder(
                // 颁发者信息
                createStdBuilder().build(),
                // 证书序列号
                BigInteger.valueOf(1),
                // 证书生效日期
                new Date(),
                // 证书失效日期
                DateUtils.addYears(new Date(), 1),
                // 使用者信息（PS：由于是自签证书，所以颁发者和使用者DN都相同）
                createStdBuilder().build(),
                // 证书公钥
                keyPair.getPublic())
                /*
                设置证书扩展
                证书扩展属性，请根据需求设定，参数请参考 《RFC 5280》
                 */
                // 设置密钥用法
                .addExtension(Extension.keyUsage, false,
                        new X509KeyUsage(X509KeyUsage.digitalSignature | X509KeyUsage.nonRepudiation))
                // 设置扩展密钥用法：客户端身份认证、安全电子邮件
                .addExtension(Extension.extendedKeyUsage, false, extendedKeyUsage())
                // 基础约束,标识是否是CA证书，这里false标识为实体证书
                .addExtension(Extension.basicConstraints, false, new BasicConstraints(false))
                // Netscape Cert Type SSL客户端身份认证
                .addExtension(MiscObjectIdentifiers.netscapeCertType, false, new NetscapeCertType(NetscapeCertType.sslClient));

        // 将证书构造参数装换为X.509证书对象
        X509Certificate certificate = new JcaX509CertificateConverter()
                .setProvider(BC)
                .getCertificate(certGen.build(sigGen));

        // 保存为证书文件
//        makeCertFile(certificate, Paths.get("D:\\keystore\\sm2.cer"));

        // 创建KeyStore
        KeyStore store = KeyStore.getInstance(KEY_STORE_TYPE);
        store.load(null);

        String password = "123456";
        store.setKeyEntry("ACGkaka", keyPair.getPrivate(), password.toCharArray(), new Certificate[]{certificate});

        try (FileOutputStream fout = new FileOutputStream("D:\\keystore\\sm2.p12");) {
            store.store(fout, password.toCharArray());
        }
    }

    /**
     * 表示信息构造（DN）
     */
    private static X500NameBuilder createStdBuilder() {
        X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
        // 国家代码
        builder.addRDN(BCStyle.C, "CN");
        // 组织
        builder.addRDN(BCStyle.O, "HZNU");
        // 省份
        builder.addRDN(BCStyle.ST, "Zhejiang");
        // 地区
        builder.addRDN(BCStyle.L, "Hangzhou");
        return builder;
    }

    /**
     * 获取一个扩展密钥用途的序列（可选）
     */
    public static DERSequence extendedKeyUsage() {
        // 构造容器对象
        ASN1EncodableVector vector = new ASN1EncodableVector();
        // 客户端身份认证
        vector.add(KeyPurposeId.id_kp_clientAuth);
        // 安全电子邮件
        vector.add(KeyPurposeId.id_kp_emailProtection);
        return new DERSequence(vector);
    }

    /**
     * 保存证书
     */
    public static void makeCertFile(X509Certificate x509Certificate, Path savePath) throws CertificateEncodingException, IOException, IOException {
        if (Files.exists(savePath)) {
            // 删除已存在文件
            Files.deleteIfExists(savePath);
        }
        // 创建文件
        Files.createFile(savePath);
        // 获取ASN.1编码的证书字节码
        byte[] asn1BinCert = x509Certificate.getEncoded();
        // 编码为BASE64 便于传输
        byte[] base64EncodedCert = Base64.encode(asn1BinCert);
        // 写入文件
        Files.write(savePath, base64EncodedCert);
    }


}
