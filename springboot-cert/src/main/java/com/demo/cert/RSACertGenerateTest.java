package com.demo.cert;

import com.demo.dto.CertDTO;
import com.demo.util.PfxGenerateUtil;
import lombok.extern.slf4j.Slf4j;

import java.security.cert.X509Certificate;
import java.util.Objects;

/**
 * <p> @Title RSACertGenerateTest
 * <p> @Description RSA证书生成测试类
 *
 * @author ACGkaka
 * @date 2024/8/23 15:41
 */
@Slf4j
public class RSACertGenerateTest {

    public static void main(String[] args) {
        CertDTO certInfo = new CertDTO("ACGkaka", "test.mail", "123456");
        // 生成个人证书（RSA）
//        applyLocalCert(certInfo, "personCert.p12", "d:\\test\\", 1);
        // 生成企业证书（RSA）
        applyLocalCert(certInfo, "enterpriseCert.p12", "d:\\test\\", 2);
    }

    /**
     * 申请本地服务器生成的证书 目前只支持RSA证书
     *
     * @param certDTO 证书信息
     * @param fileName  文件名称
     * @param filePath  文件路径
     * @param certType  证书类型（1个人 2企业）
     */
    public static void applyLocalCert(CertDTO certDTO, String fileName, String filePath, Integer certType) {
        filePath = filePath + fileName;
        try {
            //创建本地证书
            assert certDTO != null;
            X509Certificate certificate;
            if (Objects.equals(certType, 1)) {
                // 个人证书
                certificate = PfxGenerateUtil.createPersonPfx(filePath, certDTO.getPassword(), certDTO.getUserName());
            } else {
                // 企业证书
                certificate = PfxGenerateUtil.createOrgPfx(filePath, certDTO.getPassword(), certDTO.getUserName(), "XXX股份有限公司");
            }
            if (certificate == null) {
                throw new RuntimeException("创建本地证书失败...");
            }
            log.info("创建本地证书成功...");
            certDTO.setEmail(null);

        } catch (Exception e) {
            log.error("创建本地证书失败... {}", e.getMessage());
            throw new RuntimeException("创建本地证书失败");
        }
    }
}
