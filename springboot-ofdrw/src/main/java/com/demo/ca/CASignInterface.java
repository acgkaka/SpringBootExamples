package com.demo.ca;

/**
 * CA签署接口类
 */
public interface CASignInterface {

    /**
     * 签署
     */
    byte[] sign(byte[] data);

}
