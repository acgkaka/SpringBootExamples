package com.demo.util;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.ofdrw.gm.cert.PKCS12Tools;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * 原生PKCS12证书工具类
 */
public class NativePKCS12Tools {
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public static X509Certificate readX509Certificate(String cerBase64)
            throws CertificateException, IOException, NoSuchProviderException {
        byte[] bytes = Base64.decodeBase64(cerBase64);
        CertificateFactory cf = CertificateFactory.getInstance("X.509", BouncyCastleProvider.PROVIDER_NAME);
        X509Certificate cert = (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(bytes));
        return cert;
    }

    public static PrivateKey ReadPrvKey(String certBase64, String alias, String pwd){
        InputStream inputStream = null;
        try {
            inputStream = new ByteArrayInputStream(Base64.decodeBase64(certBase64));
            PrivateKey privateKey = PKCS12Tools.ReadPrvKey(inputStream, alias, pwd);
            return privateKey;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(inputStream!=null){
                try {
                    inputStream.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
        return null;

    }
    public static Certificate ReadUserCert(String certBase64, String alias, String pwd){
        InputStream inputStream = null;
        try {
            inputStream = new ByteArrayInputStream(Base64.decodeBase64(certBase64));
            Certificate certificate = PKCS12Tools.ReadUserCert(inputStream, alias, pwd);
            return certificate;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(inputStream!=null){
                try {
                    inputStream.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
        return null;
    }

}
