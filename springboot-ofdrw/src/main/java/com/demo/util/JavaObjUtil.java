package com.demo.util;

import org.ofdrw.converter.utils.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * java对象工具类
 */
public class JavaObjUtil {

    /**
     * 判断对象中属性值是否全为空
     * @param object
     * @ignoreFileName 忽略验证的参数
     * @return
     */
    public static java.util.List<String> checkObjAllFieldsIsNull(Object object, String ... ignoreFieldName) {
        System.out.println("校验数据....");
        if (null == object) {
            return null;
        }
        java.util.List<String> list = new ArrayList<>();

        try {
            System.out.println(object.getClass().getDeclaredFields().length);
            for (Field f : object.getClass().getDeclaredFields()) {
                f.setAccessible(true);
//                System.out.print(f.getName() + ":");
//                System.out.println(f.get(object));
                boolean ignore =false;
                for (String str : ignoreFieldName){
                    if(f.getName().equals(str)){
                        System.out.println(f.getName()+":该值忽略验证");
                        ignore=true;
                        break;
                    }
                }
                if(ignore){
                    continue;
                }

                if (!ignore &&f.get(object) != null && !StringUtils.isBlank(f.get(object).toString())) {
//                    System.out.println("属性不为空");
                }else{
                    list.add(f.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
