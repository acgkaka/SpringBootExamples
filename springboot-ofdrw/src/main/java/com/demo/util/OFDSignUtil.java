package com.demo.util;

import com.demo.dto.ItemMapDTO;
import com.demo.dto.SignDetail;
import com.demo.dto.SignInfo;
import com.demo.service.ESLBuilderService;
import com.demo.sign.OFDSigner;
import com.demo.sign.BuildOFDSignerBase64;
import com.demo.sign.BuildOFDSignerRideBase64;
import com.demo.sign.Pos;
import com.demo.util.sm2.SM2Util;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.ofdrw.gm.cert.PKCS12Tools;
import org.ofdrw.reader.OFDReader;
import org.ofdrw.reader.PageInfo;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.List;
import java.util.*;

/**
 * @author ACGkaka
 * @description: ofd签署工具类
 */
@Slf4j
public class OFDSignUtil {

    /**
     * 字体路径（宋体）
     */
    private static final String fontPath = "C:\\Windows\\Fonts\\simsun.ttc,0";

    /**
     * 读取证书内容
     * @param base64String
     * @return
     */
    public static String fileToInputStream(String base64String){
        try {
            Certificate signCert = PKCS12Tools.ReadUserCert(base64ToInputStream(base64String), "ACGkaka", "123456");
            byte[]  encoded = signCert.getEncoded();
            return Base64.getEncoder().encodeToString(encoded);
        } catch (IOException | GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }


    public static InputStream base64ToInputStream(String base64String) throws IOException {
        // 将 Base64 字符串解码为字节数组
        byte[] bytes = Base64.getDecoder().decode(base64String);
        // 创建 ByteArrayInputStream，并传入解码后的字节数组
        InputStream inputStream = new ByteArrayInputStream(bytes);

        return inputStream;
    }


    /**
     * 关键字签署
     *
     * @param reqBase64  待签署文件base64
     * @param eslBase64  印章文件base64
     * @param certBase64 用户证书base64
     * @param apList     印章坐标
     * @return 签署后文件
     */
    public static byte[] signByXy(String reqBase64, String eslBase64, String certBase64, List<Pos> apList) {
        log.info("OFD关键字签署开始");
        BuildOFDSignerBase64 buildOFDSigner = new BuildOFDSignerBase64()
                .setOfdReqBase64(reqBase64)
                .setEslBase64(eslBase64)
                .setSignMode(0)
                .setUseSealerCertBase64(fileToInputStream(certBase64))
                .setApList(apList);
        try {
            byte[] bytes = OFDSigner.signByXyBase64(buildOFDSigner, data -> {
                try {
                    PrivateKey sealerPrvKey = NativePKCS12Tools.ReadPrvKey(certBase64, "ACGkaka", "123456");
                    Signature sg = null;
                    try {
                        sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.initSign(sealerPrvKey);
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.update(data);
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    byte[] sigVal = new byte[0];
                    try {
                        sigVal = sg.sign();
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    System.out.println(sigVal.length);

                    return sigVal;
                } catch (Exception e) {
                    log.error("OFD关键字签署异常", e);
                    throw new IllegalArgumentException("关键字签署异常");
                }
            });
            log.info("关键字签署完成,签署后文件大小:{}kb", bytes == null ? 0 : bytes.length / 1024);
            return bytes;
        } catch (Exception e) {
            log.error("OFD关键字签署异常", e);
            throw new IllegalArgumentException("关键字签署异常");
        }
    }

    /**
     * 骑缝签署
     *
     * @param reqBase64  待签署文件base64
     * @param eslBase64  印章文件base64
     * @param certBase64 用户证书base64
     * @param side       Left左骑缝 Right右骑缝(默认)
     * @return 签署后文件
     */
    public static byte[] signByRidingStampPos(String reqBase64, String eslBase64, String certBase64, String side, double offset) {
        log.info("OFD骑缝签署开始");
        BuildOFDSignerRideBase64 buildOFDSigner = new BuildOFDSignerRideBase64()
                .setOfdReqBase64(reqBase64)
                .setEslBase64(eslBase64)
                .setSignMode(0)
                .setUseSealerCertBase64(fileToInputStream(certBase64))
                .setSide(side)
                .setOffset(offset);
        try {
            byte[] bytes = OFDSigner.signByRidingStampPosBase64(buildOFDSigner, data -> {
                try {
                    PrivateKey sealerPrvKey = NativePKCS12Tools.ReadPrvKey(certBase64, "ACGkaka", "123456");
                    Signature sg = null;
                    try {
                        sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.initSign(sealerPrvKey);
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.update(data);
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    byte[] sigVal = new byte[0];
                    try {
                        sigVal = sg.sign();
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    System.out.println(sigVal.length);

                    return sigVal;
                } catch (Exception e) {
                    log.error("OFD骑缝签署异常", e);
                    throw new IllegalArgumentException("骑缝签署异常");
                }
            });
            log.info("骑缝签署完成,签署后文件大小:{}kb", bytes == null ? 0 : bytes.length / 1024);
            return bytes;
        } catch (Exception e) {
            log.error("OFD骑缝签署异常", e);
            throw new IllegalArgumentException("骑缝签署异常");
        }
    }

/*   public static void main(String[] args) throws Exception {
//        Constant.FONTS_PATH = "C:\\Users\\wangtianteng\\Desktop\\fsdownload\\SimSun.ttf";
//        String text = "去一般般吧啊啊现场v徐";
//
//        // 文本区域的宽高
//        int width = 400;
//        int height = 300;
//
//        // 创建 JTextPane 并设置样式
//        JTextPane textPane = new JTextPane();
//        textPane.setText(text);
//        textPane.setEditable(false);
//        textPane.setFont(new Font(Constant.FONTS_PATH, Font.PLAIN, 20));
//        textPane.setForeground(Color.RED);
//        textPane.setOpaque(false);
//        textPane.setSize(width,height);
//
//        // 设置文本对齐
//        StyledDocument doc = textPane.getStyledDocument();
//        SimpleAttributeSet center = new SimpleAttributeSet();
//        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER); // 水平居中
//        doc.setParagraphAttributes(0, doc.getLength(), center, false);
//
//        // 创建 BufferedImage 并设置透明背景
//        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
//        Graphics2D g2d = image.createGraphics();
//        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
//        g2d.setFont(new Font(Constant.FONTS_PATH, Font.PLAIN, 20));
//        g2d.fillRect(0, 0, width, height);
//
//        // 将 JTextPane 绘制到 BufferedImage 上
//        textPane.paint(g2d);
//
//        // 释放资源
//        g2d.dispose();
//
//        // 保存图片文件
//        byte[] src = new byte[0];
//        try {
//            String outputFile = "C:\\Users\\wangtianteng\\Desktop\\fsdownload\\text_image2.png";
//            ImageIO.write(image, "png", new File(outputFile));
//             src = ImageUtil.bufferedImageToByteArray(image,"png");
//            System.out.println("Image saved as: " + outputFile);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        String req = "C:\\Users\\wangtianteng\\Desktop\\未确定\\test_.ofd";
        String reqBase64 = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(new File(req)));
        OFDReader reader = new OFDReader(new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(req))));
        int page = Integer.parseInt("2");
        PageInfo pageInfo = reader.getPageInfo(page);
        float pageWidth = pageInfo.getSize().getWidth().floatValue();

        float fileWidth= 630F;

        float ratio = pageWidth / fileWidth;


        String usercertpath = "C:\\Users\\wangtianteng\\Desktop\\fsdownload\\b5b023f959c244bf8eeb617544a5e205.p12";
        String certBase64 = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(new File(usercertpath)));

        String text_image2 = "C:\\Users\\wangtianteng\\Desktop\\1711941178598.png";
        String txtBase64 = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(new File(text_image2)));

        byte[] build = EslBuildUtil.build(UUID.randomUUID().toString().replace("-", ""),
                "印章",
                txtBase64,
                400,
                300,
                "sign",
                new Date(),
                new Date(),
                certBase64,
                certBase64,
                "123456",
                "123456");



            List<Pos> apList = new ArrayList<>();
            Pos pos = new Pos();
            pos.setPage(2);
            pos.setX(ratio * 272.56f);
            pos.setY(ratio * 255.90f);
            pos.setWidth(ratio *250F);
            pos.setHeigh(ratio *100f);
            apList.add(pos);
            // 坐标签署
            byte[] bytes = signByXy(reqBase64, Base64.getEncoder().encodeToString(build), certBase64, apList);
            FileUtils.writeByteArrayToFile(new File("C:\\Users\\wangtianteng\\Desktop\\fsdownload\\xy.ofd"), bytes);
            // 骑缝签署
            String side = "Right";
            double offset = 40.0d;
            byte[] bytes1 = signByRidingStampPos(reqBase64, Base64.getEncoder().encodeToString(build), certBase64, side, offset);
            FileUtils.writeByteArrayToFile(new File("C:\\Users\\wangtianteng\\Desktop\\fsdownload\\RidingStampPos.ofd"), bytes1);

    }*/


    public static void main(String[] args) throws IOException, GeneralSecurityException {
        byte[] pdfBytes = FileUtils.readFileToByteArray(new File("D:\\test_2页.ofd"));
        byte[] certBytes = FileUtils.readFileToByteArray(new File("D:\\keystore\\sm2.p12"));
        String certBase64 = Base64.getEncoder().encodeToString(certBytes);

        // 坐标签署
//        List<ItemMapDTO> itemList = getItemList();
//        byte[] newPdfBytes = OFDSignUtil.signSm2PDFBySeal(pdfBytes, itemList, "els", certBase64);
//        FileUtils.writeByteArrayToFile(new File("D:\\test\\test2.ofd"), newPdfBytes);

        // 骑缝签署
        String side = "Right";
        double offset = 40.0d;
        byte[] eslBytes = FileUtils.readFileToByteArray(new File("D:\\keystore\\sm2-seal.esl"));
        String eslBase64 = Base64.getEncoder().encodeToString(eslBytes);
        String pdfBase64 = Base64.getEncoder().encodeToString(pdfBytes);
        byte[] bytes1 = OFDSignUtil.signByRidingStampPos(pdfBase64, eslBase64, certBase64, side, offset);
        FileUtils.writeByteArrayToFile(new File("D:\\test\\test3.ofd"), bytes1);
    }

    private static List<ItemMapDTO> getItemList() throws IOException {
        List<ItemMapDTO> itemList = new ArrayList<>();

        // 封装签署规则
        SignInfo signInfo = new SignInfo();
        signInfo.setPage(1);
        signInfo.setX(30.0F);
        signInfo.setY(30.0F);
        signInfo.setSignW(20.0F);
        signInfo.setSignH(10.0F);
        signInfo.setRotate(0f);

        ItemMapDTO itemMap = new ItemMapDTO();
        // 签署规则
        itemMap.setRules(signInfo.getPage() + "," + signInfo.getX() + "," + signInfo.getY() + "," +
                signInfo.getSignW() + "," + signInfo.getSignH() + "," +
                signInfo.getRotate());
        // 印章图片
        byte[] eslBytes = FileUtils.readFileToByteArray(new File("D:\\keystore\\sm2-sinature.esl"));
        itemMap.setImage(Base64.getEncoder().encodeToString(eslBytes));

        itemList.add(itemMap);
        return itemList;
    }

    public static byte[] signSm2PDFBySeal(byte[] ofdArray, List<ItemMapDTO> signItemList, String buildSealerCertBase64,String certBase64) {
        log.info("ofdArray" + ofdArray);
        log.info("signItemList" + signItemList);
        log.info("buildSealerCertBase64" + buildSealerCertBase64);
        log.info("certBase64" + certBase64);
        try {
            // pdf签章开始
            OFDReader reader = null;

            Date validStartDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, 1);
            Date validEndDate = calendar.getTime();

            for (ItemMapDTO itemMap : signItemList) {

                reader = new OFDReader(new ByteArrayInputStream(ofdArray));

                String reqBase64 = Base64.getEncoder().encodeToString(ofdArray);
                List<Pos> posList = new ArrayList<>();

                String rules = itemMap.getRules();
                SignDetail signDetail = itemMap.getSignDetail();
                Image image = null;

                String ruleItem[] = rules.split(",");
                int page = Integer.parseInt(ruleItem[0]);
                PageInfo pageInfo = reader.getPageInfo(page);
                float pageWidth = pageInfo.getSize().getWidth().floatValue();
                float pageHeight = pageInfo.getSize().getHeight().floatValue();
                float fLeft = Float.parseFloat(ruleItem[1]) / 3; // x
                float y = Float.parseFloat(ruleItem[2]) / 3;
                float signW = Float.parseFloat(ruleItem[3]) / 3;
                float signH = Float.parseFloat(ruleItem[4]) / 3;
                float rotate = Float.parseFloat(ruleItem[5]);
                float signWx = Float.parseFloat(ruleItem[3]);
                float signHx = Float.parseFloat(ruleItem[4]);
                float fUp = pageHeight - y;
                float fBottom = fUp - signH;
                Rectangle rect = null;
                //初始化pos
                Pos pos = new Pos();
                pos.setPage(page);
                pos.setX(fLeft);
                pos.setY(y);
                pos.setWidth(signW);
                pos.setHeigh(signH);
                posList.add(pos);
                byte[] imageByte = null;
                if (signDetail == null || StringUtils.isNotEmpty(signDetail.getFlexDirection())) {
                    Object cacheImage = itemMap.getImage();
                    log.info("itemMap for:{}", itemMap);
                    log.info("cacheImage for:{}", cacheImage);
                    if (cacheImage instanceof String) {
                        String eslBase64 = (String) cacheImage;
                        //todo 正式上线需要更换
                        ofdArray = signByXy(reqBase64, eslBase64, certBase64, posList);
                        continue;
                    } else {
                        if (cacheImage instanceof Image) {
                            image = (Image) cacheImage;
                        } else if (cacheImage instanceof byte[]) {
                            image = Image.getInstance((byte[]) cacheImage);
                        } else {
                            throw new IllegalArgumentException("不支持的印章类型");
                        }
                        rect = new Rectangle(fLeft, fBottom, fLeft + signW, fUp);
                        image.scaleToFit(rect.getWidth(), rect.getHeight());
                    }
                    if (Float.compare(0, rotate) != 0) {
                        image.setRotationDegrees(-rotate);
                    }
                    imageByte = image.getOriginalData();
                } else {
                    log.info("valSetting.getFontSize() for:{}", signDetail.getFontSize());
                    log.info("valSetting.getFontFamily() for:{}", signDetail.getFontFamily());
                    log.info("valSetting.getFontColor() for:{}", signDetail.getFontColor());
                    log.info("valSetting.getTextAlign() for:{}", signDetail.getTextAlign());
                    log.info("valSetting.getTextVlign() for:{}", signDetail.getTextVlign());
                    log.info("valSetting.getContent() for:{}", signDetail.getContent());
                    imageByte = getOfdTextImage(signDetail.getFontSize().intValue()
                            , signDetail.getFontColor(), signDetail.getTextAlign(),
                            signDetail.getTextVlign(), signDetail.getContent(), ((Float) signWx).intValue(), ((Float) signHx).intValue(), rotate);
                }
                //todo 正式上线需要更换
                log.info("imageByte for:{}", Base64.getEncoder().encodeToString(imageByte));
                log.info("signW for:{}", signW);
                log.info("signH for:{}", signH);
                log.info("X for:{}", fLeft);
                log.info("Y for:{}", y);
                ESLBuilderService sealTemplateService = SpringUtils.getBean(ESLBuilderService.class);
                String eslBase64 = sealTemplateService.createESLFile(
                        Base64.getEncoder().encodeToString(imageByte),
                        certBase64,
                        (int) signWx,
                        (int) signHx,
                        validStartDate,
                        validEndDate);
                ofdArray = signByXy(reqBase64, eslBase64, certBase64, posList);
            }

            try {
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
                log.error("关闭pdf资源发生异常：：{}", e.getMessage());
            }

            return ofdArray;
        } catch (Exception e) {
            log.error("文件签署发生异常，原因：{}", e.getMessage(), e);
            throw new RuntimeException("文件签署发生异常");
        }
    }


    public static byte[] getOfdTextImage(int fontSize, String fontColor, String textAlign, String textVlign, String content, int width, int height, Float rotate) {
        Font font = new Font(fontPath, Font.PLAIN, fontSize);

        // 创建 JTextPane 并设置样式
        JTextPane textPane = new JTextPane();
        textPane.setText(content);
        textPane.setEditable(false);
        textPane.setFont(font);
        textPane.setForeground(hexToColor(fontColor));
        textPane.setOpaque(false);
        textPane.setSize(width, height);
        textPane.setBackground(new Color(0, 0, 0, 0));

        // 设置文本对齐
        StyledDocument doc = textPane.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        if (StringUtils.equals("right", textAlign)) {
            // 水平靠右
            StyleConstants.setAlignment(center, StyleConstants.ALIGN_RIGHT);
        } else if (StringUtils.equals("center", textAlign)) {
            // 水平居中
            StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        } else {
            // 水平靠左
            StyleConstants.setAlignment(center, StyleConstants.ALIGN_LEFT);
        }
        doc.setParagraphAttributes(0, doc.getLength(), center, false);

        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        ;
        Graphics2D g2d = bufferedImage.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setFont(font);

        // 将 JTextPane 绘制到 BufferedImage 上
        textPane.paint(g2d);

        if (Float.compare(0, rotate) != 0) {
            // 旋转画布
            g2d.rotate(Math.toRadians(rotate), width / 2, height / 2);
        }
        // 释放资源
        g2d.dispose();
        return ImageUtil.bufferedImageToByteArray(bufferedImage, "png");
    }

    public static Color hexToColor(String hex) {
        // 去除#号并转为整数
        int rgb = Integer.parseInt(hex.substring(1), 16);
        // R通道
        int red = (rgb >> 16) & 255;
        // G通道
        int green = (rgb >> 8) & 255;
        // B通道
        int blue = rgb & 255;
        // 创建新的Color对象
        return new Color(red, green, blue);
    }


    public static PrivateKey SO48996981BCparseECprivate() throws Exception {
//        Provider BC = new BouncyCastleProvider();
//
//        byte[] server_sec1 = DatatypeConverter.parseBase64Binary("MHcCAQEEIMdlnFgad6HqoOcwue0ZR6d+pLWhKVcj3wNEkU5rOzqwoAoGCCqBHM9VAYItoUQDQgAEwbz5gPcCeEFXbjA1VP4kmyQaq79XDbfTsom58Iw92MltcCa6bXK2Dvys87xUwC/MM9MyQun3/TmXE1UXLQJNfQ==");
//
//        ASN1Sequence seq = ASN1Sequence.getInstance(server_sec1);
//
//        org.bouncycastle.asn1.sec.ECPrivateKey pKey = org.bouncycastle.asn1.sec.ECPrivateKey.getInstance(seq);
//
//        AlgorithmIdentifier algId = new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, pKey.getParameters());
//
//        byte[] server_pkcs8 = new PrivateKeyInfo(algId, pKey).getEncoded();
//
//        KeyFactory fact = KeyFactory.getInstance("EC", BC);
//
//        PrivateKey pkey = fact.generatePrivate(new PKCS8EncodedKeySpec(server_pkcs8));
//// for test only:
//
//        log.info(pkey.getClass().getName() + " " + pkey.getAlgorithm());
//        return pkey;

        KeyPair keyPair = SM2Util.generateKeyPair();
        KeySpec keySpec = new PKCS8EncodedKeySpec(keyPair.getPrivate().getEncoded());
        final Provider provider = new BouncyCastleProvider();
        KeyFactory keyFactory = KeyFactory.getInstance("EC", provider);
        return keyFactory.generatePrivate(keySpec);

    }

}
