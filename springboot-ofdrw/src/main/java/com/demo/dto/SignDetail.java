package com.demo.dto;


import lombok.Data;

import java.util.List;

@Data
public class SignDetail {

    /**
     * 字体大小、控件大小
     */
    private Float fontSize;

    /**
     * 字体颜色，默认黑色
     */
    private String fontColor = "#000000";

    /**
     * 字体名称
     */
    private String fontFamily;

    /**
     * 水平对其方式，可选值left\right\center
     */
    private String textAlign;

    private String textVlign;

    /**
     * 是否必填
     */
    private Boolean required = false;

    /**
     * 内容
     */
    private String content;

    private String elementType;

    /**以下参数用于单选多选控件 start**/
    private String textTitle;
    /**
     * 控件个数
     */
    private Integer comLength;

    /**
     * 控件间隔
     */
    private Integer margin;

    /**
     * 排布方向 column纵向 row横向
     */
    private String flexDirection;

    /**
     * 选中值
     */
    private List<Integer> value;
    /**以下参数用于单选多选控件 end**/

    /**
     * 是否必填
     */
    private Boolean isVerify = false;

}
