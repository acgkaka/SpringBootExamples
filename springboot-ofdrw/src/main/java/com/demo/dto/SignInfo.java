package com.demo.dto;


import lombok.Data;

import java.io.Serializable;

/**
 *
 * 签名信息
 * </p>
 *
 * @author ACGkaka
 * @since 2021-08-20
 */
@Data
public class SignInfo implements Serializable {

    /**
     * 文件ID
     */
    private String fileId;

    /**
     * 签章页码
     */
    private Integer page;

    /**
     * x坐标
     */
    private Float x;

    /**
     * y坐标
     */
    private Float y;

    /**
     * 签名ID
     */
    private Long sealId;

    /**
     * 宽
     */
    private Float signW;

    /**
     * 高
     */
    private Float signH;

    /**
     *  pdf文件页高
     */
    private Float pageHeight = 842f;

    /**
     * 角度旋转
     */
    private Float rotate;

    /**
     * 类型 （textarea文本 sign个人签字 seal机构签章 time时间 crossPage骑缝章 checkbox多选 radio单选）
     */
    private String type;

    /**
     * 手绘签名缓存ID
     */
    private String writeId;

    /**
     * 用于textarea time类型的子属性
     */
    private SignDetail signDetail;


    /**
     * 以下参数 深圳市场景 专用
     * 电子印章类型，1：法定名称章 2：财务专用章 3：合同专用章 4：法定代表人名章 5：其他自定义
     */
    private String sealType;
    /**
     * 场景类型
     */
    private String sealSource;

}
