package com.demo.dto;

import lombok.Data;

/**
 * <p> @Title ItemMapDTO
 * <p> @Description TODO
 *
 * @author ACGkaka
 * @date 2024/9/12 16:46
 */
@Data
public class ItemMapDTO {

    private Object image;

    /**
     * 签署规则（page,x,y,signW,signH,rotate）
     */
    private String rules;

    private SignDetail signDetail;
}
