package com.demo.example;

import com.demo.ca.CASignInterface;
import com.demo.seal.SESeal;
import com.demo.seal.BuildSESealBase64;
import com.demo.sign.OFDSigner;
import com.demo.sign.BuildOFDSignerBase64;
import com.demo.sign.Pos;
import com.demo.util.NativePKCS12Tools;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.ofdrw.converter.ofdconverter.ImageConverter;
import org.ofdrw.converter.ofdconverter.PDFConverter;
import org.ofdrw.converter.ofdconverter.TextConverter;
import org.ofdrw.gm.cert.PKCS12Tools;
import org.ofdrw.reader.OFDReader;
import org.ofdrw.sign.verify.OFDValidator;
import org.ofdrw.sign.verify.container.SESV4ValidateContainer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.*;

/**
 * <p> @Title OFDExample
 * <p> @Description OFD示例
 *
 * @author ACGkaka
 * @date 2024/11/11 15:51
 */
@Slf4j
public class OFDExample {

    public static void main(String[] args) throws Exception {
        // pdf 转 ofd
//        pdf2Ofd();
        // img 转 ofd
//        img2Ofd();
        // text 转 ofd
//        text2Ofd();

        // 生成ESL印章
        buildEsl();
        // 签署OFD
//        signByXy();
        // 验签OFD
//        signVerify();
    }

    /**
     * pdf 转 ofd
     */
    private static void pdf2Ofd() {
        Path src = Paths.get("D:\\test.pdf");
        Path dst = Paths.get("D:\\test.ofd");
        try (PDFConverter converter = new PDFConverter(dst)) {
            converter.convert(src);
        } catch (IOException e) {
            log.error("pdf 转 ofd 失败，请稍后重试，原因：{}", e.getMessage(), e);
            throw new RuntimeException("pdf 转 ofd 失败，请稍后重试", e);
        }
        System.out.println("pdf 转 ofd 成功，路径： " + dst.toAbsolutePath());
    }

    /**
     * img 转 ofd
     */
    private static void img2Ofd() {
        Path src = Paths.get("D:\\test.png");
        Path dst = Paths.get("D:\\test.ofd");
        try (ImageConverter converter = new ImageConverter(dst)) {
            converter.convert(src);
            converter.convert(src);
            converter.convert(src);
        } catch (IOException e) {
            log.error("img 转 ofd 失败，请稍后重试，原因：{}", e.getMessage(), e);
            throw new RuntimeException("img 转 ofd 失败，请稍后重试", e);
        }
        System.out.println("img 转 ofd 成功，路径： " + dst.toAbsolutePath());
    }

    /**
     * text 转 ofd
     */
    private static void text2Ofd() {
        Path src = Paths.get("D:\\test.txt");
        Path dst = Paths.get("D:\\test.ofd");
        try (TextConverter converter = new TextConverter(dst)) {
            converter.convert(src);
            converter.convert(src);
            converter.convert(src);
        } catch (IOException e) {
            log.error("text 转 ofd 失败，请稍后重试，原因：{}", e.getMessage(), e);
            throw new RuntimeException("text 转 ofd 失败，请稍后重试", e);
        }
        System.out.println("text 转 ofd 成功，路径： " + dst.toAbsolutePath());
    }

    /**
     * 生成ESL印章
     */
    public static void buildEsl() throws IOException, CertificateEncodingException {
        String imagePath ="D:\\seal.png";
        String sealerCertPath = "D:\\keystore\\sm2.p12";


        String imageBase64 = java.util.Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(new File(imagePath)));
        Path userP12Path = Paths.get(sealerCertPath);
        PrivateKey sealerPrvKey;
        try {
            byte[] bytes = FileUtils.readFileToByteArray(new File(sealerCertPath));
            sealerPrvKey = NativePKCS12Tools.ReadPrvKey(java.util.Base64.getEncoder().encodeToString(bytes), "ACGkaka", "123456");

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Certificate signCert = null ;
        try {
            signCert = PKCS12Tools.ReadUserCert(userP12Path, "ACGkaka", "123456");
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
        byte[] encoded = signCert.getEncoded();
        String s1 = java.util.Base64.getEncoder().encodeToString(encoded);
        System.out.println("s1="+s1);


        System.out.println("imageBase64:"+imageBase64);

        long start = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.add(Calendar.YEAR, 2);
        Date then = now.getTime();
        BuildSESealBase64 seal = new BuildSESealBase64().setEsID(UUID.randomUUID().toString().replace("-", ""))//印章ID
                .setSealImageBase64(imageBase64)//印章图片base64
                .setSealName("ACGkaka测试章")//印章名称
                .setBuildSealerCertBase64(s1)//制章人公钥
                .setHeight(40)//印章高度单位毫米
                .setWidth(40)//印章宽度单位毫米
                .setSesheader("chinamobile sign")//印章头部信息
                .setUseSealerCertBase64(s1)//用章人公钥
                .setValidStartDate(new Date())//印章有效期 传空,固定读用章人证书有效
                .setValidEndDate(then);//印章有效期传空,固定读用章人证书有效
        try {
            PrivateKey finalSealerPrvKey = sealerPrvKey;
            Map<String, Object> stringObjectMap = SESeal.buildBase64(seal, new CASignInterface() {//制章人私钥签名方法(需要调用CA密码机服务)
                @Override
                public byte[] sign(byte[] data) {

                    Signature sg = null;
                    try {
                        sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.initSign(finalSealerPrvKey);
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.update(data);
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    byte[] sigVal = new byte[0];
                    try {
                        sigVal = sg.sign();
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    System.out.println(sigVal.length);

                    return sigVal;
                }
            });
            String s = stringObjectMap.get("base64").toString();
            FileUtils.writeByteArrayToFile(new File("D:\\keystore\\sm2-seal.esl"), java.util.Base64.getDecoder().decode(s));

        } catch (Exception e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("执行耗时间="+(end-start)/1000);
    }

    /**
     * OFD坐标签
     */
    private static void signByXy() throws GeneralSecurityException, IOException {

        Pos pos = new Pos();
        pos.setPage(1);
        pos.setX(100);
        pos.setY(100);
        pos.setWidth(40);
        pos.setHeigh(40);

        List<Pos> apList = new ArrayList<>();
        apList.add(pos);
        String req = "D:\\test.ofd";
        String reqbase = java.util.Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(new File(req)));

        String eslpath = "D:\\keystore\\sm2-sinature.esl";
        String eslbase = java.util.Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(new File(eslpath)));

        Path userP12Path = Paths.get("D:\\keystore\\sm2.p12");

        PrivateKey sealerPrvKey = null ;
        try {
            sealerPrvKey = PKCS12Tools.ReadPrvKey(userP12Path, "ACGkaka", "123456");
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
        Certificate signCert = null ;
        try {
            signCert = PKCS12Tools.ReadUserCert(userP12Path, "ACGkaka", "123456");
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
        byte[] encoded = signCert.getEncoded();
        String s1 = java.util.Base64.getEncoder().encodeToString(encoded);
        System.out.println("s1="+s1);


        BuildOFDSignerBase64 buildOFDSigner = new BuildOFDSignerBase64()
                .setOfdReqBase64(reqbase)
                .setEslBase64(eslbase)
                .setSignMode(0)
                .setUseSealerCertBase64(s1)
                .setApList(apList);
        byte[] bytes = null;
        try {
            PrivateKey finalSealerPrvKey = sealerPrvKey;
            bytes = OFDSigner.signByXyBase64(buildOFDSigner, new CASignInterface() {
                @Override
                public byte[] sign(byte[] data) {
                    Signature sg = null;
                    try {
                        sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.initSign(finalSealerPrvKey);
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.update(data);
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    byte[] sigVal = new byte[0];
                    try {
                        sigVal = sg.sign();
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    System.out.println(sigVal.length);

                    return sigVal;
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        FileUtils.writeByteArrayToFile(new File("D:\\test3.ofd"),bytes);
    }

    /**
     * 验签OFD
     */
    public static void signVerify() throws IOException, GeneralSecurityException {
        Path out = Paths.get("D:\\test3.ofd");
        // 验证
        try (OFDReader reader = new OFDReader(out);
             OFDValidator validator = new OFDValidator(reader)) {
            validator.setValidator(new SESV4ValidateContainer());
            validator.exeValidate();
            System.out.println(">> 验证通过");
        }
    }

}
