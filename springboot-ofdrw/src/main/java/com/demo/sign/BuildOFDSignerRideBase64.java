package com.demo.sign;

/**
 * OFD签署数据
 */
public class BuildOFDSignerRideBase64 {

    //印章
    private String eslBase64;
    //证书路径
    private String useSealerCertBase64;
    //待签署ofd路径
    private String ofdReqBase64;
    //0或者null表示非保护模式,1表示保护模式
    private Integer signMode;
    //    Left,
    //    Right,
    //    Top,
    //    Bottom
    private String side;

    private Integer width;
    private Integer height;
    //相对于原点最近的边的顶点位置，null则默认居中
    private double offset;


    public BuildOFDSignerRideBase64(){

    }

    public String getEslBase64() {
        return eslBase64;
    }

    public BuildOFDSignerRideBase64 setEslBase64(String eslBase64) {
        this.eslBase64 = eslBase64;return this;
    }

    public String getUseSealerCertBase64() {
        return useSealerCertBase64;
    }

    public BuildOFDSignerRideBase64 setUseSealerCertBase64(String useSealerCertBase64) {
        this.useSealerCertBase64 = useSealerCertBase64;return this;
    }

    public String getOfdReqBase64() {
        return ofdReqBase64;
    }

    public BuildOFDSignerRideBase64 setOfdReqBase64(String ofdReqBase64) {
        this.ofdReqBase64 = ofdReqBase64;return this;
    }


    public Integer getSignMode() {
        return signMode;
    }

    public BuildOFDSignerRideBase64 setSignMode(Integer signMode) {
        this.signMode = signMode;return this;
    }


    public Integer getWidth() {
        return width;
    }

    public BuildOFDSignerRideBase64 setWidth(Integer width) {
        this.width = width;return this;
    }

    public Integer getHeight() {
        return height;
    }

    public BuildOFDSignerRideBase64 setHeight(Integer height) {
        this.height = height;return this;
    }

    public String getSide() {
        return side;
    }

    public BuildOFDSignerRideBase64 setSide(String side) {
        this.side = side;
        return this;
    }

    public double getOffset() {
        return offset;
    }

    public BuildOFDSignerRideBase64 setOffset(double offset) {
        this.offset = offset;
        return this;
    }
}
