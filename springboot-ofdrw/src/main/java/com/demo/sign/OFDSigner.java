package com.demo.sign;

import com.demo.ca.CASignInterface;
import com.demo.dto.MySignatureContainer;
import com.demo.util.JavaObjUtil;
import com.demo.util.NativePKCS12Tools;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.dom4j.DocumentException;
import org.ofdrw.core.basicType.ST_Box;
import org.ofdrw.gm.cert.PKCS12Tools;
import org.ofdrw.gm.ses.v4.SESeal;
import org.ofdrw.reader.OFDReader;
import org.ofdrw.reader.keyword.KeywordExtractor;
import org.ofdrw.reader.keyword.KeywordPosition;
import org.ofdrw.sign.NumberFormatAtomicSignID;
import org.ofdrw.sign.SignMode;
import org.ofdrw.sign.signContainer.SESV4Container;
import org.ofdrw.sign.stamppos.NormalStampPos;
import org.ofdrw.sign.stamppos.RidingStampPos;
import org.ofdrw.sign.stamppos.Side;
import org.ofdrw.sign.verify.OFDValidator;
import org.ofdrw.sign.verify.container.SESV4ValidateContainer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * OFD签署数据
 */
@Slf4j
public class OFDSigner {

    /**
     * 坐标签署
     */
    public static void signByXy(BuildOFDSigner buildOFDSigner) throws GeneralSecurityException, IOException {
        log.info("prepare for signByXy...");
        List<String> strings = JavaObjUtil.checkObjAllFieldsIsNull(buildOFDSigner, "signMode");
        if (strings != null && strings.size() > 0) {
            throw new RuntimeException(strings.toString() + "存在为空");
        }
        log.info("BuildOFDSigner={}", buildOFDSigner.toString());

        Security.addProvider(new BouncyCastleProvider());

        Path userP12Path = Paths.get(buildOFDSigner.getUseSealerCertPath());
        Path sealPath = Paths.get(buildOFDSigner.getEslPath());

        String useSealerCertAlias = buildOFDSigner.getUseSealerCertAlias();
        String useSealerCertPwd = buildOFDSigner.getUseSealerCertPwd();
        Integer signMode = buildOFDSigner.getSignMode();


        PrivateKey prvKey = PKCS12Tools.ReadPrvKey(userP12Path, useSealerCertAlias, useSealerCertPwd);
        Certificate signCert = PKCS12Tools.ReadUserCert(userP12Path, useSealerCertAlias, useSealerCertPwd);
        SESeal seal = SESeal.getInstance(Files.readAllBytes(sealPath));

        Path src = Paths.get(buildOFDSigner.getOfdReqPath());
        Path out = Paths.get(buildOFDSigner.getOfdOutPaht());

        // 1. 构造签名引擎
        try (OFDReader reader = new OFDReader(src);
             org.ofdrw.sign.OFDSigner signer = new org.ofdrw.sign.OFDSigner(reader, out, new NumberFormatAtomicSignID())
        ) {
            SESV4Container signContainer = new SESV4Container(prvKey, seal, signCert);
            // 2. 设置签名模式
            if (signMode == 1) {
                signer.setSignMode(SignMode.WholeProtected);
            } else {
                signer.setSignMode(SignMode.ContinueSign);
            }
            // 3. 设置签名使用的扩展签名容器
            signer.setSignContainer(signContainer);
            // 4. 设置显示位置
            for (int i = 0; i < buildOFDSigner.getApList().size(); i++) {
                Pos pos = buildOFDSigner.getApList().get(i);
                signer.addApPos(new NormalStampPos(pos.getPage(), pos.getX(), pos.getY(), pos.getWidth(), pos.getHeigh()));
            }
            // 5. 执行签名
            signer.exeSign();
            // 6. 关闭签名引擎，生成文档。
        }
        // 验证
        try (OFDReader reader = new OFDReader(out);
             OFDValidator validator = new OFDValidator(reader)) {
            validator.setValidator(new SESV4ValidateContainer());
            validator.exeValidate();
            log.info(">> 验证通过");
        }

    }

    /**
     * 坐标签署
     */
    public static byte[] signByXyBase64(BuildOFDSignerBase64 buildOFDSigner, CASignInterface caSignInterface) throws IOException {
        InputStream ofdReqInputStream = null;
        try {
            log.info("prepare for signByXy...");
            List<String> strings = JavaObjUtil.checkObjAllFieldsIsNull(buildOFDSigner, "signMode");
            if (strings != null && strings.size() > 0) {
                throw new RuntimeException(strings.toString() + "存在为空");
            }
            Security.addProvider(new BouncyCastleProvider());

            ofdReqInputStream = new ByteArrayInputStream(Base64.decodeBase64(buildOFDSigner.getOfdReqBase64()));

            Integer signMode = buildOFDSigner.getSignMode();

            X509Certificate signCert = NativePKCS12Tools.readX509Certificate(buildOFDSigner.getUseSealerCertBase64());

            SESeal seal = SESeal.getInstance(Base64.decodeBase64(buildOFDSigner.getEslBase64()));


            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            // 1. 构造签名引擎
            try (OFDReader reader = new OFDReader(ofdReqInputStream);
                 org.ofdrw.sign.OFDSigner signer = new org.ofdrw.sign.OFDSigner(reader, outputStream, new NumberFormatAtomicSignID())
            ) {
                MySignatureContainer signContainer = new MySignatureContainer(seal, signCert, caSignInterface);
                // 2. 设置签名模式
                if (signMode == 1) {
                    signer.setSignMode(SignMode.WholeProtected);
                } else {
                    signer.setSignMode(SignMode.ContinueSign);
                }
                // 3. 设置签名使用的扩展签名容器
                signer.setSignContainer(signContainer);
                // 4. 设置显示位置
                for (int i = 0; i < buildOFDSigner.getApList().size(); i++) {
                    Pos pos = buildOFDSigner.getApList().get(i);
                    signer.addApPos(new NormalStampPos(pos.getPage(), pos.getX(), pos.getY(), pos.getWidth(), pos.getHeigh()));
                }
                // 5. 执行签名
                signer.exeSign();
                // 6. 关闭签名引擎，生成文档。
            }
            return outputStream.toByteArray();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new RuntimeException("文件解析失败");
        } catch (IOException ioException) {
            ioException.printStackTrace();
            throw new RuntimeException("文件解析失败");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ofdReqInputStream != null) {
                ofdReqInputStream.close();
            }
        }
        log.info("签署失败");
        return null;

    }

    /**
     * 关键字签署
     */
    public static void signByKey(BuildOFDSignerKey buildOFDSigner) throws GeneralSecurityException, IOException {
        log.info("prepare for signByKey...");
        List<String> strings = JavaObjUtil.checkObjAllFieldsIsNull(buildOFDSigner, "signMode", "offsetX", "offsetY");
        if (strings != null && strings.size() > 0) {
            throw new RuntimeException(strings + "存在为空");
        }
        log.info("BuildOFDSignerKey={}", buildOFDSigner.toString());

        Security.addProvider(new BouncyCastleProvider());

        Path userP12Path = Paths.get(buildOFDSigner.getUseSealerCertPath());
        Path sealPath = Paths.get(buildOFDSigner.getEslPath());

        String useSealerCertAlias = buildOFDSigner.getUseSealerCertAlias();
        String useSealerCertPwd = buildOFDSigner.getUseSealerCertPwd();
        Integer signMode = buildOFDSigner.getSignMode();

        Integer offsetX = buildOFDSigner.getOffsetX();
        Integer offsetY = buildOFDSigner.getOffsetY();


        PrivateKey prvKey = PKCS12Tools.ReadPrvKey(userP12Path, useSealerCertAlias, useSealerCertPwd);
        Certificate signCert = PKCS12Tools.ReadUserCert(userP12Path, useSealerCertAlias, useSealerCertPwd);
        SESeal seal = SESeal.getInstance(Files.readAllBytes(sealPath));

        Path src = Paths.get(buildOFDSigner.getOfdReqPath());
        Path out = Paths.get(buildOFDSigner.getOfdOutPaht());

        // 1. 构造签名引擎
        String[] keyword = buildOFDSigner.getKey();
        try (OFDReader reader = new OFDReader(src)) {
            List<KeywordPosition> positionList = KeywordExtractor.getKeyWordPositionList(reader, keyword);
            if (positionList.size() > 0) {
                try (org.ofdrw.sign.OFDSigner signer = new org.ofdrw.sign.OFDSigner(reader, out, new NumberFormatAtomicSignID())) {
                    SESV4Container signContainer = new SESV4Container(prvKey, seal, signCert);
                    // 2. 设置签名模式
                    if (signMode == 1) {
                        signer.setSignMode(SignMode.WholeProtected);
                    } else {
                        signer.setSignMode(SignMode.ContinueSign);
                    }
                    // 3. 设置签名使用的扩展签名容器
                    signer.setSignContainer(signContainer);
                    for (KeywordPosition position : positionList) {
                        // 4. 中心点对齐签署
                        ST_Box box = position.getBox();
                        signer.addApPos(new NormalStampPos(position.getPage(), box.getTopLeftX() + box.getWidth() / 2 - 20 + offsetX,
                                box.getTopLeftY() + box.getHeight() / 2 - 20 + offsetY, buildOFDSigner.getWidth(), buildOFDSigner.getHeight()));
                    }

                    // 5. 执行签名
                    signer.exeSign();
                    // 6. 关闭签名引擎，生成文档。
                    System.out.println(">> 生成文件位置: " + out.toAbsolutePath().toAbsolutePath());
                }
            }

        } catch (DocumentException e) {
            e.printStackTrace();
        }
        // 验证
        try (OFDReader reader = new OFDReader(out);
             OFDValidator validator = new OFDValidator(reader)) {
            validator.setValidator(new SESV4ValidateContainer());
            validator.exeValidate();
            log.info(">> 验证通过");
        }

    }

    /**
     * 关键字签署
     */
    public static byte[] signByKeyBase64(BuildOFDSignerKeyBase64 buildOFDSigner, CASignInterface caSignInterface) throws IOException {
        InputStream ofdReqInputStream = null;

        try {
            log.info("prepare for signByKey...");
            List<String> strings = JavaObjUtil.checkObjAllFieldsIsNull(buildOFDSigner, "signMode", "offsetX", "offsetY");
            if (strings != null && strings.size() > 0) {
                throw new RuntimeException(strings.toString() + "存在为空");
            }
            log.info("BuildOFDSignerKey={}", buildOFDSigner.toString());


            Integer signMode = buildOFDSigner.getSignMode();

            Integer offsetX = buildOFDSigner.getOffsetX();
            Integer offsetY = buildOFDSigner.getOffsetY();

            Security.addProvider(new BouncyCastleProvider());
            ofdReqInputStream = new ByteArrayInputStream(Base64.decodeBase64(buildOFDSigner.getOfdReqBase64()));

            X509Certificate signCert = NativePKCS12Tools.readX509Certificate(buildOFDSigner.getUseSealerCertBase64());


            SESeal seal = SESeal.getInstance(Base64.decodeBase64(buildOFDSigner.getEslBase64()));

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            // 1. 构造签名引擎
            String[] keyword = buildOFDSigner.getKey();
            try (OFDReader reader = new OFDReader(ofdReqInputStream)) {
                List<KeywordPosition> positionList = KeywordExtractor.getKeyWordPositionList(reader, keyword);
                if (positionList.size() > 0) {
                    try (org.ofdrw.sign.OFDSigner signer = new org.ofdrw.sign.OFDSigner(reader, outputStream, new NumberFormatAtomicSignID())) {
                        MySignatureContainer signContainer = new MySignatureContainer(seal, signCert, caSignInterface);
                        // 2. 设置签名模式
                        if (signMode == 1) {
                            signer.setSignMode(SignMode.WholeProtected);
                        } else {
                            signer.setSignMode(SignMode.ContinueSign);
                        }
                        // 3. 设置签名使用的扩展签名容器
                        signer.setSignContainer(signContainer);
                        for (KeywordPosition position : positionList) {
                            // 4. 中心点对齐签署
                            ST_Box box = position.getBox();
                            signer.addApPos(new NormalStampPos(position.getPage(), box.getTopLeftX() + box.getWidth() / 2 - 20 + offsetX,
                                    box.getTopLeftY() + box.getHeight() / 2 - 20 + offsetY, buildOFDSigner.getWidth(), buildOFDSigner.getHeight()));
                        }

                        // 5. 执行签名
                        signer.exeSign();
                        // 6. 关闭签名引擎，生成文档。
                    }
                }
                return outputStream.toByteArray();

            } catch (DocumentException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ofdReqInputStream != null) {
                ofdReqInputStream.close();
            }
        }
        log.info("签署失败");
        return null;
    }

    /**
     * 骑缝章签署
     */
    public static byte[] signByRidingStampPosBase64(BuildOFDSignerRideBase64 buildOFDSigner, CASignInterface caSignInterface) throws GeneralSecurityException, IOException {
        InputStream ofdReqInputStream = null;
        try {
            log.info("prepare for signByRidingStampPosBase64...");
            List<String> strings = JavaObjUtil.checkObjAllFieldsIsNull(buildOFDSigner, "offset", "width", "height");
            if (strings != null && strings.size() > 0) {
                throw new RuntimeException(strings.toString() + "存在为空");
            }
            log.info("BuildOFDSignerRideBase64={}", buildOFDSigner.toString());


            Integer signMode = buildOFDSigner.getSignMode();

            double offset = buildOFDSigner.getOffset();

            String side = buildOFDSigner.getSide();
            log.info("side={}", side);

            Security.addProvider(new BouncyCastleProvider());
            ofdReqInputStream = new ByteArrayInputStream(Base64.decodeBase64(buildOFDSigner.getOfdReqBase64()));
            Certificate signCert = NativePKCS12Tools.readX509Certificate(buildOFDSigner.getUseSealerCertBase64());


            SESeal seal = SESeal.getInstance(Base64.decodeBase64(buildOFDSigner.getEslBase64()));

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            // 1. 构造签名引擎
            try (OFDReader reader = new OFDReader(ofdReqInputStream)) {
                try (org.ofdrw.sign.OFDSigner signer = new org.ofdrw.sign.OFDSigner(reader, outputStream, new NumberFormatAtomicSignID())) {
                    MySignatureContainer signContainer = new MySignatureContainer(seal, signCert, caSignInterface);

                    // 2. 设置签名模式
                    if (signMode == 1) {
                        signer.setSignMode(SignMode.WholeProtected);
                    } else {
                        signer.setSignMode(SignMode.ContinueSign);
                    }
                    // 3. 设置签名使用的扩展签名容器
                    signer.setSignContainer(signContainer);
                    switch (side) {
                        case "Left":
                            signer.addApPos(new RidingStampPos(Side.Left, offset, 40, 40));
                            break;
                        case "Right":
                            signer.addApPos(new RidingStampPos(Side.Right, offset, 40, 40));
                            break;
                        case "Top":
                            signer.addApPos(new RidingStampPos(Side.Top, offset, 40, 40));
                            break;
                        case "Bottom":
                            signer.addApPos(new RidingStampPos(Side.Bottom, offset, 40, 40));
                            break;
                    }
                    // 5. 执行签名
                    signer.exeSign();
                    // 6. 关闭签名引擎，生成文档。
                }
                return outputStream.toByteArray();
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("文件解析失败");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ofdReqInputStream != null) {
                ofdReqInputStream.close();
            }
        }
        log.info("签署失败");
        return null;
    }
}