package com.demo.sign;

import java.util.LinkedList;
import java.util.List;

/**
 * OFD签署数据
 */
public class BuildOFDSignerBase64 {

    //印章
    private String eslBase64;
    //证书路径
    private String useSealerCertBase64;

    //待签署ofd路径
    private String ofdReqBase64;
    //0或者null表示非保护模式,1表示保护模式
    private Integer signMode;

    private List<Pos> apList;


    public BuildOFDSignerBase64(){

    }

    public String getEslBase64() {
        return eslBase64;
    }

    public  BuildOFDSignerBase64 setEslBase64(String eslBase64) {
        this.eslBase64 = eslBase64;return this;
    }

    public String getUseSealerCertBase64() {
        return useSealerCertBase64;
    }

    public  BuildOFDSignerBase64 setUseSealerCertBase64(String useSealerCertBase64) {
        this.useSealerCertBase64 = useSealerCertBase64;return this;
    }

    public String getOfdReqBase64() {
        return ofdReqBase64;
    }

    public  BuildOFDSignerBase64 setOfdReqBase64(String ofdReqBase64) {
        this.ofdReqBase64 = ofdReqBase64;
        return this;
    }


    public Integer getSignMode() {
        return signMode;
    }

    public BuildOFDSignerBase64 setSignMode(Integer signMode) {
        this.signMode = signMode;return this;
    }

    public List<Pos> getApList() {
        return apList;
    }

    public BuildOFDSignerBase64 setApList(List<Pos> apList) {
        this.apList = apList;
        return this;
    }

    public BuildOFDSignerBase64 addApList(Pos pos) {
        if (pos == null) {
            return this;
        } else {
            if(this.apList==null){
                this.apList= new LinkedList<>();
            }
            this.apList.add(pos);
            return this;
        }
    }
}
