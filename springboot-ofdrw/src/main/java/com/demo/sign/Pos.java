package com.demo.sign;

import lombok.Data;

/**
 * 签署坐标
 */
@Data
public class Pos {
    private Integer page;
    private float x;
    private float y;
    private float width;
    private float heigh;
}
