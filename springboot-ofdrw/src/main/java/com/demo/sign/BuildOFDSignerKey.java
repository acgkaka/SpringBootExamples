package com.demo.sign;

/**
 * OFD签署数据
 */
public class BuildOFDSignerKey {

    //印章路径
    private String eslPath;
    //证书路径
    private String useSealerCertPath;
    //证书盐
    private String useSealerCertAlias;
    //证书密码
    private String useSealerCertPwd;
    //待签署ofd路径
    private String ofdReqPath;
    //签署输出路径
    private String ofdOutPaht;
    //0或者null表示非保护模式,1表示保护模式
    private Integer signMode;

    private String [] key;

    private Integer width;
    private Integer height;

    private Integer offsetX;
    private Integer offsetY;


    public BuildOFDSignerKey(){

    }


    public String getEslPath() {
        return eslPath;
    }

    public BuildOFDSignerKey setEslPath(String eslPath) {
        this.eslPath = eslPath;
        return this;
    }

    public String getUseSealerCertPath() {
        return useSealerCertPath;
    }

    public BuildOFDSignerKey setUseSealerCertPath(String useSealerCertPath) {
        this.useSealerCertPath = useSealerCertPath;
        return this;
    }

    public String getUseSealerCertAlias() {
        return useSealerCertAlias;
    }

    public BuildOFDSignerKey setUseSealerCertAlias(String useSealerCertAlias) {
        this.useSealerCertAlias = useSealerCertAlias;
        return this;
    }

    public String getUseSealerCertPwd() {
        return useSealerCertPwd;
    }

    public BuildOFDSignerKey setUseSealerCertPwd(String useSealerCertPwd) {
        this.useSealerCertPwd = useSealerCertPwd;return this;
    }

    public String getOfdReqPath() {
        return ofdReqPath;
    }

    public BuildOFDSignerKey setOfdReqPath(String ofdReqPath) {
        this.ofdReqPath = ofdReqPath;return this;
    }

    public String getOfdOutPaht() {
        return ofdOutPaht;
    }

    public BuildOFDSignerKey setOfdOutPaht(String ofdOutPaht) {
        this.ofdOutPaht = ofdOutPaht;return this;
    }

    public Integer getSignMode() {
        return signMode;
    }

    public BuildOFDSignerKey setSignMode(Integer signMode) {
        this.signMode = signMode;return this;
    }

    public String[] getKey() {
        return key;
    }

    public BuildOFDSignerKey setKey(String[] key) {
        this.key = key;return this;
    }

    public Integer getWidth() {
        return width;
    }

    public BuildOFDSignerKey setWidth(Integer width) {
        this.width = width;return this;
    }

    public Integer getHeight() {
        return height;
    }

    public BuildOFDSignerKey setHeight(Integer height) {
        this.height = height;return this;
    }

    public Integer getOffsetX() {
        if(offsetX==null){
            offsetX = 0;
        }
        return offsetX;
    }

    public BuildOFDSignerKey setOffsetX(Integer offsetX) {
        this.offsetX = offsetX;return this;
    }

    public Integer getOffsetY() {
        if(offsetY==null){
            offsetY = 0;
        }
        return offsetY;
    }

    public BuildOFDSignerKey setOffsetY(Integer offsetY) {
        this.offsetY = offsetY;return this;
    }
}
