package com.demo.sign;

import java.util.LinkedList;
import java.util.List;

/**
 * OFD签署数据
 */
public class BuildOFDSigner {

    //印章路径
    private String eslPath;
    //证书路径
    private String useSealerCertPath;
    //证书盐
    private String useSealerCertAlias;
    //证书密码
    private String useSealerCertPwd;
    //待签署ofd路径
    private String ofdReqPath;
    //签署输出路径
    private String ofdOutPaht;
    //0或者null表示非保护模式,1表示保护模式
    private Integer signMode;

    private List<Pos> apList;


    public BuildOFDSigner(){

    }


    public String getEslPath() {
        return eslPath;
    }

    public  BuildOFDSigner setEslPath(String eslPath) {
        this.eslPath = eslPath;
        return this;
    }

    public String getUseSealerCertPath() {
        return useSealerCertPath;
    }

    public  BuildOFDSigner setUseSealerCertPath(String useSealerCertPath) {
        this.useSealerCertPath = useSealerCertPath;
        return this;
    }

    public String getUseSealerCertAlias() {
        return useSealerCertAlias;
    }

    public  BuildOFDSigner setUseSealerCertAlias(String useSealerCertAlias) {
        this.useSealerCertAlias = useSealerCertAlias;
        return this;
    }

    public String getUseSealerCertPwd() {
        return useSealerCertPwd;
    }

    public  BuildOFDSigner setUseSealerCertPwd(String useSealerCertPwd) {
        this.useSealerCertPwd = useSealerCertPwd;return this;
    }

    public String getOfdReqPath() {
        return ofdReqPath;
    }

    public  BuildOFDSigner setOfdReqPath(String ofdReqPath) {
        this.ofdReqPath = ofdReqPath;return this;
    }

    public String getOfdOutPaht() {
        return ofdOutPaht;
    }

    public  BuildOFDSigner setOfdOutPaht(String ofdOutPaht) {
        this.ofdOutPaht = ofdOutPaht;return this;
    }

    public Integer getSignMode() {
        return signMode;
    }

    public  BuildOFDSigner setSignMode(Integer signMode) {
        this.signMode = signMode;return this;
    }

    public List<Pos> getApList() {
        return apList;
    }

    public BuildOFDSigner setApList(List<Pos> apList) {
        this.apList = apList;
        return this;
    }

    public BuildOFDSigner addApList(Pos pos) {
        if (pos == null) {
            return this;
        } else {
            if(this.apList==null){
                this.apList= new LinkedList<>();
            }
            this.apList.add(pos);
            return this;
        }
    }
}
