package com.demo.sign;

/**
 * OFD签署数据
 */
public class BuildOFDSignerKeyBase64 {

    //印章
    private String eslBase64;
    //证书路径
    private String useSealerCertBase64;
    //待签署ofd路径
    private String ofdReqBase64;
    //0或者null表示非保护模式,1表示保护模式
    private Integer signMode;

    private String [] key;

    private Integer width;
    private Integer height;

    private Integer offsetX;
    private Integer offsetY;


    public BuildOFDSignerKeyBase64(){

    }

    public String getEslBase64() {
        return eslBase64;
    }

    public BuildOFDSignerKeyBase64 setEslBase64(String eslBase64) {
        this.eslBase64 = eslBase64;return this;
    }

    public String getUseSealerCertBase64() {
        return useSealerCertBase64;
    }

    public BuildOFDSignerKeyBase64 setUseSealerCertBase64(String useSealerCertBase64) {
        this.useSealerCertBase64 = useSealerCertBase64;return this;
    }


    public String getOfdReqBase64() {
        return ofdReqBase64;
    }

    public BuildOFDSignerKeyBase64 setOfdReqBase64(String ofdReqBase64) {
        this.ofdReqBase64 = ofdReqBase64;return this;
    }


    public Integer getSignMode() {
        return signMode;
    }

    public BuildOFDSignerKeyBase64 setSignMode(Integer signMode) {
        this.signMode = signMode;return this;
    }

    public String[] getKey() {
        return key;
    }

    public BuildOFDSignerKeyBase64 setKey(String[] key) {
        this.key = key;return this;
    }

    public Integer getWidth() {
        return width;
    }

    public BuildOFDSignerKeyBase64 setWidth(Integer width) {
        this.width = width;return this;
    }

    public Integer getHeight() {
        return height;
    }

    public BuildOFDSignerKeyBase64 setHeight(Integer height) {
        this.height = height;return this;
    }

    public Integer getOffsetX() {
        if(offsetX==null){
            offsetX = 0;
        }
        return offsetX;
    }

    public BuildOFDSignerKeyBase64 setOffsetX(Integer offsetX) {
        this.offsetX = offsetX;return this;
    }

    public Integer getOffsetY() {
        if(offsetY==null){
            offsetY = 0;
        }
        return offsetY;
    }

    public BuildOFDSignerKeyBase64 setOffsetY(Integer offsetY) {
        this.offsetY = offsetY;return this;
    }
}
