package com.demo.seal;

import lombok.Data;

import java.math.BigInteger;
import java.util.Date;

@Data
public class CertInfo {
    private BigInteger serialNumber ;
    private Date notBefore;
    private Date notAfter;
}
