package com.demo.seal;

import org.ofdrw.converter.utils.StringUtils;

import java.util.Date;
import java.util.UUID;

public class BuildSESeal {

    //印章唯一ID
    private String esID;

    private String buildSealerCertPath;
    private String buildSealerCertAlias;
    private String buildSealerCertPwd;

    private String useSealerCertPath;
    private String useSealerCertAlias;
    private String useSealerCertPwd;

    private String sealImagePath;

    private String sesheader;
    //公章建议使用企业名称；个人章建议使用个人姓名
    private String sealName;

    private Integer width;
    private Integer height;

    //此处地址使用相对路径 建议路径使用值=相对路径+esId+.esl
    private String outEslPath;

    //此处时间为印章有效期开始时间 建议使用印章使用者证书开始时间
    private Date validStartDate;
    //此处时间为印章有效期结束时间 建议使用印章使用者证书结束时间
    private Date validEndDate;

    public BuildSESeal(){

    }


    public String getEsID() {
        if(StringUtils.isBlank(esID)){
           return UUID.randomUUID().toString().replace("-", "");
        }
        return esID;
    }

    public  BuildSESeal setEsID(String esID) {
        this.esID = esID;
        return this;
    }

    public String getBuildSealerCertPath() {
        return buildSealerCertPath;
    }

    public  BuildSESeal setBuildSealerCertPath(String buildSealerCertPath) {
        this.buildSealerCertPath = buildSealerCertPath;
        return this;
    }

    public String getBuildSealerCertAlias() {
        return buildSealerCertAlias;
    }

    public  BuildSESeal setBuildSealerCertAlias(String buildSealerCertAlias) {
        this.buildSealerCertAlias = buildSealerCertAlias;
        return this;
    }

    public String getBuildSealerCertPwd() {
        return buildSealerCertPwd;
    }

    public  BuildSESeal setBuildSealerCertPwd(String buildSealerCertPwd) {
        this.buildSealerCertPwd = buildSealerCertPwd;
        return this;
    }

    public String getUseSealerCertPath() {
        return useSealerCertPath;
    }

    public  BuildSESeal setUseSealerCertPath(String useSealerCertPath) {
        this.useSealerCertPath = useSealerCertPath;
        return this;
    }

    public String getUseSealerCertAlias() {
        return useSealerCertAlias;
    }

    public  BuildSESeal setUseSealerCertAlias(String useSealerCertAlias) {
        this.useSealerCertAlias = useSealerCertAlias;
        return this;
    }

    public String getUseSealerCertPwd() {
        return useSealerCertPwd;
    }

    public  BuildSESeal setUseSealerCertPwd(String useSealerCertPwd) {
        this.useSealerCertPwd = useSealerCertPwd;
        return this;
    }

    public String getSealImagePath() {
        return sealImagePath;
    }

    public  BuildSESeal setSealImagePath(String sealImagePath) {
        this.sealImagePath = sealImagePath;
        return this;
    }

    public String getSesheader() {
        return sesheader;
    }

    public  BuildSESeal setSesheader(String sesheader) {
        this.sesheader = sesheader;
        return this;
    }

    public String getSealName() {
        return sealName;
    }

    public  BuildSESeal setSealName(String sealName) {
        this.sealName = sealName;
        return this;
    }

    public Integer getWidth() {
        return width;
    }

    public  BuildSESeal setWidth(Integer width) {
        this.width = width;
        return this;
    }

    public Integer getHeight() {
        return height;
    }

    public  BuildSESeal setHeight(Integer height) {
        this.height = height;
        return this;
    }

    public String getOutEslPath() {
        return outEslPath;
    }

    public  BuildSESeal setOutEslPath(String outEslPath) {
        this.outEslPath = outEslPath;
        return this;
    }

    public Date getValidStartDate() {
        return validStartDate;
    }

    public BuildSESeal setValidStartDate(Date validStartDate) {
        this.validStartDate = validStartDate;
        return this;
    }

    public Date getValidEndDate() {
        return validEndDate;
    }

    public BuildSESeal setValidEndDate(Date validEndDate) {
        this.validEndDate = validEndDate;
        return this;
    }

}
