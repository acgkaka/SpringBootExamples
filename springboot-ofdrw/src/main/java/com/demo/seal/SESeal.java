package com.demo.seal;

import com.demo.ca.CASignInterface;
import com.demo.util.JavaObjUtil;
import com.demo.util.NativePKCS12Tools;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.jcajce.provider.asymmetric.x509.CertificateFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.ofdrw.gm.cert.PKCS12Tools;
import org.ofdrw.gm.ses.v1.SES_ESPictrueInfo;
import org.ofdrw.gm.ses.v1.SES_Header;
import org.ofdrw.gm.ses.v4.SES_CertList;
import org.ofdrw.gm.ses.v4.SES_ESPropertyInfo;
import org.ofdrw.gm.ses.v4.SES_SealInfo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * @Description ESL组件
 * @Date 2022/10/21
 */
@Slf4j
public class SESeal {

     
    static void  signVerify(String certPath, String alias, String pwd) throws GeneralSecurityException, IOException {
        Path sealerPath = Paths.get("src/test/resources", "sm2.pfx");
        Security.addProvider(new BouncyCastleProvider());
        PrivateKey sealerPrvKey = PKCS12Tools.ReadPrvKey(sealerPath, "123456", "111111");
        Signature sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
        sg.initSign(sealerPrvKey);
        sg.update(new byte[32]);
        byte[] sigVal = sg.sign();
        System.out.println(sigVal.length);

        Certificate certificate = PKCS12Tools.ReadUserCert(sealerPath, "123456", "111111");

        sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
        sg.initVerify(certificate);
        sg.update(new byte[32]);
        System.out.println(sg.verify(sigVal));
    }

     
    public static void build(BuildSESeal buildSESeal) throws GeneralSecurityException, IOException {

        log.info("prepare for build seseal...");
        List<String> strings = JavaObjUtil.checkObjAllFieldsIsNull(buildSESeal,"esID");
        if(strings!=null && strings.size()>0){
            throw new RuntimeException(strings.toString()+"存在为空");
        }
        log.info("buildSESeal={}",buildSESeal.toString());

        Security.addProvider(new BouncyCastleProvider());
        Path sealerPath = Paths.get(buildSESeal.getBuildSealerCertPath());
        Path userPath = Paths.get(buildSESeal.getUseSealerCertPath());

        //证书校验耗时比较长，以后处理
        checkCertDate(userPath,buildSESeal.getUseSealerCertAlias(),buildSESeal.getUseSealerCertPwd());
        checkCertDate(sealerPath,buildSESeal.getBuildSealerCertAlias(),buildSESeal.getBuildSealerCertPwd());

        Path picturePath = Paths.get(buildSESeal.getSealImagePath());

        Certificate sealerCert = PKCS12Tools.ReadUserCert(sealerPath, buildSESeal.getBuildSealerCertAlias(), buildSESeal.getBuildSealerCertPwd());
        Certificate userCert = PKCS12Tools.ReadUserCert(userPath, buildSESeal.getUseSealerCertAlias(), buildSESeal.getUseSealerCertPwd());

        ASN1EncodableVector v = new ASN1EncodableVector(1);
        v.add(new DEROctetString(userCert.getEncoded()));

        SES_Header header = new SES_Header(SES_Header.V4, new DERIA5String(buildSESeal.getSesheader()));
        Calendar now = Calendar.getInstance();
        now.add(Calendar.YEAR, 2);
        Date then = now.getTime();
        SES_ESPropertyInfo propertyInfo = new SES_ESPropertyInfo()
                .setType(new ASN1Integer(3))
                .setName(new DERUTF8String(buildSESeal.getSealName()))
                .setCertListType(SES_ESPropertyInfo.CertListType)
                .setCertList(SES_CertList.getInstance(SES_ESPropertyInfo.CertListType, new DERSequence(v)))
                .setCreateDate(new ASN1GeneralizedTime(new Date()))
                .setValidStart(new ASN1GeneralizedTime(buildSESeal.getValidStartDate()))
                .setValidEnd(new ASN1GeneralizedTime(buildSESeal.getValidEndDate()));

        SES_ESPictrueInfo pictrueInfo = new SES_ESPictrueInfo()
                .setType("PNG")
                .setData(Files.readAllBytes(picturePath))
                .setWidth(buildSESeal.getWidth())
                .setHeight(buildSESeal.getHeight());

        SES_SealInfo sesSealInfo = new SES_SealInfo()
                .setHeader(header)
                .setEsID(new DERIA5String(buildSESeal.getEsID()))
                .setProperty(propertyInfo)
                .setPicture(pictrueInfo);

        PrivateKey sealerPrvKey = PKCS12Tools.ReadPrvKey(sealerPath, buildSESeal.getUseSealerCertAlias(), buildSESeal.getUseSealerCertPwd());
        Signature sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
        sg.initSign(sealerPrvKey);
        sg.update(sesSealInfo.getEncoded("DER"));
        byte[] sigVal = sg.sign();

        org.ofdrw.gm.ses.v4.SESeal seal = new org.ofdrw.gm.ses.v4.SESeal()
                .seteSealInfo(sesSealInfo)
                .setCert(sealerCert)
                .setSignAlgID(GMObjectIdentifiers.sm2sign_with_sm3)
                .setSignedValue(sigVal);

        Path out = Paths.get(buildSESeal.getOutEslPath());
        Files.write(out, seal.getEncoded("DER"));
        System.out.println(">> V4版本印章存储于: " + out.toAbsolutePath().toAbsolutePath());

    }
    public static Map<String,Object> buildBase64(BuildSESealBase64 buildSESeal, CASignInterface CASignInterface) throws GeneralSecurityException, IOException {

        log.info("prepare for build seseal...");
        List<String> strings = JavaObjUtil.checkObjAllFieldsIsNull(buildSESeal,"esID","validStartDate","validEndDate");
        if(strings!=null && strings.size()>0){
            throw new RuntimeException(strings.toString()+"存在为空");
        }
        Security.addProvider(new BouncyCastleProvider());
        byte[] pictureData = Base64.decodeBase64(buildSESeal.getSealImageBase64());
        System.out.println("图片长度="+pictureData.length);
        //证书校验耗时比较长，以后处理
        checkCertDate(buildSESeal.getBuildSealerCertBase64());
        CertInfo certInfo = checkCertDate(buildSESeal.getUseSealerCertBase64());

        Certificate sealerCert = NativePKCS12Tools.readX509Certificate(buildSESeal.getBuildSealerCertBase64());
        Certificate userCert = NativePKCS12Tools.readX509Certificate(buildSESeal.getUseSealerCertBase64());

        ASN1EncodableVector v = new ASN1EncodableVector(1);
        v.add(new DEROctetString(userCert.getEncoded()));

        SES_Header header = new SES_Header(SES_Header.V4, new DERIA5String(buildSESeal.getSesheader()));
//        Calendar now = Calendar.getInstance();
//        now.add(Calendar.YEAR, 2);
//        Date then = now.getTime();
        SES_ESPropertyInfo propertyInfo = new SES_ESPropertyInfo()
                .setType(new ASN1Integer(buildSESeal.getSealType()))
                .setName(new DERUTF8String(buildSESeal.getSealName()))
                .setCertListType(SES_ESPropertyInfo.CertListType)
                .setCertList(SES_CertList.getInstance(SES_ESPropertyInfo.CertListType, new DERSequence(v)))
                .setCreateDate(new ASN1GeneralizedTime(new Date()))
                .setValidStart(new ASN1GeneralizedTime(certInfo.getNotBefore()))
                .setValidEnd(new ASN1GeneralizedTime(certInfo.getNotAfter()));

        SES_ESPictrueInfo pictrueInfo = new SES_ESPictrueInfo()
                .setType("PNG")
                .setData(pictureData)
                .setWidth(buildSESeal.getWidth())
                .setHeight(buildSESeal.getHeight());

        SES_SealInfo sesSealInfo = new SES_SealInfo()
                .setHeader(header)
                .setEsID(new DERIA5String(buildSESeal.getEsID()))
                .setProperty(propertyInfo)
                .setPicture(pictrueInfo);
        //调用签署
        byte[] sigVal = CASignInterface.sign(sesSealInfo.getEncoded("DER"));
        org.ofdrw.gm.ses.v4.SESeal seal = new org.ofdrw.gm.ses.v4.SESeal()
                .seteSealInfo(sesSealInfo)
                .setCert(sealerCert)
                .setSignAlgID(GMObjectIdentifiers.sm2sign_with_sm3)
                .setSignedValue(sigVal);
        byte[] ders = seal.getEncoded("DER");
        String eslBase64 = Base64.encodeBase64String(ders);
        System.out.println("印章制作成功，正在转base64返回");
        Map<String,Object> map = new HashMap<>();
        map.put("esID",buildSESeal.getEsID());
        map.put("base64",eslBase64);
        return map;
    }

    public static void checkCertDate(Path certPath, String certAlias, String certPwd) throws GeneralSecurityException, IOException {
        Security.addProvider(new BouncyCastleProvider());
        log.info("prepare check certpath={}",certPath.toAbsolutePath().toAbsolutePath());
        X509Certificate certificate = (X509Certificate) PKCS12Tools.ReadUserCert(certPath,certAlias,certPwd);
        System.out.println("pubkey="+Base64.encodeBase64String(certificate.getPublicKey().getEncoded()));
        Date notBefore = certificate.getNotBefore();
        Date notAfter = certificate.getNotAfter();
        log.info("cert notBefore={}",notBefore);
        log.info("cert notAfter={}",notAfter);
        validateCertEndTime(notAfter);
    }

    //cer
    private static CertInfo checkCertDate( String certBase64 ) throws GeneralSecurityException, IOException {
        InputStream inputStream = null;
        try {
            inputStream = new ByteArrayInputStream(Base64.decodeBase64(certBase64));
//            X509Certificate certificate = (X509Certificate) PKCS12Tools.ReadUserCert(inputStream,certAlias,certPwd);
            X509Certificate certificate = NativePKCS12Tools.readX509Certificate(certBase64);
            Date notBefore = certificate.getNotBefore();
            Date notAfter = certificate.getNotAfter();
            BigInteger serialNumber = certificate.getSerialNumber();
            log.info("cert notBefore={}",notBefore);
            log.info("cert notAfter={}",notAfter);
            CertInfo certInfo = new CertInfo();
            certInfo.setNotAfter(notAfter);
            certInfo.setNotBefore(notBefore);
            certInfo.setSerialNumber(serialNumber);
//            validateCertEndTime(notAfter);
            return certInfo;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(inputStream!=null){
                inputStream.close();
            }
        }
        return null;

    }

    //pfx
    private static CertInfo checkCertDate( String certBase64 ,String certAlias,String certPwd) throws GeneralSecurityException, IOException {
        InputStream inputStream = null;
        try {
            inputStream = new ByteArrayInputStream(Base64.decodeBase64(certBase64));
            X509Certificate certificate = (X509Certificate) PKCS12Tools.ReadUserCert(inputStream,certAlias,certPwd);
//            X509Certificate certificate = NativePKCS12Tools.readX509Certificate(certBase64);
            Date notBefore = certificate.getNotBefore();
            Date notAfter = certificate.getNotAfter();
            BigInteger serialNumber = certificate.getSerialNumber();
            log.info("cert notBefore={}",notBefore);
            log.info("cert notAfter={}",notAfter);
            CertInfo certInfo = new CertInfo();
            certInfo.setNotAfter(notAfter);
            certInfo.setNotBefore(notBefore);
            certInfo.setSerialNumber(serialNumber);
            validateCertEndTime(notAfter);
            return certInfo;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(inputStream!=null){
                inputStream.close();
            }
        }
        return null;

    }

    private static void validateCertEndTime(Date certEnddate) {
        long certEndtime = certEnddate.getTime();
        long currentTimeMillis = System.currentTimeMillis();
        log.info("证书到期日期【{}】", certEndtime);
        log.info("当前日期【{}】", currentTimeMillis);
        if (currentTimeMillis > certEndtime) {
            throw new RuntimeException("证书已经过期,请联系管理员重新申请证书");
        }
        log.info("证书验证通过");
    }


    public void verify() throws IOException, NoSuchAlgorithmException, CertificateException, InvalidKeyException, SignatureException {
//        Path path = Paths.get("src/test/resources", "UserV4.esl");
//        Path path = Paths.get("target", "UserV4.esl");
        Path path = Paths.get("target", "sm2-signature.esl");
        org.ofdrw.gm.ses.v4.SESeal seal = org.ofdrw.gm.ses.v4.SESeal.getInstance(Files.readAllBytes(path));

        ASN1OctetString cert = seal.getCert();
        CertificateFactory factory = new CertificateFactory();
        X509Certificate certificate = (X509Certificate) factory.engineGenerateCertificate(cert.getOctetStream());

        SES_SealInfo ses_sealInfo = seal.geteSealInfo();

        Signature sg = Signature.getInstance(seal.getSignAlgID().toString()
                , new BouncyCastleProvider());
        sg.initVerify(certificate);
        sg.update(ses_sealInfo.getEncoded());
        byte[] sigVal = seal.getSignedValue().getBytes();

        System.out.println(sg.verify(sigVal));
    }

}