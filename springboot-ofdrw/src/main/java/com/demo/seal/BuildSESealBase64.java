package com.demo.seal;

import org.ofdrw.converter.utils.StringUtils;

import java.util.Date;
import java.util.UUID;

public class BuildSESealBase64 {

    //印章唯一ID
    private String esID;

    private String buildSealerCertBase64;
//    private String buildSealerCertAlias;
//    private String buildSealerCertPwd;

    private String useSealerCertBase64;
//    private String useSealerCertAlias;
//    private String useSealerCertPwd;

    private String sealImageBase64;

    private String sesheader;
    //公章建议使用企业名称；个人章建议使用个人姓名
    private String sealName;

    private Integer width;
    private Integer height;

    //此处时间为印章有效期开始时间 建议使用印章使用者证书开始时间
    private Date validStartDate;
    //此处时间为印章有效期结束时间 建议使用印章使用者证书结束时间
    private Date validEndDate;

    private int sealType;



    public BuildSESealBase64(){

    }

    public int getSealType() {
        return sealType;
    }

    public BuildSESealBase64 setSealType(int sealType) {
        this.sealType = sealType;
        return this;
    }

    public String getBuildSealerCertBase64() {
        return buildSealerCertBase64;
    }

    public BuildSESealBase64 setBuildSealerCertBase64(String buildSealerCertBase64) {
        this.buildSealerCertBase64 = buildSealerCertBase64;return this;
    }

    public String getUseSealerCertBase64() {
        return useSealerCertBase64;
    }

    public BuildSESealBase64 setUseSealerCertBase64(String useSealerCertBase64) {
        this.useSealerCertBase64 = useSealerCertBase64;return this;
    }


    public String getSealImageBase64() {
        return sealImageBase64;
    }

    public BuildSESealBase64 setSealImageBase64(String sealImageBase64) {
        this.sealImageBase64 = sealImageBase64;return this;
    }


    public String getEsID() {
        if(StringUtils.isBlank(esID)){
            String esID = UUID.randomUUID().toString().replace("-", "");
            System.out.println("系统自动生成ESID="+esID);
           return esID;
        }
        return esID;
    }

    public BuildSESealBase64 setEsID(String esID) {
        this.esID = esID;
        return this;
    }


//
//    public String getBuildSealerCertAlias() {
//        return buildSealerCertAlias;
//    }
//
//    public BuildSESealBase64 setBuildSealerCertAlias(String buildSealerCertAlias) {
//        this.buildSealerCertAlias = buildSealerCertAlias;
//        return this;
//    }
//
//    public String getBuildSealerCertPwd() {
//        return buildSealerCertPwd;
//    }
//
//    public BuildSESealBase64 setBuildSealerCertPwd(String buildSealerCertPwd) {
//        this.buildSealerCertPwd = buildSealerCertPwd;
//        return this;
//    }



    public String getSesheader() {
        return sesheader;
    }

    public BuildSESealBase64 setSesheader(String sesheader) {
        this.sesheader = sesheader;
        return this;
    }

    public String getSealName() {
        return sealName;
    }

    public BuildSESealBase64 setSealName(String sealName) {
        this.sealName = sealName;
        return this;
    }

    public Integer getWidth() {
        return width;
    }

    public BuildSESealBase64 setWidth(Integer width) {
        this.width = width;
        return this;
    }

    public Integer getHeight() {
        return height;
    }

    public BuildSESealBase64 setHeight(Integer height) {
        this.height = height;
        return this;
    }


    public Date getValidStartDate() {
        return validStartDate;
    }

    public BuildSESealBase64 setValidStartDate(Date validStartDate) {
        this.validStartDate = validStartDate;
        return this;
    }

    public Date getValidEndDate() {
        return validEndDate;
    }

    public BuildSESealBase64 setValidEndDate(Date validEndDate) {
        this.validEndDate = validEndDate;
        return this;
    }

}
