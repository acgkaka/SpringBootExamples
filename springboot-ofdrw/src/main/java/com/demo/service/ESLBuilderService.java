package com.demo.service;

import java.util.Date;

public interface ESLBuilderService {

    /**
     * 生成ESL文件
     */
    String createESLFile(String imageBase64, String certBase64, Integer height, Integer width, Date startTime, Date endTime);

}
