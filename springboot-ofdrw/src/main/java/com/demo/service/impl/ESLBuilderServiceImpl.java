package com.demo.service.impl;

import com.demo.ca.CASignInterface;
import com.demo.seal.SESeal;
import com.demo.seal.BuildSESealBase64;
import com.demo.service.ESLBuilderService;
import com.demo.util.NativePKCS12Tools;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
public class ESLBuilderServiceImpl implements ESLBuilderService {

    @Override
    public String createESLFile(String imageBase64, String certBase64, Integer height, Integer width, Date startTime, Date endTime) {

        long start = System.currentTimeMillis();
        BuildSESealBase64 seal = new BuildSESealBase64().setEsID(UUID.randomUUID().toString().replace("-", ""))
                .setSealImageBase64(imageBase64)//印章图片base64
                .setSealName("ACGkaka测试章")//印章名称
                .setBuildSealerCertBase64(certBase64)//制章人公钥
                .setHeight(height)//印章高度单位毫米
                .setWidth(width)//印章宽度单位毫米
                .setSesheader("sign")//印章头部信息
                .setUseSealerCertBase64(certBase64)//用章人公钥
                .setValidStartDate(startTime)//印章有效期 传空,固定读用章人证书有效
                .setValidEndDate(endTime);//印章有效期传空,固定读用章人证书有效
        try {
            final String certFileBase64 = certBase64;
            Map<String, Object> stringObjectMap = SESeal.buildBase64(seal, new CASignInterface() {//制章人私钥签名方法(需要调用CA密码机服务)
                @Override
                public byte[] sign(byte[] data) {
                    PrivateKey sealerPrvKey = null;
                    try {
                        sealerPrvKey = NativePKCS12Tools.ReadPrvKey(certBase64, "ACGkaka", "123456");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Signature sg = null;
                    try {
                        sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.initSign(sealerPrvKey);
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    }
                    try {
                        sg.update(data);
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    byte[] sigVal = new byte[0];
                    try {
                        sigVal = sg.sign();
                    } catch (SignatureException e) {
                        e.printStackTrace();
                    }
                    log.info("长度" + sigVal.length);
                    log.info("开始制章人验签");
                    X509Certificate certificate = null;
                    try {
                        certificate = NativePKCS12Tools.readX509Certificate(certFileBase64);
                        Date notAfter = certificate.getNotAfter();
                        Date notBefore = certificate.getNotBefore();
                        log.info("notAfter：{}", notAfter.toString());
                        log.info("notBefore：{}", notBefore.toString());

                        sg = Signature.getInstance("SM3WithSM2", new BouncyCastleProvider());
                        sg.initVerify(certificate);
                        sg.update(data);
                    } catch (GeneralSecurityException e) {
                        e.printStackTrace();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    return sigVal;
                }
            });

            return stringObjectMap.get("base64").toString();

        } catch (Exception e) {
            log.error(">>>>>>>>>>【ERROR】印章文件制作失败，请稍后重试，原因：{}", e.getMessage(), e);
            throw new IllegalArgumentException("印章文件制作失败，请稍后重试");
        } finally {
            long end = System.currentTimeMillis();
            log.info("执行耗时间=" + (end - start) / 1000);
        }
    }
}
