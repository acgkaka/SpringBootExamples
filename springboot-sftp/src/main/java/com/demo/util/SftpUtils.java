package com.demo.util;

import com.demo.config.SftpBaseProperties;
import com.jcraft.jsch.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;

/**
 * <p> @Title SftpUtils
 * <p> @Description SFTP工具类
 *
 * @author ACGkaka
 * @date 2024/1/31 17:41
 */
@Slf4j
public class SftpUtils {

    /**
     * 上传文件
     *
     * @param sftpPath
     * @param file
     * @param sftpBaseProperties
     */
    public static void upload(String sftpPath, MultipartFile file, SftpBaseProperties sftpBaseProperties) {
        // 上传文件
        ChannelSftp sftp = null;
        try (InputStream in = file.getInputStream()) {
            // 开启sftp连接
            sftp = createSftp(sftpBaseProperties);

            // 进入sftp文件目录
            sftp.cd(sftpPath);
            log.info("修改目录为：{}", sftpPath);

            // 上传文件
            sftp.put(in, file.getOriginalFilename());
            log.info("上传文件成功，目标目录：{}", sftpPath);
        } catch (SftpException | JSchException | IOException e) {
            log.error("上传文件失败，原因：{}", e.getMessage(), e);
            throw new RuntimeException("上传文件失败");
        } finally {
            // 关闭sftp
            disconnect(sftp);
        }
    }

    /**
     * 下载文件
     * @param sftpPath
     * @param response
     * @param sftpBaseProperties
     */
    public static void download(String sftpPath, HttpServletResponse response, SftpBaseProperties sftpBaseProperties) {
        long start = System.currentTimeMillis();

        ChannelSftp sftp = null;
        try {
            // 开启sftp连接
            sftp = createSftp(sftpBaseProperties);

            // 判断sftp文件存在
            File sftpFile = new File(sftpPath);
            boolean isExist = isFileExist(sftpPath, sftp);
            if (isExist) {
                // 下载文件
                response.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE);
                response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(sftpFile.getName(), "utf-8"));
                sftp.get(sftpFile.getName(), response.getOutputStream());

                // 记录日志
                long time = System.currentTimeMillis() - start;
                log.info("sftp文件下载成功，目标文件：{}，总耗时：{}ms.", sftpPath, time);
            } else {
                log.error("sftp文件下载失败，sftp文件不存在：" + sftpFile.getParent());
                throw new RuntimeException("sftp文件下载失败，sftp文件不存在：" + sftpFile.getParent());
            }
        } catch (SftpException | JSchException | IOException e) {
            log.error("sftp文件下载失败，目标文件名：{}，原因：{}", sftpPath, e.getMessage(), e);
            throw new RuntimeException("sftp文件下载失败");
        } finally {
            // 关闭sftp
            disconnect(sftp);
        }
    }

    /**
     * 重命名
     * @param oldPath
     * @param newPath
     * @param sftpBaseProperties
     */
    public static void rename(String oldPath, String newPath, SftpBaseProperties sftpBaseProperties) {
        ChannelSftp sftp = null;
        try {
            // 开启sftp连接
            sftp = createSftp(sftpBaseProperties);

            // 修改sftp文件路径
            sftp.rename(oldPath, newPath);
            log.info("sftp文件重命名成功，历史路径：{}，新路径：{}", oldPath, newPath);
        } catch (SftpException | JSchException e) {
            log.error("sftp文件重命名失败，原因：{}", e.getMessage(), e);
            throw new RuntimeException("sftp文件重命名失败");
        } finally {
            // 关闭sftp
            disconnect(sftp);
        }
    }

    /**
     * 删除文件
     * @param sftpPath
     * @param sftpBaseProperties
     */
    public static void delete(String sftpPath, SftpBaseProperties sftpBaseProperties) {
        ChannelSftp sftp = null;
        try {
            // 开启sftp连接
            sftp = createSftp(sftpBaseProperties);

            // 判断sftp文件存在
            boolean isExist = isFileExist(sftpPath, sftp);
            if (isExist) {
                // 删除文件
                SftpATTRS sftpATTRS = sftp.lstat(sftpPath);
                if (sftpATTRS.isDir()) {
                    sftp.rmdir(sftpPath);
                } else {
                    sftp.rm(sftpPath);
                }
                log.info("sftp文件删除成功，目标文件：{}.", sftpPath);
            } else {
                log.error("sftp文件删除失败，sftp文件不存在：" + sftpPath);
                throw new RuntimeException("sftp文件删除失败，sftp文件不存在：" + sftpPath);
            }
        } catch (SftpException | JSchException e) {
            log.error("sftp文件删除失败，原因：{}", e.getMessage(), e);
            throw new RuntimeException("sftp文件删除失败");
        } finally {
            // 关闭sftp
            disconnect(sftp);
        }
    }


    // ------------------------------------------------------------------------------------------
    // 私有方法
    // ------------------------------------------------------------------------------------------


    /**
     * 创建SFTP连接
     */
    private static ChannelSftp createSftp(SftpBaseProperties sftpBaseProperties) throws JSchException {
        JSch jsch = new JSch();
        log.info("Try to connect sftp[" + sftpBaseProperties.getUsername() + "@" + sftpBaseProperties.getHost() + "]");

        Session session = createSession(jsch, sftpBaseProperties.getHost(), sftpBaseProperties.getUsername(), sftpBaseProperties.getPort());
        session.setPassword(sftpBaseProperties.getPassword());
        session.setConfig("StrictHostKeyChecking", "no");
        // 默认情况下，JSch库本身并没有会话超时时间。
        // 为了避免长时间无活动连接占用资源或因网络问题导致连接挂起而不被释放，通常建议设置会话超时，（单位：毫秒）
        session.setTimeout(30000);
        session.connect();

        log.info("Session connected to {}.", sftpBaseProperties.getHost());

        Channel channel = session.openChannel(sftpBaseProperties.getProtocol());
        channel.connect();

        log.info("Channel created to {}.", sftpBaseProperties.getHost());

        return (ChannelSftp) channel;
    }

    /**
     * 创建 Session
     */
    private static Session createSession(JSch jsch, String host, String username, Integer port) throws JSchException {
        Session session = null;

        if (port <= 0) {
            session = jsch.getSession(username, host);
        } else {
            session = jsch.getSession(username, host, port);
        }

        if (session == null) {
            throw new RuntimeException(host + "session is null");
        }

        return session;
    }

    /**
     * 关闭连接
     */
    private static void disconnect(ChannelSftp sftp) {
        try {
            if (sftp != null) {
                if (sftp.isConnected()) {
                    sftp.disconnect();
                } else if (sftp.isClosed()) {
                    log.error("sftp 连接已关闭");
                }
                if (sftp.getSession() != null) {
                    sftp.getSession().disconnect();
                }
            }
        } catch (JSchException e) {
            log.error("sftp 断开连接失败，原因：{}", e.getMessage(), e);
        }
    }

    /**
     * 判断目录是否存在
     */
    private static boolean isFileExist(String sftpPath, ChannelSftp sftp) {
        try {
            // 获取文件信息
            SftpATTRS sftpATTRS = sftp.lstat(sftpPath);
            return sftpATTRS != null;
        } catch (Exception e) {
            log.error("判断文件是否存在失败，原因：{}", e.getMessage(), e);
            return false;
        }
    }
}
