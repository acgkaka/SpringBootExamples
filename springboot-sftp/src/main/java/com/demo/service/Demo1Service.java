package com.demo.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * <p> @Title DemoService
 * <p> @Description 测试Service
 *
 * @author ACGkaka
 * @date 2023/4/24 18:13
 */
public interface Demo1Service {

    /**
     * 上传文件
     * @param sftpPath 路径
     * @param file 文件
     */
    void upload(String sftpPath, MultipartFile file);

    /**
     * 下载文件
     * @param sftpPath
     * @param response
     */
    void download(String sftpPath, HttpServletResponse response);

    /**
     * 重命名文件（移动）
     * @param oldPath
     * @param newPath
     */
    void rename(String oldPath, String newPath);

    /**
     * 删除文件
     * @param sftpPath
     */
    void delete(String sftpPath);
}
