package com.demo.service.impl;

import com.demo.config.SftpDemo2Properties;
import com.demo.service.Demo2Service;
import com.demo.util.SftpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * <p> @Title DemoServiceImpl
 * <p> @Description 测试ServiceImpl
 *
 * @author ACGkaka
 * @date 2023/4/24 18:14
 */
@Slf4j
@Service
public class Demo2ServiceImpl implements Demo2Service {

    @Resource
    private SftpDemo2Properties sftpDemo2Properties;

    @Override
    public void upload(String sftpPath, MultipartFile file) {
        // 上传文件
        SftpUtils.upload(sftpPath, file, sftpDemo2Properties);
    }

    @Override
    public void download(String sftpPath, HttpServletResponse response) {
        // 下载文件
        SftpUtils.download(sftpPath, response, sftpDemo2Properties);
    }

    @Override
    public void rename(String oldPath, String newPath) {
        // 重命名文件（移动）
        SftpUtils.rename(oldPath, newPath, sftpDemo2Properties);
    }

    @Override
    public void delete(String sftpPath) {
        // 删除文件
        SftpUtils.delete(sftpPath, sftpDemo2Properties);
    }

}
