package com.demo.service.impl;

import com.demo.config.SftpDemo1Properties;
import com.demo.service.Demo1Service;
import com.demo.util.SftpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * <p> @Title DemoServiceImpl
 * <p> @Description 测试ServiceImpl
 *
 * @author ACGkaka
 * @date 2023/4/24 18:14
 */
@Slf4j
@Service
public class Demo1ServiceImpl implements Demo1Service {

    @Resource
    private SftpDemo1Properties sftpDemo1Properties;

    @Override
    public void upload(String sftpPath, MultipartFile file) {
        // 上传文件
        SftpUtils.upload(sftpPath, file, sftpDemo1Properties);
    }

    @Override
    public void download(String sftpPath, HttpServletResponse response) {
        // 下载文件
        SftpUtils.download(sftpPath, response, sftpDemo1Properties);
    }

    @Override
    public void rename(String oldPath, String newPath) {
        // 重命名文件（移动）
        SftpUtils.rename(oldPath, newPath, sftpDemo1Properties);
    }

    @Override
    public void delete(String sftpPath) {
        // 删除文件
        SftpUtils.delete(sftpPath, sftpDemo1Properties);
    }

}
