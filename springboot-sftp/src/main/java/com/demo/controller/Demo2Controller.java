package com.demo.controller;

import com.demo.common.Result;
import com.demo.service.Demo2Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * <p> @Title DemoController
 * <p> @Description 测试Controller
 *
 * @author ACGkaka
 * @date 2023/4/24 18:02
 */
@Slf4j
@RestController
@RequestMapping("/demo2")
public class Demo2Controller {

    @Resource
    private Demo2Service demo2Service;

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    public Result<Object> upload(@RequestParam String sftpPath, @RequestParam MultipartFile file) {
        demo2Service.upload(sftpPath, file);
        return Result.succeed();
    }

    /**
     * 下载文件
     */
    @GetMapping("/download")
    public void download(@RequestParam String sftpPath,  HttpServletResponse response) {
        demo2Service.download(sftpPath, response);
    }

    /**
     * 重命名文件（移动）
     */
    @GetMapping("/rename")
    public Result<Object> rename(@RequestParam String oldPath,  @RequestParam String newPath) {
        demo2Service.rename(oldPath, newPath);
        return Result.succeed();
    }

    /**
     * 删除文件
     */
    @GetMapping("/delete")
    public Result<Object> delete(@RequestParam String sftpPath) {
        demo2Service.delete(sftpPath);
        return Result.succeed();
    }

}
