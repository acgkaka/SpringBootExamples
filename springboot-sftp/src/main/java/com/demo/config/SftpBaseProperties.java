package com.demo.config;

import lombok.Data;

/**
 * <p> @Title SftpProperties
 * <p> @Description SFTP配置类
 *
 * @author ACGkaka
 * @date 2024/1/31 17:38
 */
@Data
public class SftpBaseProperties {

    /**
     * 协议
     */
    private String protocol;

    /**
     * IP
     */
    private String host;

    /**
     * 端口
     */
    private Integer port;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

}
