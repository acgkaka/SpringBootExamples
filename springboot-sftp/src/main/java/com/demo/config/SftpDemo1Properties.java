package com.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p> @Title SftpDemo1Properties
 * <p> @Description SFTP：示例场景一
 *
 * @author ACGkaka
 * @date 2024/2/2 16:17
 */
@Component
@ConfigurationProperties(prefix = "sftp.demo1")
public class SftpDemo1Properties extends SftpBaseProperties {
}
